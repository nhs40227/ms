//====================================================================
//
// メーター [Meter.fx]
//
//====================================================================
matrix proj;
matrix world;
float3x3 texR;

sampler s0 : register(s0);
sampler s1 : register(s1);

float degree;
float range;

struct psInput{
	float4 pos : POSITION;
	float2 tex : TEXCOORD;
	float3 tex1 : TEXCOORD1;
};

//====================================================================
// 描画
//====================================================================
psInput vs_main(float4 pos : POSITION, float2 tex : TEXCOORD)
{
	psInput Out = (psInput)0;

	Out.pos = mul(pos, world);
	Out.pos = mul(Out.pos, proj);

	Out.tex = tex;
	Out.tex1 = mul(float3(tex - 0.5f, 1), texR);

	return Out;
}

float4 ps_main(psInput In) : COLOR0
{
	float2 v = normalize(In.tex - 0.5f);
	float angle = acos(dot(float2(0, -1), v));
	angle = degrees(v.x < 0 ? -angle : angle);
	float4 color = angle > degree - range && angle < degree + range ? float4(1,0,0,1) : 1;

	float4 tex_color = tex2D(s0, In.tex) * color * 0.8f;
	float4 tex_color1 = tex2D(s1, In.tex1.xy);

	return tex_color1.w < 0.5f ? tex_color : tex_color1;
}

technique tech{
	pass{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader  = compile ps_3_0 ps_main();

		ZEnable = FALSE;
		AlphaBlendEnable = TRUE;
		SrcBlend = SRCALPHA;
		DestBlend = INVSRCALPHA;
	}
}
