//====================================================================
//
// 2Dスプライト描画シェーダ [2DSprite.fx]
//
//====================================================================
matrix world;
matrix proj;

float2 scWH;

float4 color;

texture Texture;
sampler samp = sampler_state{
	texture = <Texture>;
	MagFilter = LINEAR;
	MinFilter = LINEAR;
	AddressU = CLAMP;
	AddressV = CLAMP;
};

float2 texelSize;

//********************************************************************
// 構造体定義
//********************************************************************
struct VS_IN{
	float4 pos : POSITION;
	float2 uv : TEXCOORD0;
};

struct VS_OUT{
	float4 pos : POSITION;
	float2 uv : TEXCOORD0;
};

//====================================================================
// 頂点シェーダ
//====================================================================
VS_OUT vs_main(VS_IN In)
{
	VS_OUT Out = (VS_OUT)0;

	Out.pos = mul(In.pos, world);
	Out.pos = mul(Out.pos, proj);

	Out.uv = In.uv;
	Out.uv += 1.0f / scWH;

	return Out;
}

//====================================================================
// ピクセルシェーダ
//====================================================================
float4 ps_main(VS_OUT In) : COLOR0
{
	return color * tex2D(samp, In.uv);
}

float4 ps_outline(VS_OUT In) : COLOR0
{
	float4 current	= tex2D(samp, In.uv);

	// 周囲のテクセル取得
	float4 tex[8];
	tex[0] = tex2D(samp, In.uv + float2(-texelSize.x, 0.0f));
	tex[1] = tex2D(samp, In.uv + float2(0.0f, -texelSize.y));
	tex[2] = tex2D(samp, In.uv + float2(texelSize.x, 0.0f));
	tex[3] = tex2D(samp, In.uv + float2(0.0f, texelSize.y));

	tex[4] = tex2D(samp, In.uv + float2(-texelSize.x, -texelSize.y));
	tex[5] = tex2D(samp, In.uv + float2(texelSize.x, -texelSize.y));
	tex[6] = tex2D(samp, In.uv + float2(texelSize.x, texelSize.y));
	tex[7] = tex2D(samp, In.uv + float2(-texelSize.x, texelSize.y));

	for(uint i = 0; i < 8; i++){
		float r = abs(tex[i].r - current.r);
		float g = abs(tex[i].g - current.g);
		float b = abs(tex[i].b - current.b);
		if(r + g + b > 1.0f){
			return color * current;
		}
	}

	return 0;
}

//********************************************************************
// テクニック
//********************************************************************
technique tech{
	// 通常描画
	pass p0{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader  = compile ps_3_0 ps_main();

		ZEnable = FALSE;
		AlphaBlendEnable = TRUE;
		SrcBlend = SRCALPHA;
		DestBlend = INVSRCALPHA;
	}

	// 輪郭抽出
	pass p1{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader  = compile ps_3_0 ps_outline();

		ZEnable = FALSE;
		AlphaBlendEnable = TRUE;
		SrcBlend = SRCALPHA;
		DestBlend = INVSRCALPHA;
	}
}
