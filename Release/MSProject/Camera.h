//====================================================================
//
// カメラの処理 [Camera.h]
//
//====================================================================
#pragma once

#include "stdafx.h"

//====================================================================
// クラス定義
//====================================================================
class Camera{
public:
	Camera();

private:
	D3DXMATRIX m_mView;
	D3DXMATRIX m_mProj;

	D3DXVECTOR3 m_eye;
	D3DXVECTOR3 m_at;
	D3DXVECTOR3 m_up;

	float m_fovy;
	float m_aspect;
	float m_zn;
	float m_zf;

public:
	void Transform(void);
	void GetMatrix(D3DXMATRIX* pView, D3DXMATRIX* pProj);

	void SetEye(const D3DXVECTOR3& eye);
	void SetAt(const D3DXVECTOR3& at);
	void SetUp(const D3DXVECTOR3& up);
	void SetFovy(float degree);
	void SetAspect(float aspect);
	void SetClip(float zn, float zf);
};
