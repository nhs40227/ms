//====================================================================
//
// 2Dスプライトクラス [Sprite.h]
//
//====================================================================
#pragma once

#include "stdafx.h"

//====================================================================
// クラス定義
//====================================================================
class Sprite{
public:
	Sprite();
	Sprite(const Sprite& sprite);
	~Sprite();

	static HRESULT Initialize(LPDIRECT3DDEVICE9 pDevice, UINT scWidth, UINT scHeight);
	static void Finalize(void);
	static void OnLostDevice(void);
	static HRESULT OnResetDevice(void);

protected:
	static LPDIRECT3DDEVICE9				m_pDevice;
	static LPDIRECT3DVERTEXBUFFER9			m_pVtxBuff;
	static LPD3DXEFFECT						m_pEffect;
	static D3DXVECTOR2						m_screen;

	D3DXVECTOR2				m_pos;
	D3DXVECTOR2				m_scale;
	float					m_rot;
	D3DXVECTOR2				m_uvXY;
	D3DXVECTOR2				m_uvWH;
	D3DXCOLOR				m_color;
	LPDIRECT3DTEXTURE9		m_pTexture;

public:
	void Draw(void);

	// 以下セッター
	void SetPos(const D3DXVECTOR2& pos);
	void SetScale(const D3DXVECTOR2& scale);
	void SetRot(float radian);

	void SetUVXY(const D3DXVECTOR2& uvXY);
	void SetUVWH(const D3DXVECTOR2& uvWH);

	void SetColor(const D3DXCOLOR& color);
	void SetTexture(LPDIRECT3DTEXTURE9 pTexture);

	// 以下ゲッター
	const D3DXVECTOR2& GetPos(void);
	const D3DXVECTOR2& GetScale(void);
	float GetRot(void);

	const D3DXVECTOR2& GetUVXY(void);
	const D3DXVECTOR2& GetUVWH(void);

	const D3DXCOLOR& GetColor(void);
};
