//====================================================================
//
// ポリゴンクラス [Polygon.cpp]
//
//====================================================================
#include "Polygon.h"

//====================================================================
// 静的メンバ変数
//====================================================================
LPDIRECT3DDEVICE9 Polygon3D::m_pDevice = nullptr;
LPDIRECT3DVERTEXBUFFER9 Polygon3D::m_pVtxBuffer = nullptr;
LPD3DXEFFECT Polygon3D::m_pEffect = nullptr;

//====================================================================
// 初期化
//====================================================================
HRESULT Polygon3D::Initialize(LPDIRECT3DDEVICE9 pDevice)
{
	// デバイスセット
	m_pDevice = pDevice;
	m_pDevice->AddRef();

	// 頂点バッファ作成
	float vtx[] = {
		-0.5f, +0.5f, 0.0f, 1.0f, 0.0f, 0.0f,
		+0.5f, +0.5f, 0.0f, 1.0f, 1.0f, 0.0f,
		-0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f,
		+0.5f, -0.5f, 0.0f, 1.0f, 1.0f, 1.0f
	};
	if(FAILED(m_pDevice->CreateVertexBuffer(sizeof(vtx), D3DUSAGE_WRITEONLY,
		D3DFVF_XYZW | D3DFVF_TEX1, D3DPOOL_MANAGED, &m_pVtxBuffer, nullptr))) return E_FAIL;
	float* p = nullptr;
	m_pVtxBuffer->Lock(0, 0, (void**)&p, 0);
	memcpy(p, vtx, sizeof(vtx));
	m_pVtxBuffer->Unlock();

	// シェーダ作成
	if(!m_pEffect){
		const char* path = "data/shader/polygon.fx";
		LPD3DXBUFFER pErr = nullptr;
		if(FAILED(D3DXCreateEffectFromFileA(m_pDevice, path,
			nullptr, nullptr, 0, nullptr, &m_pEffect, &pErr))){
				if(pErr) MessageBoxA(nullptr, (LPCSTR)pErr->GetBufferPointer(), path, MB_OK);
				else MessageBoxA(nullptr, "エフェクトファイルが見つかりません", path, MB_OK);
				SAFE_RELEASE(pErr);
				return E_FAIL;
		}
	}

	return S_OK;
}

//====================================================================
// 終了処理
//====================================================================
void Polygon3D::Finalize(void)
{
	SAFE_RELEASE(m_pDevice);
	SAFE_RELEASE(m_pVtxBuffer);
	SAFE_RELEASE(m_pEffect);
}

//====================================================================
// リストア処理
//====================================================================
void Polygon3D::OnLostDevice(void)
{
	m_pEffect->OnLostDevice();
}

HRESULT Polygon3D::OnResetDevice(void)
{
	return m_pEffect->OnResetDevice();
}

//====================================================================
// コンストラクタ、デストラクタ
//====================================================================
Polygon3D::Polygon3D() : m_uvXY(0.0f, 0.0f), m_uvWH(1.0f, 1.0f), m_color(0xffffffff)
{
	m_pTexture = nullptr;
	D3DXMatrixIdentity(&m_matTranslation);
	D3DXMatrixIdentity(&m_matRotation);
	D3DXMatrixIdentity(&m_matScaling);
}

Polygon3D::~Polygon3D()
{

}

//====================================================================
// 描画
//====================================================================
void Polygon3D::Draw(const D3DXMATRIX& view, const D3DXMATRIX& proj)
{
	// 描画開始
	m_pEffect->SetTechnique("tech");
	m_pEffect->Begin(nullptr, 0);
	m_pEffect->BeginPass(0);

	// 行列
	m_pEffect->SetMatrix("view", &view);
	m_pEffect->SetMatrix("proj", &proj);

	// ワールド変換
	D3DXMATRIX world = m_matScaling * m_matRotation * m_matTranslation;
	m_pEffect->SetMatrix("world", &world);

	// 頂点データ
	m_pDevice->SetFVF(D3DFVF_XYZW | D3DFVF_TEX1);
	m_pDevice->SetStreamSource(0, m_pVtxBuffer, 0, sizeof(float) * 6);

	// uv座標
	m_pEffect->SetFloatArray("uvXY", m_uvXY, 2);
	m_pEffect->SetFloatArray("uvWH", m_uvWH, 2);

	// 色
	m_pEffect->SetFloatArray("color", m_color, 4);

	// テクスチャ
	m_pDevice->SetTexture(0, m_pTexture);
	m_pEffect->SetBool("IsTexture", (BOOL)m_pTexture);

	// サンプラー
	m_sampler.Set(0, m_pDevice);

	// 描画
	m_pEffect->CommitChanges();
	m_pDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);

	// 描画終了
	m_pEffect->EndPass();
	m_pEffect->End();

	m_sampler.Reset(0, m_pDevice);
}

//====================================================================
// セッター
//====================================================================
void Polygon3D::SetPos(const D3DXVECTOR3& pos)
{
	m_matTranslation._41 = pos.x;
	m_matTranslation._42 = pos.y;
	m_matTranslation._43 = pos.z;
}

void Polygon3D::SetScale(const D3DXVECTOR3& scale)
{
	m_matScaling._11 = scale.x;
	m_matScaling._22 = scale.y;
	m_matScaling._33 = scale.z;
}

void Polygon3D::SetRot(const D3DXMATRIX& mR)
{
	m_matRotation = mR;
}

void Polygon3D::SetUVXY(const D3DXVECTOR2& xy)
{
	m_uvXY = xy;
}

void Polygon3D::SetUVWH(const D3DXVECTOR2& wh)
{
	m_uvWH = wh;
}

void Polygon3D::SetColor(const D3DXCOLOR& color)
{
	m_color = color;
}

void Polygon3D::SetTexture(LPDIRECT3DTEXTURE9 pTexture)
{
	m_pTexture = pTexture;
}

//====================================================================
// ゲッター
//====================================================================
void Polygon3D::GetPos(D3DXVECTOR3* pOut)
{
	pOut->x = m_matTranslation._41;
	pOut->y = m_matTranslation._42;
	pOut->z = m_matTranslation._43;
}

void Polygon3D::GetScale(D3DXVECTOR3* pOut)
{
	pOut->x = m_matScaling._11;
	pOut->y = m_matScaling._22;
	pOut->z = m_matScaling._33;
}

sampler_state& Polygon3D::GetSamplerState(void)
{
	return m_sampler;
}
