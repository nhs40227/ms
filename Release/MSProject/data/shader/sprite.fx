//====================================================================
//
// 2Dスプライト描画シェーダ [2DSprite.fx]
//
//====================================================================
matrix world;
matrix proj;

float2 uvXY;
float2 uvWH;
float2 scWH;

float4 color;

texture Texture;
sampler samp = sampler_state{
	texture = <Texture>;
	MagFilter = GAUSSIANQUAD;
	MinFilter = GAUSSIANQUAD;
	AddressU = WRAP;
	AddressV = WRAP;
};
bool IsTexture;

//====================================================================
// 構造体定義
//====================================================================
struct PS_IN{
	float4 pos : POSITION;
	float2 uv : TEXCOORD0;
};

//====================================================================
// 頂点シェーダ
//====================================================================
PS_IN vs_main(float4 pos : POSITION, float2 uv : TEXCOORD)
{
	PS_IN Out;

	Out.pos = mul(pos, world);
	Out.pos = mul(Out.pos, proj);

	Out.uv = uv * uvWH + uvXY;

	return Out;
}

//====================================================================
// ピクセルシェーダ
//====================================================================
float4 ps_main(PS_IN In) : COLOR0
{
	float4 tex = IsTexture ? tex2D(samp, In.uv) : 1;
	return color * tex;
}

//====================================================================
// テクニック
//====================================================================
technique tech{
	pass{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader  = compile ps_3_0 ps_main();

		ZEnable = FALSE;
	}
}
