//====================================================================
//
// 文字列テクスチャ [StrTexture.h]
//
//====================================================================
#pragma once

#include "stdafx.h"

//====================================================================
// クラス定義
//====================================================================
class StrTexture{
public:
	static HRESULT Initialize(LPDIRECT3DDEVICE9 pDevice);
	static void Finalize(void);

private:
	static LPDIRECT3DDEVICE9 m_pDevice;

public:
	StrTexture();
	~StrTexture();

	HRESULT CreateTexture(LPCSTR pFaceName, UINT weight, LPCSTR str, DWORD format);
	void ReleaseTexture(void);
	LPDIRECT3DTEXTURE9 GetTexture(void);

private:
	LPDIRECT3DTEXTURE9 m_pTexture;
};
