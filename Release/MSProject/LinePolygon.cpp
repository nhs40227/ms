//====================================================================
//
// ラインポリゴン [LinePolygon.cpp]
//
//====================================================================
#include "LinePolygon.h"

//====================================================================
// 静的メンバ変数
//====================================================================
LPDIRECT3DDEVICE9 LinePolygon::m_pDevice = nullptr;
LPDIRECT3DVERTEXBUFFER9 LinePolygon::m_pVtxBuffer = nullptr;
LPD3DXEFFECT LinePolygon::m_pEffect = nullptr;
std::list<LinePolygon*> LinePolygon::m_list;

//====================================================================
// コンストラクタ、デストラクタ
//====================================================================
LinePolygon::LinePolygon()
{
	D3DXMatrixIdentity(&m_mWorld);
	m_pTexture = nullptr;
}

LinePolygon::~LinePolygon()
{

}

//====================================================================
// 初期化
//====================================================================
HRESULT LinePolygon::Initialize(LPDIRECT3DDEVICE9 pDevice)
{
	// デバイスセット
	m_pDevice = pDevice;
	m_pDevice->AddRef();

	// 頂点バッファ作成
	float vtx[] = {
		-0.05f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f,
		+0.05f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f,
		-0.05f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
		+0.05f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f
	};
	if(FAILED(m_pDevice->CreateVertexBuffer(sizeof(vtx), D3DUSAGE_WRITEONLY,
		D3DFVF_XYZW | D3DFVF_TEX1, D3DPOOL_MANAGED, &m_pVtxBuffer, nullptr))) return E_FAIL;
	float* p = nullptr;
	m_pVtxBuffer->Lock(0, 0, (void**)&p, 0);
	memcpy(p, vtx, sizeof(vtx));
	m_pVtxBuffer->Unlock();

	// シェーダ作成
	LPCSTR path = "data/shader/LinePolygon.fx";
	LPD3DXBUFFER pErr = nullptr;
	if(FAILED(D3DXCreateEffectFromFileA(m_pDevice, path, nullptr, nullptr, 0, nullptr, &m_pEffect, &pErr))){
		if(pErr) MessageBoxA(nullptr, (LPCSTR)pErr->GetBufferPointer(), path, MB_OK);
		else MessageBoxA(nullptr, "エフェクトファイルが見つかりません", path, MB_OK);
		SAFE_RELEASE(pErr);
		return E_FAIL;
	}
	m_pEffect->SetTechnique("tech");

	return S_OK;
}

//====================================================================
// 終了処理
//====================================================================
void LinePolygon::Finalize(void)
{
	SAFE_RELEASE(m_pDevice);
	SAFE_RELEASE(m_pVtxBuffer);
	SAFE_RELEASE(m_pEffect);
}

//====================================================================
// リストア処理
//====================================================================
void LinePolygon::OnLostDevice(void)
{
	m_pEffect->OnLostDevice();
}

HRESULT LinePolygon::OnResetDevice(void)
{
	if(FAILED(m_pEffect->OnResetDevice())) return E_FAIL;

	return S_OK;
}

//====================================================================
// 描画
//====================================================================
void LinePolygon::DrawAll(const D3DXMATRIX& mView, const D3DXMATRIX& mProj)
{
	// 描画開始
	m_pEffect->Begin(nullptr, 0);
	m_pEffect->BeginPass(0);

	// 頂点データ
	m_pDevice->SetStreamSource(0, m_pVtxBuffer, 0, sizeof(float) * 6);
	m_pDevice->SetFVF(D3DFVF_XYZW | D3DFVF_TEX1);

	// ビュー射影行列
	m_pEffect->SetMatrix("mView", &mView);
	m_pEffect->SetMatrix("mProj", &mProj);

	// リストに積まれたポリゴンを描画
	for each(auto p in m_list){
		// ワールド行列
		m_pEffect->SetMatrix("mWorld", &p->m_mWorld);

		//テクスチャとサンプラー
		m_pDevice->SetTexture(0, p->m_pTexture);
		m_pEffect->SetBool("bTexture", (BOOL)p->m_pTexture);
		p->m_sampler.Set(0, m_pDevice);

		// 描画
		m_pEffect->CommitChanges();
		m_pDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);

		// サンプラーを戻す
		p->m_sampler.Reset(0, m_pDevice);
	}

	// 描画終了
	m_pEffect->EndPass();
	m_pEffect->End();

	// リストのクリア
	m_list.clear();
}

//====================================================================
// セッター
//====================================================================
void LinePolygon::SetPos(const D3DXVECTOR3& begin, const D3DXVECTOR3& dir)
{
	m_mWorld._41 = begin.x;
	m_mWorld._42 = begin.y;
	m_mWorld._43 = begin.z;

	D3DXVECTOR3 cross;
	D3DXVec3Cross(&cross, &D3DXVECTOR3(0,1,0), &dir);

	m_mWorld._31 = dir.x;
	m_mWorld._32 = dir.y;
	m_mWorld._33 = dir.z;

	m_mWorld._11 = cross.x;
	m_mWorld._12 = cross.y;
	m_mWorld._13 = cross.z;
}

void LinePolygon::SetScale(const D3DXVECTOR2& scale)
{
	D3DXVECTOR3 axisX(&m_mWorld._11);
	D3DXVECTOR3 axisZ(&m_mWorld._31);

	D3DXVec3Normalize(&axisX, &axisX);
	D3DXVec3Normalize(&axisZ, &axisZ);

	axisX *= scale.x;
	axisZ *= scale.y;

	m_mWorld._11 = axisX.x;
	m_mWorld._12 = axisX.y;
	m_mWorld._13 = axisX.z;

	m_mWorld._31 = axisZ.x;
	m_mWorld._32 = axisZ.y;
	m_mWorld._33 = axisZ.z;
}

void LinePolygon::SetTexture(LPDIRECT3DTEXTURE9 pTexture)
{
	m_pTexture = pTexture;
}

//====================================================================
// サンプラーステート取得
//====================================================================
sampler_state& LinePolygon::GetSamplerState(void)
{
	return m_sampler;
}

//====================================================================
// 描画リストに積む
//====================================================================
void LinePolygon::Draw(void)
{
	m_list.push_back(this);
}
