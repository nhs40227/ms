//====================================================================
//
// 風船 [Balloon.h]
//
//====================================================================
#pragma once

#include "stdafx.h"
#include "Sprite.h"

//====================================================================
// クラス定義
//====================================================================
class Balloon{
public:
	void Init(const D3DXVECTOR2& pos, const D3DXVECTOR2& scale,
		float range, LPDIRECT3DTEXTURE9 pTexture);

	void Move(float speedX);
	void Draw(void);

private:
	Sprite m_sprite;

	float m_moveRange;		// 移動範囲
	float m_baseHeight;		// 基準の高さ
	float m_time;
	float m_speed;
};
