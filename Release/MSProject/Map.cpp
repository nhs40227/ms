//====================================================================
//
// マップ [Map.cpp]
//
//====================================================================
#include "Map.h"

//====================================================================
// 静的メンバ変数
//====================================================================
const POINT Map::m_arraySize = {19, 10};		// 配列サイズ

//====================================================================
// コンストラクタ、デストラクタ
//====================================================================
Map::Map()
{
	// テクスチャ配列の初期化
	for(BYTE i = 0; i < TEX_MAX; i++){
		m_pTextures[i] = nullptr;
	}
}

//====================================================================
// 作成
//====================================================================
HRESULT Map::Create(LPDIRECT3DDEVICE9 pDevice, UINT scHeight)
{
	// デバイスセット
	m_pDevice = pDevice;
	m_pDevice->AddRef();

	// グリッドのサイズを設定
	m_fGridScale = (float)scHeight / m_arraySize.x;

	// 中心位置を設定
	LONG centerX = m_arraySize.x / 2;
	LONG centerY = m_arraySize.y / 2;

	// 左下座標を設定
	float left = -m_fGridScale * centerX;
	float bottom = -m_fGridScale * centerY;

	// グリッドに張り付けるテクスチャの読み込み
	LPCSTR texName[TEX_MAX] = {
		"data/texture/tatami2.png",
		"data/texture/shoji.png",
		"data/texture/tenjo2.png",
	};
	for(BYTE i = 0; i < TEX_MAX; i++){
		if(FAILED(D3DXCreateTextureFromFileA(m_pDevice, texName[i], &m_pTextures[i]))){
			return E_FAIL;
		}
	}

	// 配列の初期化と設定
	m_gridArray.resize(m_arraySize.y);
	for(LONG y = 0; y < m_arraySize.y; y++){
		m_gridArray[y].resize(m_arraySize.x);
		for(LONG x = 0; x < m_arraySize.x; x++){
			// グリッド動的確保
			m_gridArray[y][x] = new Grid;

			// 壁で初期化
			m_gridArray[y][x]->SetState(0);
			m_gridArray[y][x]->SetTexture(m_pTextures[TEX_TENJO]);

			// ポリゴンの座標と大きさ設定
			m_gridArray[y][x]->SetScale(D3DXVECTOR3(m_fGridScale, m_fGridScale, 0.0f));
			m_gridArray[y][x]->SetPos(D3DXVECTOR3(left + m_fGridScale * x, bottom + m_fGridScale * y, 0.0f));
		}
	}

	// 部屋の数と位置をランダムで設定
	UINT nRoom = rand() % 8 + 4;
	ComputeRoom(nRoom);

	return S_OK;
}

//====================================================================
// 解放
//====================================================================
void Map::Release(void)
{
	// 配列の削除
	for each(auto ary in m_gridArray){
		for each(auto grid in ary){
			SAFE_DELETE(grid);
		}
	}
	m_gridArray.clear();

	for(BYTE i = 0; i < TEX_MAX; i++){
		SAFE_RELEASE(m_pTextures[i]);
	}
	SAFE_RELEASE(m_pDevice);
}

//====================================================================
// 部屋を計算
//====================================================================
void Map::ComputeRoom(UINT numRoom)
{
	// 部屋の数だけ領域確保
	RECT* pRoom = new RECT[numRoom];

	// 部屋の領域を決定
	for(UINT i = 0; i < numRoom; i++){
		// 幅と高さを決定
		LONG w = rand() % (m_arraySize.x - 5) + 2;
		LONG h = rand() % (m_arraySize.y - 5) + 2;
		// 左下
		pRoom[i].left = rand() % (m_arraySize.x - 1 - w) + 1;
		pRoom[i].bottom = rand() % (m_arraySize.y - 1 - h) + 1;
		// 右上
		pRoom[i].right = pRoom[i].left + w - 1;
		pRoom[i].top = pRoom[i].bottom + h - 1;
	}

	// 部屋が孤立しないように修正
	for(UINT i = 0; i < numRoom; i++){
		for(UINT j = i + 1; j < numRoom; j++){
			// 孤立していない
			if(pRoom[i].left <= pRoom[j].right && pRoom[i].right >= pRoom[j].left){
				if(pRoom[i].bottom <= pRoom[j].top && pRoom[i].top >= pRoom[j].bottom){
					continue;
				}
			}

			// 孤立しているので部屋を拡張
			LONG left = pRoom[i].left - pRoom[j].right;
			LONG right = pRoom[j].left - pRoom[i].right;
			LONG bottom = pRoom[i].bottom - pRoom[j].top;
			LONG top = pRoom[j].bottom - pRoom[i].top;
			pRoom[i].left -= left > 0 ? left : 0;
			pRoom[i].right += right > 0 ? right : 0;
			pRoom[i].bottom -= bottom > 0 ? bottom : 0;
			pRoom[i].top += top > 0 ? top : 0;
		}
	}

	// 部屋に該当する部分を空ける
	for(LONG y = 1; y < m_arraySize.y - 2; y++){
		for(LONG x = 1; x < m_arraySize.x - 1; x++){
			for(UINT i = 0; i < numRoom; i++){
				auto grid = m_gridArray[y][x];

				if(x >= pRoom[i].left && x <= pRoom[i].right){
					if(y >= pRoom[i].bottom && y <= pRoom[i].top){
						grid->SetState(1);
						grid->SetColor(0xffffffff);
						grid->SetTexture(m_pTextures[TEX_TATAMI]);
					}
				}
			}
		}
	}

	for(LONG y = 0; y < m_arraySize.y; y++){
		for(LONG x = 0; x < m_arraySize.x; x++){
			auto grid = m_gridArray[y][x];

			if(y == 0){
				grid->SetTexture(m_pTextures[TEX_TENJO]);
				continue;
			}

			if(grid->GetState() == 0){
				if(m_gridArray[y - 1][x]->GetState() == 1){
					// 障子のテクスチャを張る
					grid->SetTexture(m_pTextures[TEX_SHOJI]);
				}
			}
		}
	}

	// 解放
	SAFE_DELETE_ARRAY(pRoom);
}

//====================================================================
// 描画
//====================================================================
void Map::Draw(const D3DXMATRIX& view, const D3DXMATRIX& proj)
{
	// zバッファOFF
	m_pDevice->SetRenderState(D3DRS_ZENABLE, FALSE);

	// 描画
	for each(auto ary in m_gridArray){
		for each(auto grid in ary){
			grid->Draw(view, proj);
		}
	}

	// 設定を戻す
	m_pDevice->SetRenderState(D3DRS_ZENABLE, TRUE);
}

//====================================================================
// セッター
//====================================================================
void Map::SetUnitId(const POINT& point, DWORD id)
{
	m_gridArray[point.y][point.x]->SetUnitId(id);
}

//====================================================================
// ゲッター
//====================================================================
void Map::GetPos(const POINT& point, D3DXVECTOR3* pOut)
{
	// 指定した要素の座標を取得
	m_gridArray[point.y][point.x]->GetPos(pOut);
}

void Map::GetEmptyPoint(POINT* pOut)
{
	UINT count = 0;
	std::vector<LONG> aryX;
	std::vector<LONG> aryY;
	for(LONG y = 1; y < m_arraySize.y - 1; y++){
		for(LONG x = 1; x < m_arraySize.x - 1; x++){
			if(m_gridArray[y][x]->GetState() != 0){
				count++;
				aryX.push_back(x);
				aryY.push_back(y);
			}
		}
	}

	UINT i = rand() % count;
	pOut->x = aryX[i];
	pOut->y = aryY[i];
}

float Map::GetGridScale(void)
{
	return m_fGridScale;
}

BYTE Map::GetState(const POINT& point)
{
	return m_gridArray[point.y][point.x]->GetState();
}

DWORD Map::GetUnitId(const POINT& point)
{
	return m_gridArray[point.y][point.x]->GetUnitId();
}
