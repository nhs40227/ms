//====================================================================
//
// ミニゲーム基底クラス [MiniGame.cpp]
//
//====================================================================
#include "MiniGame.h"

#include "MyMath.h"
#include "Swordsmith_easy.h"

//====================================================================
// 静的メンバ変数
//====================================================================
LPDIRECT3DDEVICE9 MiniGame::m_pDevice = nullptr;
D3DXVECTOR2 MiniGame::m_spriteSize(0,0);
D3DXVECTOR2 MiniGame::m_spritePos(0,0);

//====================================================================
// コンストラクタ
//====================================================================
MiniGame::MiniGame()
{

}

//====================================================================
// ミニゲーム初期化
//====================================================================
HRESULT MiniGame::Initialize(LPDIRECT3DDEVICE9 pDevice,
	const D3DXVECTOR2& spritePos, const D3DXVECTOR2& spriteSize)
{
	// デバイスセット
	m_pDevice = pDevice;
	m_pDevice->AddRef();

	m_spritePos = spritePos;
	m_spriteSize = spriteSize;

	return S_OK;
}

//====================================================================
// 終了処理
//====================================================================
void MiniGame::Finalize(void)
{
	SAFE_RELEASE(m_pDevice);
}
