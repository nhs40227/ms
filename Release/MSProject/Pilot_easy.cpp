//====================================================================
//
// パイロット (Easyモード) [Pilot.cpp]
//
//====================================================================
#include "Pilot_easy.h"

#include "Keyboard.h"
#include "Joystick.h"

//====================================================================
// コンストラクタ
//====================================================================
Pilot_easy::Pilot_easy() : m_scrollFrame(180),
	m_accelAmt(m_spriteSize.y * 0.0002f)
{
	// 飛行機
	m_pPlaneTexture = nullptr;
	m_pPlane = nullptr;

	// 風船
	m_pBalloonTexture = nullptr;
	for(UINT i = 0; i < m_numBalloon; i++){
		m_balloonAry[i] = nullptr;
	}

	// 背景
	m_pSkyTexture = nullptr;
	m_pBackground = nullptr;
}

//====================================================================
// 開始
//====================================================================
void Pilot_easy::Begin(void)
{
	// 飛行機の生成
	D3DXCreateTextureFromFileA(m_pDevice, "data/texture/Airplane.png", &m_pPlaneTexture);
	m_pPlane = new Sprite;
	float planeSize = m_spriteSize.x / 12.0f;
	m_pPlane->SetScale(D3DXVECTOR2(planeSize, planeSize));
	m_pPlane->SetPos(D3DXVECTOR2(planeSize, m_spritePos.y));
	m_pPlane->SetTexture(m_pPlaneTexture);
	m_planeAccel = 0.0f;

	// 風船の生成
	D3DXCreateTextureFromFileA(m_pDevice, "data/texture/Balloon.png", &m_pBalloonTexture);
	float blnSpace = m_spriteSize.x / 3;
	const int maxPosPattern = 5;
	int posPattern = rand() % maxPosPattern;
	float moveRange = m_spriteSize.y / (maxPosPattern + 1);
	for(UINT i = 0; i < m_numBalloon; i++){
		m_balloonAry[i] = new Balloon;

		posPattern = posPattern + (rand() % 3 - 1);
		if(posPattern < 0){
			posPattern = 1;
		}else if(posPattern >= maxPosPattern){
			posPattern = posPattern - 2;
		}

		D3DXVECTOR2 pos(m_spriteSize.x + blnSpace * i, moveRange * (posPattern + 1));
		m_balloonAry[i]->Init(pos, D3DXVECTOR2(planeSize, planeSize),
			moveRange, m_pBalloonTexture);
	}

	m_maxDistance = m_spriteSize.x + blnSpace * m_numBalloon;
	m_crntPos = 0.0f;

	// 背景の生成
	D3DXCreateTextureFromFileA(m_pDevice, "data/texture/SkyBackGround.png", &m_pSkyTexture);
	m_pBackground = new Sprite;
	m_pBackground->SetPos(m_spritePos);
	m_pBackground->SetScale(m_spriteSize);
	m_pBackground->SetTexture(m_pSkyTexture);

	// 時間のリセット
	m_dwTime = 0;
}

//====================================================================
// 終了
//====================================================================
void Pilot_easy::End(void)
{
	// 飛行機
	SAFE_RELEASE(m_pPlaneTexture);
	SAFE_DELETE(m_pPlane);

	// 風船
	SAFE_RELEASE(m_pBalloonTexture);
	for(UINT i = 0; i < m_numBalloon; i++){
		SAFE_DELETE(m_balloonAry[i]);
	}

	// 背景
	SAFE_RELEASE(m_pSkyTexture);
	SAFE_DELETE(m_pBackground);
}

//====================================================================
// 更新処理
//====================================================================
bool Pilot_easy::Update(void)
{
	// 時間更新
	m_dwTime++;

	// 飛行機の移動
	if(Keyboard::Prs(DIK_UP) || Joystick::LStickY(0) < -500){
		m_planeAccel -= m_accelAmt;
	}
	if(Keyboard::Prs(DIK_DOWN) || Joystick::LStickY(0) > 500){
		m_planeAccel += m_accelAmt;
	}
	auto planePos = m_pPlane->GetPos();
	auto planeScale = m_pPlane->GetScale() / 2.0f;
	planePos.y += m_planeAccel;
	if(planePos.y < planeScale.y){
		m_planeAccel = 0.0f;
		planePos.y = planeScale.y;
	}
	if(planePos.y > m_spriteSize.y - planeScale.y){
		m_planeAccel = 0.0f;
		planePos.y = m_spriteSize.y - planeScale.y;
	}
	m_pPlane->SetPos(planePos);

	// 風船の移動
	float speedX = m_spriteSize.x / m_scrollFrame;
	for(UINT i = 0; i < m_numBalloon; i++){
		m_balloonAry[i]->Move(speedX);
	}

	// 背景のスクロール
	auto uv = m_pBackground->GetUVXY();
	uv.x += 1.0f / m_scrollFrame;
	m_pBackground->SetUVXY(uv);

	m_crntPos += speedX;
	if(m_crntPos > m_maxDistance){
		return true;
	}

	return false;
}

//====================================================================
// 描画
//====================================================================
void Pilot_easy::Draw(void)
{
	// 背景
	m_pBackground->Draw();

	// αブレンディングを行う
	m_pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	m_pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	// 風船描画
	for(UINT i = 0; i < m_numBalloon; i++){
		m_balloonAry[i]->Draw();
	}

	// 飛行機描画
	m_pPlane->Draw();

	m_pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
	m_pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
	m_pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ZERO);
}

//====================================================================
// リストア処理
//====================================================================
void Pilot_easy::OnLostDevice(void)
{

}

HRESULT Pilot_easy::OnResetDevice(void)
{
	return S_OK;
}
