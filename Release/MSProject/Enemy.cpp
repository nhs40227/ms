//====================================================================
//
// 敵 [Enemy.cpp]
//
//====================================================================
#include "Enemy.h"

#include "MyMath.h"

//====================================================================
// 作成
//====================================================================
void Enemy::Create(const POINT& gridIdx, const D3DXVECTOR3& pos,
	const D3DXVECTOR3& scale, DWORD id, BYTE miniGameType)
{
	// グリッドの要素数を設定
	m_gridIdx = gridIdx;

	// 座標設定
	m_polygon.SetPos(pos);

	// 大きさ設定
	m_polygon.SetScale(scale);

	// TODO : テクスチャ設定
	m_polygon.SetTexture(nullptr);
	m_polygon.SetColor(0xff00ff00);

	// 自身のIDを設定
	m_id = id;

	// ミニゲームの種類を設定
	m_miniGameType = miniGameType;

	// 状態を初期化
	m_time = 0;
}

//====================================================================
// 移動
//====================================================================
void Enemy::BeginMove(const POINT& moveIdx, const D3DXVECTOR3& pos)
{
	m_gridIdx = moveIdx;
	m_polygon.GetPos(&m_start);
	m_end = pos;
	m_time = 0;
	m_actionState = ACTION_MOVE;
}

//====================================================================
// ミニゲームの開始
//====================================================================
void Enemy::BeginMiniGame(void)
{
	m_actionState = ACTION_MINIGAME;
}

//====================================================================
// 行動の終了
//====================================================================
void Enemy::ActionEnd(void)
{
	m_actionState = ACTION_END;
}

//====================================================================
// 更新処理
//====================================================================
void Enemy::Update(void)
{
	switch(m_actionState){
	case ACTION_WAIT:
		break;

	case ACTION_MOVE:
		Moving();
		break;

	case ACTION_MINIGAME:
		break;

	case ACTION_END:
		break;
	}
}

//====================================================================
// 移動中
//====================================================================
void Enemy::Moving(void)
{
	// 始点と終点の補間地点を求める
	D3DXVECTOR3 pos;
	const DWORD maxMoveFrame = 30;
	MyMath::Lerp<D3DXVECTOR3>(&pos, m_start, m_end, 1.0f / maxMoveFrame * m_time);

	// 座標設定
	m_polygon.SetPos(pos);
}

//====================================================================
// 待機状態に設定
//====================================================================
void Enemy::Wait(void)
{
	m_actionState = ACTION_WAIT;
}

//====================================================================
// 描画
//====================================================================
void Enemy::Draw(const D3DXMATRIX& mView, const D3DXMATRIX& mProj)
{
	m_polygon.Draw(mView, mProj);
}

//====================================================================
// ゲッター
//====================================================================
const POINT& Enemy::GetGridIndex(void)
{
	return m_gridIdx;
}

BYTE Enemy::GetActionState(void)
{
	return m_actionState;
}

DWORD Enemy::GetId(void)
{
	return m_id;
}

BYTE Enemy::GetMiniGameType(void)
{
	return m_miniGameType;
}
