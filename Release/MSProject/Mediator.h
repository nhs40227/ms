//====================================================================
//
// 仲介者 (UnitとMap) [Mediator.h]
//
//====================================================================
#pragma once

#include "stdafx.h"
#include "Unit.h"
#include "Map.h"

//====================================================================
// クラス定義
//====================================================================
class Mediator{
private:
	static Map* m_pMap;

public:
	static void SetMapPtr(Map* pMap);

	static bool CanMoveUnit(const POINT& point);
	static bool IsWall(const POINT& point);
	static void ChangeUnitIdToMap(DWORD id, const POINT& current,
		const POINT& next, D3DXVECTOR3* pOut);

	static DWORD GetUnitIdFromMap(const POINT& point);
};
