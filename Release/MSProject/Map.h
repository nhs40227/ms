//====================================================================
//
// マップ [Map.h]
//
//====================================================================
#pragma once

#include "stdafx.h"
#include "Grid.h"

//====================================================================
// クラス定義
//====================================================================
class Map{
public:
	Map();

	// 作成
	HRESULT Create(LPDIRECT3DDEVICE9 pDevice, UINT scHeight);
	void Release(void);

	// 描画
	void Draw(const D3DXMATRIX& view, const D3DXMATRIX& proj);

private:
	LPDIRECT3DDEVICE9 m_pDevice;

	static const POINT m_arraySize;			// 配列のサイズ
	float m_fGridScale;						// グリッドの大きさ
	std::vector<std::vector<Grid*>> m_gridArray;

	void ComputeRoom(UINT numRoom);

	enum : BYTE{
		TEX_TATAMI,
		TEX_SHOJI,
		TEX_TENJO,
		TEX_MAX
	};
	LPDIRECT3DTEXTURE9 m_pTextures[TEX_MAX];

public:
	// セッター
	void SetUnitId(const POINT& point, DWORD id);

	// ゲッター
	void GetPos(const POINT& point, D3DXVECTOR3* pOut);
	void GetEmptyPoint(POINT* pOut);

	float GetGridScale(void);
	BYTE GetState(const POINT& point);
	DWORD GetUnitId(const POINT& point);
};
