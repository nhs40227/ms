//====================================================================
//
// ラインポリゴン [LinePolygon.h]
//
//====================================================================
#pragma once

#include "stdafx.h"
#include "sampler_state.h"

//====================================================================
// クラス定義
//====================================================================
class LinePolygon{
public:
	LinePolygon();
	~LinePolygon();

	static HRESULT Initialize(LPDIRECT3DDEVICE9 pDevice);
	static void Finalize(void);
	static void OnLostDevice(void);
	static HRESULT OnResetDevice(void);
	static void DrawAll(const D3DXMATRIX& mView, const D3DXMATRIX& mProj);

private:
	static LPDIRECT3DDEVICE9 m_pDevice;
	static LPDIRECT3DVERTEXBUFFER9 m_pVtxBuffer;
	static LPD3DXEFFECT m_pEffect;
	static std::list<LinePolygon*> m_list;

private:
	D3DXMATRIX m_mWorld;
	sampler_state m_sampler;
	LPDIRECT3DTEXTURE9 m_pTexture;

public:
	void SetPos(const D3DXVECTOR3& begin, const D3DXVECTOR3& dir);
	void SetScale(const D3DXVECTOR2& scale);
	void SetTexture(LPDIRECT3DTEXTURE9 pTexture);

	sampler_state& GetSamplerState(void);
	void Draw(void);
};
