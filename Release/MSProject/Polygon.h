//====================================================================
//
// ポリゴンクラス [Polygon.h]
//
//====================================================================
#pragma once

#include "stdafx.h"
#include "sampler_state.h"

//====================================================================
// クラス定義
//====================================================================
class Polygon3D{
public:
	static HRESULT Initialize(LPDIRECT3DDEVICE9 pDevice);
	static void Finalize(void);
	static void OnLostDevice(void);
	static HRESULT OnResetDevice(void);

private:
	static LPDIRECT3DDEVICE9 m_pDevice;
	static LPDIRECT3DVERTEXBUFFER9 m_pVtxBuffer;
	static LPD3DXEFFECT m_pEffect;

public:
	Polygon3D();
	~Polygon3D();

	void Draw(const D3DXMATRIX& view, const D3DXMATRIX& proj);

private:
	D3DXMATRIX m_matTranslation;
	D3DXMATRIX m_matRotation;
	D3DXMATRIX m_matScaling;

	D3DXVECTOR2 m_uvXY;
	D3DXVECTOR2 m_uvWH;

	D3DXCOLOR m_color;
	LPDIRECT3DTEXTURE9 m_pTexture;
	sampler_state m_sampler;

public:
	void SetPos(const D3DXVECTOR3& pos);
	void SetScale(const D3DXVECTOR3& scale);
	void SetRot(const D3DXMATRIX& mR);
	void SetUVXY(const D3DXVECTOR2& xy);
	void SetUVWH(const D3DXVECTOR2& wh);
	void SetColor(const D3DXCOLOR& color);
	void SetTexture(LPDIRECT3DTEXTURE9 pTexture);

	void GetPos(D3DXVECTOR3* pOut);
	void GetScale(D3DXVECTOR3* pOut);
	sampler_state& GetSamplerState(void);
};
