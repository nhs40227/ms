//====================================================================
//
// カメラの処理 [Camera.cpp]
//
//====================================================================
#include "Camera.h"

//====================================================================
// コンストラクタ
//====================================================================
Camera::Camera() : m_eye(0,0,-10), m_at(0,0,0), m_up(0,1,0)
{
	D3DXMatrixIdentity(&m_mView);
	D3DXMatrixIdentity(&m_mProj);

	m_fovy = D3DXToRadian(45.0f);
	m_aspect = 1;
	m_zn = 1.0f;
	m_zf = 100.0f;
}

//====================================================================
// カメラ変換
//====================================================================
void Camera::Transform(void)
{
	D3DXMatrixLookAtLH(&m_mView, &m_eye, &m_at, &m_up);
	D3DXMatrixPerspectiveFovLH(&m_mProj, m_fovy, m_aspect, m_zn, m_zf);
}

//====================================================================
// 行列の取得
//====================================================================
void Camera::GetMatrix(D3DXMATRIX* pView, D3DXMATRIX* pProj)
{
	*pView = m_mView;
	*pProj = m_mProj;
}

//====================================================================
// セッター
//====================================================================
void Camera::SetEye(const D3DXVECTOR3& eye)
{
	m_eye = eye;
}

void Camera::SetAt(const D3DXVECTOR3& at)
{
	m_at = at;
}

void Camera::SetUp(const D3DXVECTOR3& up)
{
	m_up = up;
}

void Camera::SetFovy(float degree)
{
	m_fovy = D3DXToRadian(degree);
}

void Camera::SetAspect(float aspect)
{
	m_aspect = aspect;
}

void Camera::SetClip(float zn, float zf)
{
	m_zn = zn;
	m_zf = zf;
}
