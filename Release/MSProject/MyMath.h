//====================================================================
//
// 数学関係 [MyMath.h]
//
//====================================================================
#pragma once

#include "stdafx.h"

//====================================================================
// 名前空間定義
//====================================================================
namespace MyMath{
	template <typename type>
	void Lerp(type* pOut, const type& start, const type& end, float time){
		*pOut = start * (1.0f - time) + end * time;
	}
	template <typename type>
	void SinWave(type* pOut, const type& radius, const type& median, float degree){
		*pOut = median + radius * sin(D3DXToRadian(degree));
	}
};
