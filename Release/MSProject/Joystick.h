//====================================================================
//
// ジョイスティックの処理 [Joystick.h]
//
//====================================================================
#pragma once

#include "stdafx.h"

//====================================================================
// クラス定義
//====================================================================
// ジョイスティッククラス
class Joystick{
public:
	Joystick();
	~Joystick();

	static HRESULT Initialize(HWND hWnd, LPDIRECTINPUT8 pDirectInput, UINT num);
	static void Finalize(void);
	static void Update(void);

private:
	static LPDIRECTINPUT8				m_pDirectInput;
	static LPDIRECTINPUTDEVICE8*		m_ppDIDevice;
	static DIJOYSTATE*					m_pJoyState;
	static DIJOYSTATE*					m_pJoyStatePrev;
	static UINT							m_nNumCount;
	static UINT							m_nMaxController;

	// コールバック
	static BOOL CALLBACK EnumDevices(const DIDEVICEINSTANCE* lpddi, VOID* pvRef);
	static BOOL CALLBACK EnumDeviceObjects(const DIDEVICEOBJECTINSTANCE* lpddoi, VOID* pvRef);

public:
	// ボタン
	static bool Prs(UINT num, int button);
	static bool Trg(UINT num, int button);
	static bool Rls(UINT num, int button);

	// スティック
	static LONG LStickX(UINT num);
	static LONG LStickY(UINT num);
	static LONG RStickX(UINT num);
	static LONG RStickY(UINT num);
};
