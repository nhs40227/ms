//====================================================================
//
// 仲介者 (UnitとMap) [Mediator.cpp]
//
//====================================================================
#include "Mediator.h"

//====================================================================
// 静的メンバ変数
//====================================================================
Map* Mediator::m_pMap = nullptr;

//====================================================================
// マップへのポインタをセット
//====================================================================
void Mediator::SetMapPtr(Map* pMap)
{
	m_pMap = pMap;
}

//====================================================================
// ユニットが移動出来るか問い合わせ
//====================================================================
bool Mediator::CanMoveUnit(const POINT& point)
{
	if(m_pMap->GetState(point) == 0){		// 壁だから移動できない
		return false;
	}

	if(m_pMap->GetUnitId(point) != 0){		// ユニットがいるので移動できない
		return false;
	}

	return true;
}

//====================================================================
// 壁かどうか問い合わせ
//====================================================================
bool Mediator::IsWall(const POINT& point)
{
	return m_pMap->GetState(point) == 0;
}

//====================================================================
// マップのユニットIdを変更
//====================================================================
void Mediator::ChangeUnitIdToMap(DWORD id, const POINT& current,
	const POINT& next, D3DXVECTOR3* pOut)
{
	// 現在の位置を空にする
	m_pMap->SetUnitId(current, 0);

	// 次の位置にユニットIdを設定
	m_pMap->SetUnitId(next, id);

	// 座標を取得
	m_pMap->GetPos(next, pOut);
}

//====================================================================
// ユニットのIdを取得
//====================================================================
DWORD Mediator::GetUnitIdFromMap(const POINT& point)
{
	return m_pMap->GetUnitId(point);
}
