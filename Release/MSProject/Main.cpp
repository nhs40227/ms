//====================================================================
//
// WinMain関数 [Main.cpp]
//
//====================================================================
#include "stdafx.h"

#include "MSApp.h"

//====================================================================
// WinMain関数
//====================================================================
int __stdcall WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int nCmdShow)
{
	// アプリケーションクラス生成
	BOOL bWindow = FALSE;
#ifdef _DEBUG
	bWindow = TRUE;
#endif
	MSApp app(bWindow);

	// ここからが本番
	return app.Run(hInstance, nCmdShow);
}
