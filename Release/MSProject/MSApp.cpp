//====================================================================
//
// MS用アプリケーションクラス [MSApp.cpp]
//
//====================================================================
#include "MSApp.h"

#include "App.h"
#include "Scene.h"
#include "SceneId.h"
#include "TitleScene.h"
#include "GameScene.h"

#include "Sprite.h"
#include "StrTexture.h"
#include "Polygon.h"
#include "Keyboard.h"
#include "Joystick.h"

//====================================================================
// コンストラクタ、デストラクタ
//====================================================================
MSApp::MSApp(BOOL bWindow) : App(L"MSApp", L"未来創造店", 1366 / 2, 768 / 2, bWindow)
{

}

MSApp::~MSApp()
{

}

//====================================================================
// 初期化
//====================================================================
HRESULT MSApp::Initialize(void)
{
	// スクリーンサイズ取得
	UINT scW, scH;
	App::GetScreenSize(&scW, &scH);

	// 乱数初期化
	srand((unsigned)time(nullptr));

	// 2Dスプライトクラス初期化
	if(FAILED(Sprite::Initialize(m_pDevice, scW, scH))) return E_FAIL;

	// 文字列テクスチャクラス初期化
	if(FAILED(StrTexture::Initialize(m_pDevice))) return E_FAIL;

	// ポリゴンクラス初期化
	if(FAILED(Polygon3D::Initialize(m_pDevice))) return E_FAIL;

	// シーンクラス初期化
	if(FAILED(Scene::Initialize(m_pDevice))) return E_FAIL;

	//--------------------------------------
	// ここにシーンを登録
	Scene::RegisterScene(new TitleScene);
	Scene::RegisterScene(new GameScene);
	// ここまで
	//--------------------------------------

	// 最初のシーンに設定
	return Scene::ChangeScene(SCENE_TITLE);
}

//====================================================================
// 終了処理
//====================================================================
void MSApp::Finalize(void)
{
	Scene::Finalize();
	Sprite::Finalize();
	StrTexture::Finalize();
	Polygon3D::Finalize();
}

//====================================================================
// 更新処理
//====================================================================
HRESULT MSApp::Update(void)
{
	return Scene::UpdateScene();
}

//====================================================================
// 描画
//====================================================================
void MSApp::Render(void)
{
	Scene::RenderScene();
}

//====================================================================
// リストア処理
//====================================================================
HRESULT MSApp::Restore(void)
{
	//--------------------------------------
	// デバイスのリセット前の解放処理
	//--------------------------------------
	Scene::OnLostDeviceScene();
	Sprite::OnLostDevice();
	Polygon3D::OnLostDevice();

	//--------------------------------------
	// デバイスのリセット
	//--------------------------------------
	m_pDevice->Reset(&m_d3dpp);

	//--------------------------------------
	// デバイスのリセット後の作成処理
	//--------------------------------------
	if(FAILED(Polygon3D::OnResetDevice())) return E_FAIL;
	if(FAILED(Sprite::OnResetDevice())) return E_FAIL;
	if(FAILED(Scene::OnResetDeviceScene())) return E_FAIL;

	return S_OK;
}

//====================================================================
// 入力初期化
//====================================================================
HRESULT MSApp::InitializeInput(HINSTANCE hInstance)
{
	// DirectInput8インターフェイス作成
	if(FAILED(DirectInput8Create(hInstance, DIRECTINPUT_VERSION,
		IID_IDirectInput8, (void**)&m_pDirectInput, nullptr))) return E_FAIL;

	// キーボード初期化
	if(FAILED(Keyboard::Initialize(m_hWnd, m_pDirectInput))) return E_FAIL;

	// ジョイスティック初期化
	if(FAILED(Joystick::Initialize(m_hWnd, m_pDirectInput, 1))) return E_FAIL;

	return S_OK;
}

//====================================================================
// 入力終了処理
//====================================================================
void MSApp::FinalizeInput(void)
{
	// キーボード終了処理
	Keyboard::Finalize();

	// ジョイスティック終了処理
	Joystick::Finalize();

	// DirectInput8インターフェイス解放
	SAFE_RELEASE(m_pDirectInput);
}

//====================================================================
// 入力更新処理
//====================================================================
void MSApp::UpdateInput(void)
{
	// キーボード更新処理
	Keyboard::Update();

	// ジョイスティック更新処理
	Joystick::Update();
}
