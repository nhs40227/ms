//====================================================================
//
// アプリケーションクラス [App.cpp]
//
//====================================================================
#include "App.h"

//====================================================================
// 静的メンバ変数
//====================================================================
UINT App::m_scWidth = 0;
UINT App::m_scHeight = 0;
BOOL App::m_bWindow = TRUE;

LPDIRECT3D9 App::m_pDirect3D = nullptr;
LPDIRECT3DDEVICE9 App::m_pDevice = nullptr;
D3DPRESENT_PARAMETERS App::m_d3dpp = {};
LPDIRECTINPUT8 App::m_pDirectInput = nullptr;

//====================================================================
// コンストラクタ、デストラクタ
//====================================================================
App::App(LPCWSTR pClassName, LPCWSTR pWindowName, UINT width, UINT height, BOOL bWindow)
{
#ifdef _DEBUG
	// メモリリークの検出
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	m_pClassName = pClassName;
	m_pWindowName = pWindowName;
	m_scWidth = width;
	m_scHeight = height;
	m_bWindow = bWindow;

	m_bRenderOK = true;
#ifdef _DEBUG
	m_pFPSFont = nullptr;
#endif
}

App::~App()
{

}

//====================================================================
// プログラム開始
//====================================================================
int App::Run(HINSTANCE hInstance, int nCmdShow)
{
	// ウィンドウクラスの登録
	WNDCLASSEX windowClass = {};
	windowClass.cbSize = sizeof(WNDCLASSEX);
	windowClass.style = CS_HREDRAW | CS_VREDRAW;
	windowClass.lpfnWndProc = WndProc;
	windowClass.hInstance = hInstance;
	windowClass.hCursor = LoadCursor(nullptr, IDC_ARROW);
	windowClass.lpszClassName = m_pClassName;
	if(!(RegisterClassEx(&windowClass))){
		return -1;
	}

	// ウィンドウスタイル
	DWORD dwStyle = WS_OVERLAPPEDWINDOW;

	// ウィンドウのサイズを計算
	RECT windowRect = {0, 0, static_cast<LONG>(m_scWidth), static_cast<LONG>(m_scHeight)};
	AdjustWindowRect(&windowRect, dwStyle, FALSE);

	// ウィンドウを作成し、ハンドルを取得
	m_hWnd = CreateWindow(m_pClassName, m_pWindowName, dwStyle, 64, 64,
		windowRect.right - windowRect.left, windowRect.bottom - windowRect.top,
		nullptr, nullptr, hInstance, nullptr);

	// グラフィックス初期化
	if(FAILED(InitializeGraphics())) return -1;
	// 入力処理初期化
	if(FAILED(InitializeInput(hInstance))) return -1;
	// 初期化
	if(FAILED(Initialize())) return -1;

	// ウィンドウの表示
	ShowWindow(m_hWnd, nCmdShow);
	UpdateWindow(m_hWnd);

	// 時間計測用
	timeBeginPeriod(1);
	DWORD dwTime = 0;
	DWORD dwLastTime = timeGetTime();

#ifdef _DEBUG
	// FPS計測用
	m_dwCountFPS = 0;
	DWORD dwFrameCount = 0;
	DWORD dwFPSLastTime = timeGetTime();
#endif

	// メッセージループ
	MSG msg;
	while(true){
		if(PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)){
			if(msg.message == WM_QUIT){
				break;
			}

			// メッセージの翻訳とディスパッチ
			TranslateMessage(&msg);
			DispatchMessage(&msg);

		}else{
			dwTime = timeGetTime();

#ifdef _DEBUG
			// 0.5秒ごとに実行
			if(dwTime - dwFPSLastTime >= 500){
				// FPS測定
				m_dwCountFPS = (dwFrameCount * 1000) / (dwTime - dwFPSLastTime);
				dwFPSLastTime = dwTime;		// 最後にFPSを測定した時刻を保存
				dwFrameCount = 0;			// カウントクリア
			}
#endif
			// 1 / 60秒ごとに実行
			if(dwTime - dwLastTime > 1000 / 60){
				dwLastTime = dwTime;		// 最後に処理した時刻を保存

				// バックバッファと深度バッファのクリア
				m_pDevice->Clear(0, nullptr, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0x0, 1.0f, 0);

				// レンダリング可能か調べる
				if(m_bRenderOK){

					// 入力更新処理
					UpdateInput();

					// 更新処理
					if(FAILED(Update())){
						DestroyWindow(m_hWnd);
						continue;
					}

					// Direct3Dによる描画開始
					if(SUCCEEDED(m_pDevice->BeginScene())){

						// 描画処理
						Render();

#ifdef _DEBUG
						// FPS描画
						DrawFPS();
						dwFrameCount++;
#endif
						// Direct3Dによる描画終了
						m_pDevice->EndScene();
					}
				}

				// バックバッファとフロントバッファの入れ替え
				if(m_pDevice->Present(nullptr, nullptr, nullptr, nullptr) == D3DERR_DEVICELOST){

					// レンダリング不可
					m_bRenderOK = false;

					// デバイスが復元可能か調べる
					if(m_pDevice->TestCooperativeLevel() == D3DERR_DEVICENOTRESET){

						// リストア処理
						if(FAILED(Restore())){
							DestroyWindow(m_hWnd);
							continue;
						}

						// レンダリング可能状態に
						m_bRenderOK = true;
					}
				}
			}
		}
	}

	// 分解能をもとに戻す
	timeEndPeriod(1);

	// 終了処理
	Finalize();
	// 入力終了処理
	FinalizeInput();
	// グラフィックス終了処理
	FinalizeGraphics();

	// ウィンドウクラスの登録解除
	UnregisterClass(windowClass.lpszClassName, windowClass.hInstance);

	return (int)msg.wParam;
}

//====================================================================
// ウィンドウプロシージャ
//====================================================================
LRESULT CALLBACK App::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch(message){
	case WM_DESTROY:
		PostQuitMessage(0);		// WM_QUITを送出
		break;

	case WM_KEYDOWN:
		switch(wParam){
		case VK_ESCAPE:
			DestroyWindow(hWnd);	// ウィンドウを破棄
			break;
		}
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}

//====================================================================
// グラフィックス初期化
//====================================================================
HRESULT App::InitializeGraphics(void)
{
	HRESULT hr;

	// Direct3Dオブジェクト作成
	m_pDirect3D = Direct3DCreate9(D3D_SDK_VERSION);
	if(!m_pDirect3D) return E_FAIL;

	// 現在のディスプレイモードを取得
	D3DDISPLAYMODE d3ddm;
	hr = m_pDirect3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3ddm);
	if(FAILED(hr)) return E_FAIL;

	// デバイスのプレゼントパラメータの設定
	m_d3dpp.BackBufferCount = 1;
	m_d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	m_d3dpp.EnableAutoDepthStencil = TRUE;
	m_d3dpp.AutoDepthStencilFormat = D3DFMT_D24S8;
	m_d3dpp.Windowed = m_bWindow;

	// ウィンドウモード
	if(m_d3dpp.Windowed){
		m_d3dpp.BackBufferWidth = m_scWidth;
		m_d3dpp.BackBufferHeight = m_scHeight;

		m_d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
		m_d3dpp.FullScreen_RefreshRateInHz = 0;
		m_d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;

	}else{// フルスクリーンモード
		m_d3dpp.BackBufferWidth = m_scWidth = d3ddm.Width;
		m_d3dpp.BackBufferHeight = m_scHeight = d3ddm.Height;

		m_d3dpp.BackBufferFormat = d3ddm.Format;
		m_d3dpp.FullScreen_RefreshRateInHz = d3ddm.RefreshRate;
		m_d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_DEFAULT;
	}

	// デバイスの作成
	hr = m_pDirect3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, m_hWnd,
		D3DCREATE_HARDWARE_VERTEXPROCESSING, &m_d3dpp, &m_pDevice);
	if(FAILED(hr)){
		hr = m_pDirect3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, m_hWnd,
			D3DCREATE_SOFTWARE_VERTEXPROCESSING, &m_d3dpp, &m_pDevice);
		if(FAILED(hr)){
			hr = m_pDirect3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_REF, m_hWnd,
				D3DCREATE_HARDWARE_VERTEXPROCESSING, &m_d3dpp, &m_pDevice);
			if(FAILED(hr)){
				m_pDirect3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_REF, m_hWnd,
					D3DCREATE_SOFTWARE_VERTEXPROCESSING, &m_d3dpp, &m_pDevice);
				if(FAILED(hr)){
					return hr;
				}
			}
		}
	}

#ifdef _DEBUG
	// フォントオブジェクト作成
	hr = D3DXCreateFontA(m_pDevice, 18, 0, FW_NORMAL, 0, FALSE, SHIFTJIS_CHARSET,
		OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "Terminal", &m_pFPSFont);
	if(FAILED(hr)) return hr;
#endif

	return S_OK;
}

//====================================================================
// グラフィックス終了処理
//====================================================================
void App::FinalizeGraphics(void)
{
#ifdef _DEBUG
	SAFE_RELEASE(m_pFPSFont);
#endif
	SAFE_RELEASE(m_pDevice);
	SAFE_RELEASE(m_pDirect3D);
}

#ifdef _DEBUG
//====================================================================
// FPS描画
//====================================================================
void App::DrawFPS(void)
{
	RECT rect = {0, 0, m_scWidth, m_scHeight};
	char str[256];
	sprintf_s(str, "FPS:%d\n", m_dwCountFPS);
	m_pFPSFont->DrawTextA(nullptr, str, -1, &rect, DT_LEFT, D3DCOLOR_XRGB(255, 255, 255));
}
#endif

//====================================================================
// ウィンドウハンドルを取得
//====================================================================
HWND App::GetWindowHandle(void)
{
	return m_hWnd;
}

//====================================================================
// 描画デバイス取得
//====================================================================
LPDIRECT3DDEVICE9 App::GetDevice(void)
{
	return m_pDevice;
}

//====================================================================
// スクリーンサイズの取得
//====================================================================
void App::GetScreenSize(UINT* pWidth, UINT* pHeight)
{
	*pWidth = m_scWidth;
	*pHeight = m_scHeight;
}
