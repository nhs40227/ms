//====================================================================
//
// 2Dスプライトクラス [Sprite.cpp]
//
//====================================================================
#include "Sprite.h"

//====================================================================
// 静的メンバ変数
//====================================================================
LPDIRECT3DDEVICE9 Sprite::m_pDevice = nullptr;
LPDIRECT3DVERTEXBUFFER9 Sprite::m_pVtxBuff = nullptr;
LPD3DXEFFECT Sprite::m_pEffect = nullptr;
D3DXVECTOR2 Sprite::m_screen(0.0f, 0.0f);

//====================================================================
// コンストラクタ、デストラクタ
//====================================================================
Sprite::Sprite()
{
	m_pos = D3DXVECTOR2(0.0f, 0.0f);
	m_scale = D3DXVECTOR2(1.0f, 1.0f);
	m_rot = 0.0f;
	m_uvXY = D3DXVECTOR2(0.0f, 0.0f);
	m_uvWH = D3DXVECTOR2(1.0f, 1.0f);
	m_color = 0xffffffff;
	m_pTexture = nullptr;
}

Sprite::Sprite(const Sprite& sprite)
{
	m_pos = sprite.m_pos;
	m_scale = sprite.m_scale;
	m_rot = sprite.m_rot;
	m_uvXY = sprite.m_uvXY;
	m_uvWH = sprite.m_uvWH;
	m_color = sprite.m_color;
	m_pTexture = sprite.m_pTexture;
}

Sprite::~Sprite()
{

}

//====================================================================
// 作成
//====================================================================
HRESULT Sprite::Initialize(LPDIRECT3DDEVICE9 pDevice, UINT width, UINT height)
{
	// デバイスオブジェクトコピー
	if(!m_pDevice && pDevice){
		m_pDevice = pDevice;
		m_pDevice->AddRef();
	}

	// 頂点バッファ作成
	if(!m_pVtxBuff){
		float vtx[] = {
			/* x     y     z     w     u     v */
			-0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f,
			+0.5f, -0.5f, 0.0f, 1.0f, 1.0f, 0.0f,
			-0.5f, +0.5f, 0.0f, 1.0f, 0.0f, 1.0f,
			+0.5f, +0.5f, 0.0f, 1.0f, 1.0f, 1.0f
		};
		if(FAILED(m_pDevice->CreateVertexBuffer(sizeof(vtx), D3DUSAGE_WRITEONLY,
			D3DFVF_XYZW | D3DFVF_TEX1, D3DPOOL_MANAGED, &m_pVtxBuff, nullptr))) return E_FAIL;
		float* v = nullptr;
		m_pVtxBuff->Lock(0, 0, (void**)&v, 0);
		memcpy(v, vtx, sizeof(vtx));
		m_pVtxBuff->Unlock();
	}

	// シェーダ作成
	if(!m_pEffect){
		const char* path = "data/shader/sprite.fx";
		LPD3DXBUFFER pErr = nullptr;
		if(FAILED(D3DXCreateEffectFromFileA(m_pDevice, path,
			nullptr, nullptr, 0, nullptr, &m_pEffect, &pErr))){
				if(pErr) MessageBoxA(nullptr, (LPCSTR)pErr->GetBufferPointer(), path, MB_OK);
				else MessageBoxA(nullptr, "エフェクトファイルが見つかりません", path, MB_OK);
				SAFE_RELEASE(pErr);
				return E_FAIL;
		}
	}

	// スクリーンサイズ設定
	m_screen.x = (float)width;
	m_screen.y = (float)height;

	return S_OK;
}

//====================================================================
// 解放
//====================================================================
void Sprite::Finalize(void)
{
	SAFE_RELEASE(m_pVtxBuff);
	SAFE_RELEASE(m_pEffect);
	SAFE_RELEASE(m_pDevice);
}

//====================================================================
// リストア処理
//====================================================================
void Sprite::OnLostDevice(void)
{
	m_pEffect->OnLostDevice();
}

HRESULT Sprite::OnResetDevice(void)
{
	if(FAILED(m_pEffect->OnResetDevice())) return E_FAIL;

	return S_OK;
}

//====================================================================
// 描画リストに積む
//====================================================================
void Sprite::Draw(void)
{
	if(!m_pDevice || !m_pVtxBuff || !m_pEffect){
		return;		// インターフェイスが一つでもなければ処理しない
	}

	// 頂点バッファと頂点宣言
	m_pDevice->SetStreamSource(0, m_pVtxBuff, 0, sizeof(float) * 6);
	m_pDevice->SetFVF(D3DFVF_XYZW | D3DFVF_TEX1);

	// シェーダによる描画開始
	m_pEffect->SetTechnique("tech");
	m_pEffect->Begin(nullptr, 0);
	m_pEffect->BeginPass(0);

	// 射影変換行列作成
	D3DXMATRIX proj;
	D3DXMatrixIdentity(&proj);
	proj._11 = 2.0f / m_screen.x;
	proj._22 = -2.0f / m_screen.y;
	proj._41 = -1.0f;
	proj._42 = 1.0f;
	m_pEffect->SetMatrix("proj", &proj);

	// スクリーンサイズ (テクスチャのずれ対策)
	m_pEffect->SetFloatArray("scWH", m_screen, 2);

	// ワールド変換行列作成
	D3DXMATRIX mS, mR, mT;
	D3DXMatrixScaling(&mS, m_scale.x, m_scale.y, 0.0f);
	D3DXMatrixRotationZ(&mR, m_rot);
	D3DXMatrixTranslation(&mT, m_pos.x, m_pos.y, 0.0f);
	m_pEffect->SetMatrix("world", &(mS * mR * mT));

	// uv座標
	m_pEffect->SetFloatArray("uvXY", m_uvXY, 2);
	m_pEffect->SetFloatArray("uvWH", m_uvWH, 2);

	// 色、テクスチャ
	m_pEffect->SetFloatArray("color", m_color, 4);
	m_pEffect->SetTexture("Texture", m_pTexture);
	m_pEffect->SetBool("IsTexture", (BOOL)m_pTexture);

	// 設定を反映
	m_pEffect->CommitChanges();

	// 描画
	m_pDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);

	// シェーダによる描画終了
	m_pEffect->EndPass();
	m_pEffect->End();
}

//====================================================================
// セッター
//====================================================================
void Sprite::SetPos(const D3DXVECTOR2& pos)
{
	m_pos = pos;
}

void Sprite::SetScale(const D3DXVECTOR2& scale)
{
	m_scale = scale;
}

void Sprite::SetRot(float radian)
{
	m_rot = radian;
}

void Sprite::SetUVXY(const D3DXVECTOR2& uvXY)
{
	m_uvXY = uvXY;
}

void Sprite::SetUVWH(const D3DXVECTOR2& uvWH)
{
	m_uvWH = uvWH;
}

void Sprite::SetColor(const D3DXCOLOR& color)
{
	m_color = color;
}

void Sprite::SetTexture(LPDIRECT3DTEXTURE9 pTexture)
{
	m_pTexture = pTexture;
}

//====================================================================
// ゲッター
//====================================================================
const D3DXVECTOR2& Sprite::GetPos(void)
{
	return m_pos;
}

const D3DXVECTOR2& Sprite::GetScale(void)
{
	return m_scale;
}

float Sprite::GetRot(void)
{
	return m_rot;
}

const D3DXVECTOR2& Sprite::GetUVXY(void)
{
	return m_uvXY;
}

const D3DXVECTOR2& Sprite::GetUVWH(void)
{
	return m_uvWH;
}

const D3DXCOLOR& Sprite::GetColor(void)
{
	return m_color;
}
