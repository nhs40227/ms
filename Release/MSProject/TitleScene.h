//====================================================================
//
// タイトルシーン [TitleScene.h]
//
//====================================================================
#pragma once

#include "stdafx.h"
#include "Scene.h"
#include "Sprite.h"
#include "TextBox.h"

//====================================================================
// クラス定義
//====================================================================
class TitleScene sealed : public Scene{
public:
	TitleScene();
	~TitleScene();

private:
	HRESULT Create(void)override;
	void Release(void)override;
	HRESULT Update(void)override;
	void Render(void)override;
	void OnLostDevice(void)override;
	HRESULT OnResetDevice(void)override;

private:
	Sprite* m_pTitle;	// タイトル

	Sprite* m_pGF;	// GrandFatherの意
	Sprite* m_pGM;	// GrandMotherの意
	D3DXVECTOR2 m_baseScale;
	char m_cursor : 1;		// 0 : GF, 1 : GM
	float m_textTime;		// テキスト送りの時間

	LPDIRECT3DTEXTURE9 m_pTextFrame;
	TextBox* m_pTextBox;

	BYTE m_state;

private:
	bool UpdateBegin(void);
	bool UpdateCharSelect(void);
	bool UpdateFadeOut(void);
	void SetCursor(char cursor);
};
