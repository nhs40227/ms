//====================================================================
//
// 1マスのデータ [Grid.h]
//
//====================================================================
#pragma once

#include "stdafx.h"
#include "Polygon.h"

//====================================================================
// 構造体定義
//====================================================================
struct POINT_RECT{
	POINT l;
	POINT b;
	POINT r;
	POINT t;
	POINT lb;
	POINT lt;
	POINT rb;
	POINT rt;
	POINT_RECT(const POINT& point){
		l.x = point.x - 1, l.y = point.y;
		b.x = point.x, b.y = point.y - 1;
		r.x = point.x + 1, r.y = point.y;
		t.x = point.x, t.y = point.y + 1;
		lb.x = point.x - 1, lb.y = point.y - 1;
		lt.x = point.x - 1, lt.y = point.y + 1;
		rb.x = point.x + 1, rb.y = point.y - 1;
		rt.x = point.x + 1, rt.y = point.y + 1;
	}
};

//====================================================================
// クラス定義
//====================================================================
class Grid{
public:
	Grid();
	~Grid();

	HRESULT Create(BYTE byState);
	void Draw(const D3DXMATRIX& view, const D3DXMATRIX& proj);

	Polygon3D m_polygon;

private:
	BYTE m_byState;		/* 0:壁, 1:何もない*/
	DWORD m_dwUnitId;

public:
	void SetPos(const D3DXVECTOR3& pos);
	void SetScale(const D3DXVECTOR3& scale);
	void SetColor(const D3DXCOLOR& color);
	void SetTexture(LPDIRECT3DTEXTURE9 pTexture);
	void SetState(BYTE state);
	void SetUnitId(DWORD id);

	void GetPos(D3DXVECTOR3* pOut);
	void GetScale(D3DXVECTOR3* pOut);
	BYTE GetState(void);
	DWORD GetUnitId(void);
};
