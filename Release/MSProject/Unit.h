//====================================================================
//
// ユニット[Unit.h]
//
//====================================================================
#pragma once

#include "stdafx.h"
#include "Polygon.h"

//====================================================================
// クラス定義
//====================================================================
class Unit{
public:
	static void ResetAllocateId(void);
	static void PlayerMove(BYTE byKey);
	static void PlayerAttack(void);
	static void AllDecision(void);
	static void AllUpdate(void);
	static void AllDraw(const D3DXMATRIX& view, const D3DXMATRIX& proj);

private:
	static DWORD m_dwAllocateId;
	static const DWORD m_dwMoveCount = 20;
	static const DWORD m_dwAttackCount = 60;
	static DWORD NowOnMiniGameUnit;

protected:
	static std::list<Unit*> m_list;

	enum : BYTE{
		ACTION_END = 0x01,
		ACTION_NOW = 0x02,
		ACTION_DISABLE = 0x04,
	};

public:
	Unit(float size, const D3DXVECTOR3& pos, BYTE byMiniGameType);
	~Unit();

protected:
	Polygon3D m_polygon;
	DWORD m_dwTime;

	D3DXVECTOR3 m_start;		// 移動の始点
	D3DXVECTOR3 m_end;			// 移動の終点

	BYTE m_byState;
	BYTE m_byTmpState;
	BYTE m_byFlag;
	DWORD m_id;
	POINT m_gridPoint;
	BYTE m_byMiniGameType;

	virtual BYTE Decision(void);
	virtual void Update(void);
	void Wait(void);
	void Move(void);
	void Attack(void);

public:
	void ChangeState(BYTE state, LPVOID pData);

	// セッター
	void SetGridPoint(const POINT& point);

	// ゲッター
	const POINT& GetGridPoint(void)const;
	DWORD GetId(void)const;
	BYTE GetState(void)const;
};
