//====================================================================
//
// ユニット [Unit.cpp]
//
//====================================================================
#include "Unit.h"

#include "MyMath.h"
#include "Mediator.h"
#include "MiniGameManager.h"

//====================================================================
// 静的メンバ変数
//====================================================================
std::list<Unit*> Unit::m_list;
DWORD Unit::m_dwAllocateId = 1;

//====================================================================
// 割り当て番号のリセット
//====================================================================
void Unit::ResetAllocateId(void)
{
	m_dwAllocateId = 1;
}

//====================================================================
// プレイヤーの移動
//====================================================================
void Unit::PlayerMove(BYTE byKey)
{
	/*
		byKey変数の1が立っているビットにより移動方向を設定
		下位1ビット目 : 左
		下位2ビット目 : 下
		下位3ビット目 : 右
		下位4ビット目 : 上
		1100
	*/
	auto player = *m_list.begin();
	bool bMove = false;
	// 行動可能
	if(player->m_byFlag == 0x00){
		auto& current = player->m_gridPoint;
		POINT next;
		POINT_RECT prect(current);

		// 押されたキーによって値を設定
		switch(byKey & 0x0f){
		case 0x01:// 左
			bMove = Mediator::CanMoveUnit(prect.l);
			next = prect.l;
			break;

		case 0x02:// 下
			bMove = Mediator::CanMoveUnit(prect.b);
			next = prect.b;
			break;

		case 0x03:// 左下
			bMove = Mediator::CanMoveUnit(prect.lb);
			bMove &= !Mediator::IsWall(prect.l);
			bMove &= !Mediator::IsWall(prect.b);
			next = prect.lb;
			break;

		case 0x04:// 右
			bMove = Mediator::CanMoveUnit(prect.r);
			next = prect.r;
			break;

		case 0x06:// 右下
			bMove = Mediator::CanMoveUnit(prect.rb);
			bMove &= !Mediator::IsWall(prect.r);
			bMove &= !Mediator::IsWall(prect.b);
			next = prect.rb;
			break;

		case 0x08:// 上
			bMove = Mediator::CanMoveUnit(prect.t);
			next = prect.t;
			break;

		case 0x09:// 左上
			bMove = Mediator::CanMoveUnit(prect.lt);
			bMove &= !Mediator::IsWall(prect.l);
			bMove &= !Mediator::IsWall(prect.t);
			next = prect.lt;
			break;

		case 0x0c:// 右上
			bMove = Mediator::CanMoveUnit(prect.rt);
			bMove &= !Mediator::IsWall(prect.r);
			bMove &= !Mediator::IsWall(prect.t);
			next = prect.rt;
			break;
		}

		// 移動可能
		if(bMove){
			// マップの更新
			D3DXVECTOR3 end;
			Mediator::ChangeUnitIdToMap(player->m_id, current, next, &end);

			// 自身の位置も更新
			current = next;
			player->ChangeState(1, &end);
		}
	}
}

//====================================================================
// プレイヤーが仕掛ける
//====================================================================
void Unit::PlayerAttack(void)
{
	// プレイヤー
	auto itr = m_list.begin();
	auto player = *itr;

	// プレイヤーが行動可能なら
	if(player->m_byFlag == 0x00){
		auto current = player->m_gridPoint;
		itr++;
		while(itr != m_list.end()){
			auto enemy = *itr;
			auto enemyPoint = enemy->m_gridPoint;

			// 敵がプレイヤーの周囲1マスの範囲なら
			if(abs(enemyPoint.x - current.x) < 2 && abs(enemyPoint.y - current.y) < 2){
				// 攻撃を仕掛ける
				player->m_byTmpState = 2;
				player->m_byFlag = ACTION_DISABLE;
				break;
			}

			itr++;
		}
	}
}

//====================================================================
// 全ユニットの行動を決定
//====================================================================
void Unit::AllDecision(void)
{
	BYTE byFlag = 0x00;

	auto itr = m_list.begin();
	while(itr != m_list.end()){
		auto unit = *itr;

		// 行動が終了していれば処理を飛ばす
		if(unit->m_byFlag & ACTION_END){
			itr++;
			continue;
		}
		byFlag |= ACTION_END;

		// 行動中なら処理を飛ばす
		if(unit->m_byFlag & ACTION_NOW){
			byFlag |= ACTION_NOW;
			itr++;
			continue;
		}

		// 行動は決定しているが、開始していない
		if(unit->m_byFlag & ACTION_DISABLE){
			// 行動中のユニットがいなければ
			if((byFlag & ACTION_NOW) == 0){
				// 行動できる
				unit->m_byFlag |= ACTION_NOW;
				unit->ChangeState(unit->m_byTmpState, nullptr);
			}
			return;
		}

		// プレイヤーなら
		if(itr == m_list.begin()){
			return;
		}else{
			// ここで行動を決定!!
			if(unit->Decision() >= 2){// 2以上の行動であるならば選択を中断
				return;
			}
		}

		// 次の要素へ
		itr++;
	}

	if((byFlag & ACTION_END) == 0){
		for each(auto unit in m_list){
			unit->m_byFlag = 0x00;
		}
	}
}

//====================================================================
// 全ユニットの更新処理
//====================================================================
void Unit::AllUpdate(void)
{
	auto itr = m_list.begin();
	while(itr != m_list.end()){
		auto unit = *itr;
		unit->Update();
		itr++;
	}
}

//====================================================================
// 全ユニットを描画
//====================================================================
void Unit::AllDraw(const D3DXMATRIX& view, const D3DXMATRIX& proj)
{
	// リスト内一斉描画
	auto itr = m_list.end();
	while(itr != m_list.begin()){
		itr--;
		(*itr)->m_polygon.Draw(view, proj);
	}
}

//====================================================================
// コンストラクタ、デストラクタ
//====================================================================
Unit::Unit(float size, const D3DXVECTOR3& pos, BYTE byMiniGameType)
{
	// ポリゴンの設定
	size *= 0.9f;
	m_polygon.SetScale(D3DXVECTOR3(size, size, 0.0f));
	m_polygon.SetPos(pos);
	m_polygon.SetColor(0xffffff00);

	// パラメータ設定
	m_dwTime = 0;
	m_byState = 0;
	m_byFlag = 0x00;
	m_list.push_back(this);

	// Idを割り当て
	m_id = m_dwAllocateId;
	m_dwAllocateId++;

	// グリッド位置を設定
	m_gridPoint.x = 0;
	m_gridPoint.y = 0;

	// ミニゲームのtype列挙を設定 (列挙自体はMiniGame.hで定義)
	m_byMiniGameType = byMiniGameType;

	if(m_id == 1){
		m_polygon.SetColor(0xffff0000);
	}
}

Unit::~Unit()
{
	// リストから自身を削除
	m_list.remove(this);
}

//====================================================================
// 状態変更
//====================================================================
void Unit::ChangeState(BYTE state, LPVOID pData)
{
	m_byState = state;
	switch(state){
	case 0:			// 待機状態
		m_byFlag = ACTION_END;
		m_dwTime = 0;
		break;

	case 1:			// 移動状態
		m_byFlag = ACTION_NOW;
		m_dwTime = 0;
		m_polygon.GetPos(&m_start);
		m_end = *(D3DXVECTOR3*)pData;
		break;

	case 2:
		// ミニゲームスタート
		MiniGameManager::Begin(m_byMiniGameType);
		m_dwTime = 0;
		break;
	}
}

//====================================================================
// 更新処理
//====================================================================
void Unit::Update(void)
{
	m_dwTime++;

	switch(m_byState){
	case 0:		// 立ちどまっている
		Wait();
		break;

	case 1:		// 移動
		Move();
		break;

	case 2:
		Attack();	// 攻撃
		break;
	}
}

//====================================================================
// 行動を決定
//====================================================================
BYTE Unit::Decision(void)
{
	/* 要はAI部分 */

	// プレイヤーのインスタンス
	auto player = *m_list.begin();
	auto playerPoint = player->m_gridPoint;

	// 攻撃できるか検索
	BYTE byState = 0;

	auto& current = m_gridPoint;		// 現在の位置

	// プレイヤーが周囲1マスにいるなら攻撃を仕掛ける
	if(abs(playerPoint.x - current.x) < 2 && abs(playerPoint.y - current.y) < 2){
		m_byTmpState = 2;
		m_byFlag = ACTION_DISABLE;
		return m_byTmpState;
	}

	// プレイヤーへの方向を取得
	POINT Dir;
	Dir.x = playerPoint.x - current.x;
	Dir.y = playerPoint.y - current.y;

	// 索敵範囲外
	if(abs(Dir.x) + abs(Dir.y) > 3){
		// 待機
		ChangeState(0, nullptr);
		return m_byState;
	}

	// 方向を正規化
	Dir.x = Dir.x > 0 ? 1 : Dir.x < 0 ? -1 : 0;
	Dir.y = Dir.y > 0 ? 1 : Dir.y < 0 ? -1 : 0;

	// 移動したい位置
	POINT next;
	next.x = current.x + Dir.x;
	next.y = current.y + Dir.y;

	bool bMove = false;
	// 移動したい方向が斜めの場合
	if(Dir.x && Dir.y){
		// 縦横の位置を設定
		POINT row, column;
		row.x = current.x + Dir.x, row.y = current.y;
		column.x = current.x, column.y = current.y + Dir.y;

		// 縦横が両方とも壁でなく、何もなければ移動できる
		bool IsWallRow = Mediator::IsWall(row);
		bool IsWallColumn = Mediator::IsWall(column);
		if(!IsWallRow && !IsWallColumn && Mediator::CanMoveUnit(next)){
			bMove = true;

		}else{// 斜め方向は無理だから横か縦に進もう

			if(Mediator::CanMoveUnit(row)){// 横方向OK
				next = row;
				bMove = true;
			}else if(Mediator::CanMoveUnit(column)){// 縦方向OK
				next = column;
				bMove = true;
			}
		}

	}else{	// 斜めでない場合
		bMove = Mediator::CanMoveUnit(next);
	}

	// 移動できる
	if(bMove){
		// マップの更新
		D3DXVECTOR3 end;
		Mediator::ChangeUnitIdToMap(m_id, current, next, &end);
		
		// 自身の移動
		current = next;
		ChangeState(1, &end);

		return m_byState;
	}

	// 移動もできないので待機
	ChangeState(0, nullptr);

	return m_byState;
}

//====================================================================
// 待機状態
//====================================================================
void Unit::Wait(void)
{
	// 今は何もしない
}

//====================================================================
// 移動
//====================================================================
void Unit::Move(void)
{
	float time = 1.0f / m_dwMoveCount * m_dwTime;
	D3DXVECTOR3 pos;
	MyMath::Lerp<D3DXVECTOR3>(&pos, m_start, m_end, time);
	m_polygon.SetPos(pos);

	if(time >= 1.0f){
		m_dwTime = 0;
		m_byState = 0;
		m_byFlag = ACTION_END;
	}
}

//====================================================================
// 攻撃
//====================================================================
void Unit::Attack(void)
{
	float time = 1.0f / m_dwAttackCount * m_dwTime;
	D3DXCOLOR color, start(0.0f, 0.0f, 0.0f, 1.0f), end(0.0f, 1.0f, 0.0f, 1.0f);
	MyMath::Lerp<D3DXCOLOR>(&color, start, end, time);
	m_polygon.SetColor(color);

	if(!MiniGameManager::IsPlay()){
		m_dwTime = 0;
		m_byState = 0;
		m_byFlag = ACTION_END;
	}
}

//====================================================================
// セッター
//====================================================================
void Unit::SetGridPoint(const POINT& point)
{
	m_gridPoint = point;
}

//====================================================================
// ゲッター
//====================================================================
const POINT& Unit::GetGridPoint(void)const
{
	return m_gridPoint;
}

DWORD Unit::GetId(void)const
{
	return m_id;
}

BYTE Unit::GetState(void)const
{
	return m_byState;
}
