//====================================================================
//
// ユニット管理クラス [UnitManager.cpp]
//
//====================================================================
#include "UnitManager.h"

#include "MiniGameManager.h"

//====================================================================
// 初期化
//====================================================================
void UnitManager::Init(Map* pMap)
{
	m_pMap = pMap;

	POINT gridIdx;
	D3DXVECTOR3 pos;
	float scale;

	// プレイヤーがセットされるグリッド情報を取得
	m_pMap->GetEmptyPoint(&gridIdx);
	scale = m_pMap->GetGridScale();
	m_pMap->GetPos(gridIdx, &pos);

	// プレイヤーの作成
	m_player.Create(gridIdx, pos, D3DXVECTOR3(scale, scale, 0.0f));

	m_allocateId = 0;

	m_canDecisionPlayer = true;
}

//====================================================================
// 解放
//====================================================================
void UnitManager::Release(void)
{
	for each(auto enemy in m_enemyList){
		SAFE_DELETE(enemy);
	}
	m_enemyList.clear();
}

//====================================================================
// 敵の追加
//====================================================================
void UnitManager::AddEnemy(BYTE miniGameType)
{
	// 敵をセットするマップ情報の取得
	POINT gridIdx;
	D3DXVECTOR3 pos;
	float scale;
	m_pMap->GetEmptyPoint(&gridIdx);
	m_pMap->GetPos(gridIdx, &pos);
	scale = m_pMap->GetGridScale();

	// 敵の作成
	m_allocateId++;
	auto pEnemy = new Enemy;
	pEnemy->Create(gridIdx, pos, D3DXVECTOR3(scale, scale, 0.0f),
		m_allocateId, miniGameType);
	m_enemyList.push_back(pEnemy);
}

//====================================================================
// 敵の更新処理
//====================================================================
void UnitManager::Update(void)
{
	UINT allEnd = m_enemyList.size(); // 敵の数

	// プレイヤーの更新処理
	m_player.Update();

	// プレイヤーの行動が決定していたら敵の行動を決めよう
	if(m_player.GetActionState() != Enemy::ACTION_MINIGAME){
		for each(auto enemy in m_enemyList){
			// 行動が終了していたら何もしない
			if(enemy->GetActionState() == Enemy::ACTION_END){
				continue;
			}

			// 行動を決定
			Decision(enemy);

			// ミニゲームが開始されたら終了
			if(enemy->GetActionState() == Enemy::ACTION_MINIGAME){
				break;
			}
			if(enemy->GetActionState() == Enemy::ACTION_END){
				allEnd--;
			}
		}
	}

	if(allEnd == 0){
		for each(auto enemy in m_enemyList){
			enemy->Wait();
		}
		m_canDecisionPlayer = true;
	}

	// 更新処理
	for each(auto enemy in m_enemyList){
		enemy->Update();
	}
}

//====================================================================
// プレイヤーの移動
//====================================================================
void UnitManager::MovePlayer(BYTE key)
{
	POINT moveIdx = m_player.GetGridIndex();

	switch(key){
	case 0x01: moveIdx.x--; break;
	case 0x02: moveIdx.x++; break;
	case 0x04: moveIdx.y--; break;
	case 0x08: moveIdx.y++; break;
	default: return;
	}

	if(m_pMap->GetUnitId(moveIdx) == 0 && m_pMap->GetState(moveIdx) == 0){
		// プレイヤー移動
		D3DXVECTOR3 pos;
		m_pMap->GetPos(moveIdx, &pos);
		m_player.BeginMove(moveIdx, pos);
	}
}

//====================================================================
// 敵の行動を決定
//====================================================================
void UnitManager::Decision(Enemy* pEnemy)
{
	auto playerGridIdx = m_player.GetGridIndex();
	auto enemyGridIdx = pEnemy->GetGridIndex();

	// 方向だけを表す
	POINT dir;
	dir.x = playerGridIdx.x - enemyGridIdx.x;
	dir.y = playerGridIdx.y - enemyGridIdx.y;

	// 四方四マスにプレイヤーがいる
	if(abs(dir.x) + abs(dir.y) == 1){
		// ミニゲーム開始
		MiniGameManager::Begin(pEnemy->GetMiniGameType());
		pEnemy->BeginMiniGame();
		return;
	}

	// 索敵範囲にプレイヤーがいる
	if(abs(dir.x) <= 2 && abs(dir.y) <= 2){
		// 移動できるか問い合わせ
		POINT moveIdx;
		if(CanMove(enemyGridIdx, dir, &moveIdx)){// 移動可
			// 敵の移動
			D3DXVECTOR3 pos;
			m_pMap->GetPos(moveIdx, &pos);
			pEnemy->BeginMove(moveIdx, pos);

			// マップの更新
			m_pMap->SetUnitId(enemyGridIdx, 0);
			return;
		}
	}

	// 行動終了
	pEnemy->ActionEnd();
}

//====================================================================
// 移動出来るか確認
//
// 戻り値
// true : 移動可, false : 移動不可
//
// 引数
// const POINT& idx : 敵のグリッド要素数
// POINT* pMoveIdx : 移動可能ならここに移動先の要素数を代入
//====================================================================
bool UnitManager::CanMove(const POINT& idx, const POINT& dir, POINT* pMoveIdx)
{
	POINT X = idx;
	POINT Y = idx;
	X.x += dir.x;
	Y.y += dir.y;

	// 各方向に移動できるか
	bool canMoveX = m_pMap->GetUnitId(X) != 0 && m_pMap->GetState(X) != 0;
	bool canMoveY = m_pMap->GetUnitId(Y) != 0 && m_pMap->GetState(Y) != 0;

	// 移動不可
	if(!canMoveX || !canMoveY){
		return false;
	}

	// xのほうがプレイヤーとの距離が大きい場合
	if(abs(dir.x) > abs(dir.y)){
		*pMoveIdx = canMoveX ? X : Y;
		return true;
	}

	// yのほうがプレイヤーとの距離が大きい
	if(abs(dir.x) < abs(dir.y)){
		*pMoveIdx = canMoveY ? Y : X;
		return true;
	}

	// xとyの距離が一緒
	*pMoveIdx = rand() % 2 ? X : Y;
	return true;
}

//====================================================================
// ユニットの描画
//====================================================================
void UnitManager::DrawUnit(const D3DXMATRIX& mView, const D3DXMATRIX& mProj)
{
	// 敵の描画
	for each(auto enemy in m_enemyList){
		enemy->Draw(mView, mProj);
	}

	// プレイヤーの描画
	m_player.Draw(mView, mProj);
}
