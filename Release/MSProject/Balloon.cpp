//====================================================================
//
// 風船 [Balloon.cpp]
//
//====================================================================
#include "Balloon.h"

#include "MyMath.h"

//====================================================================
// 初期化
//====================================================================
void Balloon::Init(const D3DXVECTOR2& pos, const D3DXVECTOR2& scale,
	float range, LPDIRECT3DTEXTURE9 pTexture)
{
	// 位置設定
	m_sprite.SetPos(pos);
	m_baseHeight = pos.y;

	// サイズを設定
	m_sprite.SetScale(scale);

	// ランダムで色を設定
	D3DXCOLOR color[4] = {
		D3DXCOLOR(1,0,0,1),
		D3DXCOLOR(1,1,0,1),
		D3DXCOLOR(1,0,1,1),
		D3DXCOLOR(0,1,0,1)
	};
	m_sprite.SetColor(color[rand() % 4]);

	// テクスチャ貼り付け
	m_sprite.SetTexture(pTexture);

	// 移動範囲を設定
	m_moveRange = range;

	// スピードを変更
	m_speed = 1.0f;

	// 時間の初期化
	m_time = 0.0f;
}

//====================================================================
// 移動
//====================================================================
void Balloon::Move(float speedX)
{
	auto pos = m_sprite.GetPos();
	pos.x -= speedX;
	m_time += m_speed;

	MyMath::SinWave<float>(&pos.y, m_moveRange / 2.0f, m_baseHeight, m_time);
	m_sprite.SetPos(pos);
}

//====================================================================
// 描画
//====================================================================
void Balloon::Draw(void)
{
	m_sprite.Draw();
}
