//====================================================================
//
// ミニゲーム基底クラス [MiniGame.h]
//
//====================================================================
#pragma once

#include "stdafx.h"
#include "Sprite.h"

//====================================================================
// クラス定義
//====================================================================
class MiniGame{
public:
	MiniGame();

	static HRESULT Initialize(LPDIRECT3DDEVICE9 pDevice,
		const D3DXVECTOR2& spritePos, const D3DXVECTOR2& spriteSize);
	static void Finalize(void);

protected:
	static LPDIRECT3DDEVICE9 m_pDevice;
	static D3DXVECTOR2 m_spriteSize;
	static D3DXVECTOR2 m_spritePos;

public:
	// 更新処理
	virtual void Begin(void) = 0;				// ミニゲーム開始時の初期化処理
	virtual void End(void) = 0;					// ミニゲーム終了時の解放処理
	virtual bool Update(void) = 0;				// 戻り値 true : ミニゲーム終了
	virtual void Draw(void) = 0;				// 描画
	virtual void OnLostDevice(void) = 0;		// デバイスロスト時の処理
	virtual HRESULT OnResetDevice(void) = 0;	// デバイスリセット後の処理
};
