//====================================================================
//
// キーボードの処理 [Keyboard.cpp]
//
//====================================================================
#include "Keyboard.h"

//====================================================================
// 静的メンバ変数
//====================================================================
LPDIRECTINPUTDEVICE8 Keyboard::m_pDIDevice = nullptr;
BYTE Keyboard::m_keyState[256];
BYTE Keyboard::m_keyStatePrev[256];

//====================================================================
// 初期化
//====================================================================
HRESULT Keyboard::Initialize(HWND hWnd, LPDIRECTINPUT8 pDInput)
{
	// デバイスオブジェクト作成
	if(FAILED(pDInput->CreateDevice(GUID_SysKeyboard, &m_pDIDevice, nullptr))) return E_FAIL;

	// データフォーマットを指定
	if(FAILED(m_pDIDevice->SetDataFormat(&c_dfDIKeyboard))) return E_FAIL;

	// 協調モードの設定
	if(FAILED(m_pDIDevice->SetCooperativeLevel(hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE))) return E_FAIL;

	// アクセス権獲得
	m_pDIDevice->Acquire();

	// キー情報初期化
	ZeroMemory(m_keyState, 256);
	ZeroMemory(m_keyStatePrev, 256);

	return S_OK;
}

//====================================================================
// 終了処理
//====================================================================
void Keyboard::Finalize(void)
{
	if(m_pDIDevice){
		m_pDIDevice->Unacquire();
		m_pDIDevice->Release();
		m_pDIDevice = nullptr;
	}
}

//====================================================================
// 更新処理
//====================================================================
void Keyboard::Update(void)
{
	// 直前のデータを退避
	memcpy(m_keyStatePrev, m_keyState, 256);

	// デバイスからデータを取得
	if(FAILED(m_pDIDevice->GetDeviceState(sizeof(m_keyState), m_keyState))){
		// アクセス権を獲得
		m_pDIDevice->Acquire();
	}
}

//====================================================================
// 入力情報取得
//====================================================================
// 押している
bool Keyboard::Prs(BYTE key)
{
	return m_keyState[key] & 0x80 ? true : false;
}

// 押した
bool Keyboard::Trg(BYTE key)
{
	return m_keyState[key] & 0x80 && !(m_keyStatePrev[key] & 0x80);
}

// 離した
bool Keyboard::Rls(BYTE key)
{
	return !(m_keyState[key] & 0x80) && m_keyStatePrev[key] & 0x80;
}
