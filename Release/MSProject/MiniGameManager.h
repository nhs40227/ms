//====================================================================
//
// ミニゲーム管理クラス [MiniGameManager.h]
//
//====================================================================
#pragma once

#include "stdafx.h"
#include "MiniGame.h"
#include "Sprite.h"

//====================================================================
// クラス定義
//====================================================================
class MiniGameManager{
private:
	static std::vector<MiniGame*> m_games;		// ミニゲーム配列
	static BYTE m_currentMiniGame;				// 現在プレイ中のミニゲーム
	static LPDIRECT3DDEVICE9 m_pDevice;			// 描画デバイス
	static LPDIRECT3DTEXTURE9 m_pTexture;		// ここにゲーム画面を描画
	static LPDIRECT3DSURFACE9 m_pSurface;		// 描画用サーフェイス
	static LPDIRECT3DSURFACE9 m_pDepth;			// 描画用深度サーフェイス
	static Sprite* m_pSprite;					// ミニゲームを投影するスプライト
	static D3DXVECTOR2 m_maxSpriteSize;			// スプライトの最大サイズ
	static DWORD m_dwTime;						// 時間
	static BYTE m_gameState;					// ゲームの状態

	// ミニゲームの状態列挙
	enum : BYTE{
		GAMESTATE_NOTPLAY,		// プレイしていない
		GAMESTATE_FADEIN,		// フェードイン状態
		GAMESTATE_PLAY,			// プレイ中
		GAMESTATE_FADEOUT,		// フェードアウト
	};

public:
	static HRESULT Initialize(LPDIRECT3DDEVICE9 pDevice, UINT scW, UINT scH);
	static void Finalize(void);
	static void OnLostDevice(void);
	static HRESULT OnResetDevice(void);

	static void Begin(BYTE type);
	static void Update(void);
	static void Draw(void);

	static bool IsPlay(void);

	// ミニゲーム用列挙
	enum MiniGameType : BYTE{
		MINIGAME_SWORDSMITH_EASY,
		MINIGAME_TRAINDRIVER_EASY,
		MINIGAME_PILOT_EASY,
	};
};
