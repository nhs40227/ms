//====================================================================
//
// テキスト [TextBox.cpp]
//
//====================================================================
#include "TextBox.h"

//====================================================================
// コンストラクタ
//====================================================================
TextBox::TextBox()
{
	m_pFont = nullptr;
	m_numRow = 0;
	m_charSize = 0;
}

//====================================================================
// 作成
//====================================================================
HRESULT TextBox::Create(LPDIRECT3DDEVICE9 pDevice, const RECT& rect,
	const RECT& drawArea, UINT numRow, LPCWSTR pFaceName,
	const D3DXCOLOR& color, LPDIRECT3DTEXTURE9 pTexture)
{
	// スプライトの大きさと位置を設定
	m_sprite.SetScale(D3DXVECTOR2(float(rect.right) - rect.left,
		float(rect.bottom - rect.top)));
	m_sprite.SetPos(D3DXVECTOR2((rect.left + rect.right) / 2.0f,
		(rect.top + rect.bottom) / 2.0f));

	// 描画エリアの大きさを設定
	m_drawArea = drawArea;

	// 描画領域縦サイズ
	m_numRow = numRow;
	LONG height = drawArea.bottom - drawArea.top;
	if(height <= 0){
		return E_FAIL;
	}
	m_charSize = height / m_numRow;

	// フォントインターフェイス作成
	if(FAILED(D3DXCreateFontW(pDevice, m_charSize, 0, FW_NORMAL, 1, FALSE,
		SHIFTJIS_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE,
		pFaceName, &m_pFont))) return E_FAIL;

	// 色とテクスチャを設定
	m_sprite.SetColor(color);
	m_sprite.SetTexture(pTexture);

	return S_OK;
}

//====================================================================
// 解放処理
//====================================================================
void TextBox::Release(void)
{
	SAFE_RELEASE(m_pFont);
}

//====================================================================
// リストア処理
//====================================================================
void TextBox::OnLostDevice(void)
{
	m_pFont->OnLostDevice();
}

HRESULT TextBox::OnResetDevice(void)
{
	if(FAILED(m_pFont->OnResetDevice())) return E_FAIL;

	return S_OK;
}

//====================================================================
// 文字列の登録
//====================================================================
void TextBox::AddStr(LPCWSTR str)
{
	m_list.push_back(str);
	m_strLen.push_back(lstrlenW(str) + 1);

	if(m_list.size() > m_numRow){
		for(UINT i = 0; i < m_numRow; i++){
			m_list[i] = m_list[i + 1];
			m_strLen[i] = m_strLen[i + 1];
		}
		m_list.pop_back();
		m_strLen.pop_back();
	}
}

//====================================================================
// テキストボックスのクリア
//====================================================================
void TextBox::ClearText(void)
{
	m_list.clear();
	m_strLen.clear();
}

//====================================================================
// 描画
//====================================================================
void TextBox::Draw(float time)
{
	// スプライト描画
	m_sprite.Draw();

	// 文字数の取得
	int allLength = 0;
	for each(auto len in m_strLen){
		allLength += len;
	}

	// 描画文字数を設定
	int drawLength = int(allLength * time);

	// 文字描画
	RECT rect = m_drawArea;
	int currentLength = 0;
	for(UINT i = 0; i < m_list.size(); i++){
		currentLength += m_strLen[i];

		int len = m_strLen[i];
		bool con = true;
		if(currentLength > drawLength){
			len -= currentLength - drawLength;
			con = false;
		}
		m_pFont->DrawTextW(nullptr, m_list[i], len, &rect, DT_LEFT, 0xffffffff);
		if(!con){
			break;
		}
		rect.top += m_charSize;
	}
}
