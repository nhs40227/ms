//====================================================================
//
// サンプラーステート [sampler_state.h]
//
//====================================================================
#pragma once

#include "stdafx.h"

//====================================================================
// 構造体定義
//====================================================================
struct sampler_state{
	// テクスチャアドレシングモード
	D3DTEXTUREADDRESS AddressU;
	D3DTEXTUREADDRESS AddressV;
	D3DTEXTUREADDRESS AddressW;

	// テクスチャフィルタリングモード
	D3DTEXTUREFILTERTYPE MagFilter;
	D3DTEXTUREFILTERTYPE MinFilter;
	D3DTEXTUREFILTERTYPE MipFilter;

	// 初期設定
	sampler_state() : AddressU(D3DTADDRESS_WRAP), AddressV(D3DTADDRESS_WRAP), AddressW(D3DTADDRESS_WRAP),
		MagFilter(D3DTEXF_POINT), MinFilter(D3DTEXF_POINT), MipFilter(D3DTEXF_NONE){}

	// サンプラー設定
	void Set(DWORD Sampler, LPDIRECT3DDEVICE9 pDevice){
		pDevice->SetSamplerState(Sampler, D3DSAMP_ADDRESSU, AddressU);
		pDevice->SetSamplerState(Sampler, D3DSAMP_ADDRESSV, AddressV);
		pDevice->SetSamplerState(Sampler, D3DSAMP_ADDRESSW, AddressW);

		pDevice->SetSamplerState(Sampler, D3DSAMP_MAGFILTER, MagFilter);
		pDevice->SetSamplerState(Sampler, D3DSAMP_MINFILTER, MinFilter);
		pDevice->SetSamplerState(Sampler, D3DSAMP_MIPFILTER, MipFilter);
	}

	// 初期設定に戻す
	void Reset(DWORD Sampler, LPDIRECT3DDEVICE9 pDevice){
		pDevice->SetSamplerState(Sampler, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
		pDevice->SetSamplerState(Sampler, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
		pDevice->SetSamplerState(Sampler, D3DSAMP_ADDRESSW, D3DTADDRESS_WRAP);

		pDevice->SetSamplerState(Sampler, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
		pDevice->SetSamplerState(Sampler, D3DSAMP_MINFILTER, D3DTEXF_POINT);
		pDevice->SetSamplerState(Sampler, D3DSAMP_MIPFILTER, D3DTEXF_NONE);
	}
};
