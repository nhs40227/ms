//====================================================================
//
// ジョイスティックの処理 [Joystick.cpp]
//
//====================================================================
#include "Joystick.h"

//====================================================================
// 静的メンバ変数
//====================================================================
LPDIRECTINPUT8				Joystick::m_pDirectInput = nullptr;
LPDIRECTINPUTDEVICE8*		Joystick::m_ppDIDevice = nullptr;
DIJOYSTATE*					Joystick::m_pJoyState = nullptr;
DIJOYSTATE*					Joystick::m_pJoyStatePrev = nullptr;
UINT						Joystick::m_nNumCount = 0;
UINT						Joystick::m_nMaxController = 0;

//====================================================================
// コンストラクタ、デストラクタ
//====================================================================
Joystick::Joystick()
{
	m_ppDIDevice = nullptr;
}

Joystick::~Joystick()
{

}

//====================================================================
// 初期化
//====================================================================
HRESULT Joystick::Initialize(HWND hWnd, LPDIRECTINPUT8 pDirectInput, UINT num)
{
	HRESULT hr;
	m_pDirectInput = pDirectInput;
	m_pDirectInput->AddRef();

	// ジョイスティックの数だけデータを用意
	if(!num) return S_OK;
	m_nMaxController = num;
	m_ppDIDevice = new LPDIRECTINPUTDEVICE8[num];
	for(UINT i = 0; i < num; i++){
		m_ppDIDevice[i] = nullptr;
	}
	m_pJoyState = new DIJOYSTATE[num];
	m_pJoyStatePrev = new DIJOYSTATE[num];

	// デバイスオブジェクトを作成 (接続されているジョイスティックを列挙)
	hr = pDirectInput->EnumDevices(DI8DEVCLASS_GAMECTRL, EnumDevices, nullptr, DIEDFL_ATTACHEDONLY);
	if(FAILED(hr)){
		return hr;
	}

	// ジョイスティックの数だけ処理
	for(UINT i = 0; i < m_nMaxController; i++){

		// ジョイスティックが無い場合
		if(!m_ppDIDevice[i]){
			continue;		// 処理を飛ばす
		}

		// データの初期化
		ZeroMemory(&m_pJoyState[i], sizeof(DIJOYSTATE));
		ZeroMemory(&m_pJoyStatePrev[i], sizeof(DIJOYSTATE));

		// データフォーマットを設定
		hr = m_ppDIDevice[i]->SetDataFormat(&c_dfDIJoystick);
		if(FAILED(hr)){
			return hr;
		}

		// 協調モードを設定
		hr = m_ppDIDevice[i]->SetCooperativeLevel(hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
		if(FAILED(hr)){
			return hr;
		}

		// コールバック関数によって各軸のモードを設定
		m_nNumCount = i;
		hr = m_ppDIDevice[i]->EnumObjects(EnumDeviceObjects, NULL, DIDFT_AXIS);
		if(FAILED(hr)){
			return hr;
		}

		// アクセス権を獲得
		m_ppDIDevice[i]->Acquire();
	}

	return S_OK;
}

//====================================================================
// 終了処理
//====================================================================
void Joystick::Finalize(void)
{
	SAFE_DELETE_ARRAY(m_pJoyState);
	SAFE_DELETE_ARRAY(m_pJoyStatePrev);

	for(UINT i = 0; i < m_nMaxController; i++){
		if(m_ppDIDevice[i]){
			m_ppDIDevice[i]->Unacquire();
			m_ppDIDevice[i]->Release();
			m_ppDIDevice[i] = nullptr;
		}
	}
	SAFE_DELETE_ARRAY(m_ppDIDevice);
	SAFE_RELEASE(m_pDirectInput);
}

//====================================================================
// 更新処理
//====================================================================
void Joystick::Update(void)
{
	for(UINT i = 0; i < m_nMaxController; i++){

		// ジョイスティックが無い場合
		if(!m_ppDIDevice[i]){
			continue;		// 処理を飛ばす
		}

		// 直前の情報を退避
		m_pJoyStatePrev[i] = m_pJoyState[i];

		// デバイスからデータを取得
		if(FAILED(m_ppDIDevice[i]->GetDeviceState(sizeof(DIJOYSTATE), &m_pJoyState[i]))){

			// アクセス権を獲得
			m_ppDIDevice[i]->Acquire();
		}
	}
}

//====================================================================
// ジョイスティックコールバック関数
//====================================================================
BOOL CALLBACK Joystick::EnumDevices(const DIDEVICEINSTANCE* lpddi, VOID* pvRef)
{
	HRESULT hr;
	DIDEVCAPS diDevCaps;

	// ジョイスティック用デバイスオブジェクトを作成
	hr = m_pDirectInput->CreateDevice(lpddi->guidInstance, &m_ppDIDevice[m_nNumCount], nullptr);
	if(FAILED(hr)){
		return DIENUM_CONTINUE;		// 列挙を続ける
	}

	// ジョイスティックの能力を調べる
	diDevCaps.dwSize = sizeof(DIDEVCAPS);
	hr = m_ppDIDevice[m_nNumCount]->GetCapabilities(&diDevCaps);
	if(FAILED(hr)){
		SAFE_RELEASE(m_ppDIDevice[m_nNumCount]);
		return DIENUM_CONTINUE;		// 列挙を続ける
	}

	// 規定数に達したら終了
	m_nNumCount++;
	if(m_nNumCount >= m_nMaxController){
		return DIENUM_STOP;
	}

	return DIENUM_CONTINUE;		// 列挙を続ける
}

//====================================================================
// 軸モード設定コールバック関数
//====================================================================
BOOL CALLBACK Joystick::EnumDeviceObjects(const DIDEVICEOBJECTINSTANCE* lpddoi, VOID* pvRef)
{
	HRESULT hr;

	// スティックの軸モードを変更
	DIPROPRANGE diprg = {};
	diprg.diph.dwSize = sizeof(diprg);
	diprg.diph.dwHeaderSize = sizeof(diprg.diph);
	diprg.diph.dwObj = lpddoi->dwType;
	diprg.diph.dwHow = DIPH_BYID;
	diprg.lMin = -1000;
	diprg.lMax = +1000;
	hr = m_ppDIDevice[m_nNumCount]->SetProperty(DIPROP_RANGE, &diprg.diph);
	if(FAILED(hr)){
		return DIENUM_STOP;
	}

	return DIENUM_CONTINUE;
}

//====================================================================
// 入力情報の取得
//====================================================================
// 押している
bool Joystick::Prs(UINT num, int button)
{
	if(num >= m_nMaxController || !m_ppDIDevice[num]) return false;
	return m_pJoyState[num].rgbButtons[button] & 0x80 ? true : false;
}

// 押した
bool Joystick::Trg(UINT num, int button)
{
	if(num >= m_nMaxController || !m_ppDIDevice[num]) return false;
	return m_pJoyState[num].rgbButtons[button] & 0x80 && !(m_pJoyStatePrev[num].rgbButtons[button] & 0x80);
}

// 離した
bool Joystick::Rls(UINT num, int button)
{
	if(num >= m_nMaxController || !m_ppDIDevice[num]) return false;
	return !(m_pJoyState[num].rgbButtons[button] & 0x80) && m_pJoyStatePrev[num].rgbButtons[button] & 0x80;
}

// スティック位置
LONG Joystick::LStickX(UINT num)
{
	if(num >= m_nMaxController || !m_ppDIDevice[num]) return 0;
	return m_pJoyState[num].lX;
}

LONG Joystick::LStickY(UINT num)
{
	if(num >= m_nMaxController || !m_ppDIDevice[num]) return 0;
	return m_pJoyState[num].lY;
}

LONG Joystick::RStickX(UINT num)
{
	if(num >= m_nMaxController || !m_ppDIDevice[num]) return 0;
	return m_pJoyState[num].lZ;
}

LONG Joystick::RStickY(UINT num)
{
	if(num >= m_nMaxController || !m_ppDIDevice[num]) return 0;
	return m_pJoyState[num].lRz;
}
