//====================================================================
//
// メーター [Meter.h]
//
//====================================================================
#pragma once

#include "stdafx.h"
#include "sampler_state.h"

//====================================================================
// クラス定義
//====================================================================
class Meter{
public:
	Meter();
	~Meter();

	HRESULT Initialize(LPDIRECT3DDEVICE9 pDevice, const D3DXVECTOR2& screen);
	void OnLostDevice(void);
	HRESULT OnResetDevice(void);

	void Draw(float degree, float range, float needleRadian);
	void SetPos(const D3DXVECTOR2& pos);
	void SetScale(const D3DXVECTOR2& scale);

private:
	// インターフェイス
	LPDIRECT3DDEVICE9 m_pDevice;
	LPDIRECT3DVERTEXBUFFER9 m_pVtxBuffer;
	LPD3DXEFFECT m_pEffect;

	// 変換用行列
	D3DXMATRIX m_mProj;
	D3DXMATRIX m_mWorld;

	// メーター
	LPDIRECT3DTEXTURE9 m_pMeterTex;
	sampler_state m_samplerMeter;

	// 針
	LPDIRECT3DTEXTURE9 m_pNeedleTex;
	sampler_state m_samplerNeedle;
};
