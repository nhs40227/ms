//====================================================================
//
// プレイヤー [Player.h]
//
//====================================================================
#pragma once

#include "stdafx.h"
#include "Polygon.h"

//====================================================================
// クラス定義
//====================================================================
class Player{
public:
	void Create(const POINT& gridIdx, const D3DXVECTOR3& pos,
		const D3DXVECTOR3& scale);

	void BeginMove(const POINT& moveIdx, const D3DXVECTOR3& start, const D3DXVECTOR3& end);
	void BeginMiniGame(void);
	void EndMiniGame(void);
	void Update(void);
	void Wait(void);
	void Draw(const D3DXMATRIX& mView, const D3DXMATRIX& mProj);

private:
	void Moving(void);

private:
	Polygon3D m_polygon;	// ポリゴン
	POINT m_gridIdx;		// グリッドの要素数

	DWORD m_time;
	BYTE m_actionState;

	// 移動に関するデータ
	D3DXVECTOR3 m_start;
	D3DXVECTOR3 m_end;

	enum : BYTE{
		ACTION_WAIT,
		ACTION_MOVE,
		ACTION_MINIGAME,
		ACTION_MINIGAME_WAIT,
		ACTION_END
	};

public:
	const POINT& GetGridIndex(void);
	BYTE GetActionState(void);
};
