//====================================================================
//
// ポリゴン [polygon.fx]
//
//====================================================================

//====================================================================
// 構造体定義
//====================================================================
struct psInput{
	float4 pos : POSITION;
	float2 tex : TEXCOORD;
};

//====================================================================
// グローバル変数
//====================================================================
matrix world;
matrix view;
matrix proj;

float2 uvXY;
float2 uvWH;

float4 color;

sampler s0 : register(s0);
bool IsTexture;

//====================================================================
// 頂点シェーダ
//====================================================================
psInput vs_main(float4 pos : POSITION, float2 tex : TEXCOORD)
{
	psInput Out = (psInput)0;

	Out.pos = mul(pos, world);
	Out.pos = mul(Out.pos, view);
	Out.pos = mul(Out.pos, proj);

	Out.tex = tex * uvWH + uvXY;

	return Out;
}

//====================================================================
// ピクセルシェーダ
//====================================================================
float4 ps_main(psInput In) : COLOR0
{
	float4 tex = IsTexture ? tex2D(s0, In.tex) : 1;

	return tex * color;
}

//====================================================================
// テクニック
//====================================================================
technique tech{
	pass{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader  = compile ps_3_0 ps_main();

		CullMode = NONE;
	}
}
