//====================================================================
//
// メッシュ描画 [Mesh.fx]
//
//====================================================================
matrix mWorld;
matrix mView;
matrix mProj;
matrix mWIT;

float3 light;
float4 diffuse;

sampler s0 : register(s0);
bool bTexture;

//====================================================================
// 構造体定義
//====================================================================
struct psInput{
	float4 pos : POSITION;
	float3 normal : NORMAL;
	float2 tex : TEXCOORD;
	float4 world : TEXCOORD1;
};

//====================================================================
// 描画
//====================================================================
psInput vs_main(float4 pos : POSITION, float3 normal : NORMAL, float2 tex : TEXCOORD)
{
	psInput Out = (psInput)0;

	Out.world = mul(pos, mWorld);
	Out.pos = mul(Out.world, mView);
	Out.pos = mul(Out.pos, mProj);

	Out.normal = normal;
	Out.tex = tex;

	return Out;
}

float4 ps_main(psInput In) : COLOR0
{
	float3 N = normalize(mul(In.normal, (float3x3)mWIT));
	float3 L = normalize(light - In.world.xyz);

	float4 amb = diffuse * 0.3f;
	float4 dfs = diffuse * 0.7f * max(0, dot(N, L));

	float4 texColor = bTexture ? tex2D(s0, In.tex) : 1;

	return texColor * (amb + dfs);
}

//====================================================================
// テクニック
//====================================================================
technique tech{
	pass{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader  = compile ps_3_0 ps_main();
	}
};
