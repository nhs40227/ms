//====================================================================
//
// 鋼のシェーダ [steel.fx]
//
//====================================================================
matrix mWorld;
matrix mView;
matrix mProj;
matrix mWIT;

float3 eye;
float3 light;

struct{
	float4 diffuse;
	float4 ambient;
	float4 specular;
	float4 emissive;
	float power;
}Material;

struct psInput{
	float4 pos : POSITION;
	float3 normal : NORMAL;
	float4 world : TEXCOORD0;
};

//====================================================================
// 描画
//====================================================================
psInput vs_main(float4 pos : POSITION, float3 normal : NORMAL)
{
	psInput Out = (psInput)0;

	Out.world = mul(pos, mWorld);
	Out.pos = mul(Out.world, mView);
	Out.pos = mul(Out.pos, mProj);

	Out.normal = normal;

	return Out;
}

float4 ps_main(psInput In) : COLOR0
{
	float3 N = normalize(mul(In.normal, (float3x3)mWIT));
	float3 L = normalize(light - In.world.xyz);
	float3 E = normalize(eye - In.world.xyz);
	float3 H = normalize(L + E);

	float4 amb = Material.ambient * 0.3f;
	float4 dfs = Material.diffuse * 0.7f * max(0, dot(N, L));
	float4 spc = Material.specular * pow(max(0, dot(N, H)), Material.power);

	return amb + dfs + spc;
}

//====================================================================
// テクニック
//====================================================================
technique tech{
	pass{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader  = compile ps_3_0 ps_main();

		CullMode = CW;
	}
};
