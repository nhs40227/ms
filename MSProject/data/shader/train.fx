//====================================================================
//
// 車掌のミニゲームで使用するシェーダ [train.fx]
//
//====================================================================
matrix mWorld;
matrix mView;
matrix mProj;

sampler s0 : register(s0);

struct{
	float4 diffuse;
	float4 ambient;
	float4 specular;
	float4 emissive;
	float power;
}Material;

// 拡散反射光しか使わない奴用
float4 Diffuse;

struct psInput{
	float4 pos : POSITION;
	float3 normal : NORMAL;
	float2 tex : TEXCOORD;
};

float3 light_dir = float3(1, 1, -1);
float3 eye_dir = float3(0, 1, -2);

//====================================================================
// 電車の描画
//====================================================================
psInput vs_train(float4 pos : POSITION, float3 normal : NORMAL, float2 tex : TEXCOORD)
{
	psInput Out = (psInput)0;

	Out.pos = mul(pos, mWorld);
	Out.pos = mul(Out.pos, mView);
	Out.pos = mul(Out.pos, mProj);

	Out.normal = normal;

	Out.tex = tex;

	return Out;
}

float4 ps_train(psInput In) : COLOR0
{
	float3 L = normalize(light_dir);
	float3 E = normalize(eye_dir);
	float3 H = normalize(L + E);

	float4 amb = Material.ambient * 0.3f;
	float4 dfs = Material.diffuse * 0.7f * max(0, dot(In.normal, L));
	float4 spc = Material.specular * pow(max(0, dot(In.normal, H)), Material.power);
	return amb + dfs + spc;
}

//====================================================================
// 駅の描画
//====================================================================
psInput vs_station(float4 pos : POSITION, float3 normal : NORMAL, float2 tex : TEXCOORD)
{
	psInput Out = (psInput)0;

	Out.pos = mul(pos, mWorld);
	Out.pos = mul(Out.pos, mView);
	Out.pos = mul(Out.pos, mProj);

	Out.normal = normal;

	Out.tex = tex;

	return Out;
}

float4 ps_station(psInput In) : COLOR0
{
	return Diffuse;
}

//====================================================================
// テクニック
//====================================================================
technique tech{
	pass{
		VertexShader = compile vs_3_0 vs_train();
		PixelShader  = compile ps_3_0 ps_train();

		CullMode = NONE;
	}

	pass{
		VertexShader = compile vs_3_0 vs_station();
		PixelShader  = compile ps_3_0 ps_station();
	}
}
