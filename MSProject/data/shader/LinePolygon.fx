//====================================================================
//
// ラインポリゴンの描画 [LinePolygon.fx]
//
//====================================================================
matrix mWorld;
matrix mView;
matrix mProj;

sampler s0 : register(s0);
bool bTexture;

struct psInput{
	float4 pos : POSITION;
	float2 tex : TEXCOORD;
};

// 描画
psInput vs_main(float4 pos : POSITION, float2 tex : TEXCOORD)
{
	psInput Out = (psInput)0;

	Out.pos = mul(pos, mWorld);
	Out.pos = mul(Out.pos, mView);
	Out.pos = mul(Out.pos, mProj);

	Out.tex = tex;

	return Out;
}

float4 ps_main(psInput In) : COLOR0
{
	return bTexture ? tex2D(s0, In.tex) : float4(0.5, 0.5, 0.5, 1.0);
}

//====================================================================
// テクニック
//====================================================================
technique tech{
	pass{
		VertexShader = compile vs_3_0 vs_main();
		PixelShader  = compile ps_3_0 ps_main();

		CullMode = NONE;
	}
}
