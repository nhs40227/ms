//====================================================================
//
// 1マスのデータ [Grid.cpp]
//
//====================================================================
#include "Grid.h"

//====================================================================
// コンストラクタ、デストラクタ
//====================================================================
Grid::Grid()
{
	m_byState = 0;		// 状態を初期化
	m_dwUnitId = 0;		// 0は誰もいないことを表す
}

Grid::~Grid()
{

}

//====================================================================
// 描画
//====================================================================
void Grid::Draw(const D3DXMATRIX& view, const D3DXMATRIX& proj)
{
	m_polygon.Draw(view, proj);
}

//====================================================================
// セッター
//====================================================================
void Grid::SetPos(const D3DXVECTOR3& pos)
{
	m_polygon.SetPos(pos);
}

void Grid::SetScale(const D3DXVECTOR3& scale)
{
	m_polygon.SetScale(scale);
}

void Grid::SetColor(const D3DXCOLOR& color)
{
	m_polygon.SetColor(color);
}

void Grid::SetTexture(LPDIRECT3DTEXTURE9 pTexture)
{
	m_polygon.SetTexture(pTexture);
}

void Grid::SetState(BYTE state)
{
	m_byState = state;
}

void Grid::SetUnitId(DWORD id)
{
	m_dwUnitId = id;
}

//====================================================================
// ゲッター
//====================================================================
void Grid::GetPos(D3DXVECTOR3* pOut)
{
	m_polygon.GetPos(pOut);
}

void Grid::GetScale(D3DXVECTOR3* pOut)
{
	m_polygon.GetScale(pOut);
}

BYTE Grid::GetState(void)
{
	return m_byState;
}

DWORD Grid::GetUnitId(void)
{
	return m_dwUnitId;
}
