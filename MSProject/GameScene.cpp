
//====================================================================
//
// ゲームシーン [GameScene.cpp]
//
//====================================================================
#include "GameScene.h"

#include "App.h"
#include "Keyboard.h"
#include "Joystick.h"
#include "SceneId.h"
#include "MiniGameManager.h"
#include "UnitManager.h"

//====================================================================
// コンストラクタ、デストラクタ
//====================================================================
GameScene::GameScene()
{
	m_pMap = nullptr;
	m_pUnitManager = nullptr;
}

GameScene::~GameScene()
{

}

//====================================================================
// 作成
//====================================================================
HRESULT GameScene::Create(void)
{
	// スクリーンサイズ取得
	UINT scW, scH;
	App::GetScreenSize(&scW, &scH);

	// マップ作成
	if(!(m_pMap = new Map)) return E_OUTOFMEMORY;
	m_pMap->Create(m_pDevice, scW);
	m_eye = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	// ミニゲーム初期化
	if(FAILED(MiniGameManager::Initialize(m_pDevice, scW, scH))) return E_FAIL;

	// ユニットマネージャ作成
	if(!(m_pUnitManager = new UnitManager)) return E_OUTOFMEMORY;
	m_pUnitManager->Init(m_pMap);
	m_pUnitManager->AddEnemy(MiniGameManager::MINIGAME_PILOT_EASY);
	m_pUnitManager->AddEnemy(MiniGameManager::MINIGAME_TRAINDRIVER_EASY);
	m_pUnitManager->AddEnemy(MiniGameManager::MINIGAME_SWORDSMITH_EASY);

	return S_OK;
}

//====================================================================
// 解放
//====================================================================
void GameScene::Release(void)
{
	MiniGameManager::Finalize();

	// マップ解放
	m_pMap->Release();
	SAFE_DELETE(m_pMap);

	// ユニット管理クラス解放
	if(m_pUnitManager){
		m_pUnitManager->Release();
		delete m_pUnitManager;
		m_pUnitManager = nullptr;
	}
}

//====================================================================
// 更新処理
//====================================================================
HRESULT GameScene::Update(void)
{
	// Enterキーで何度でもやり直せる
	if(Keyboard::Trg(DIK_RETURN)){
		return Scene::ChangeScene(SCENE_GAME);
	}

	// プレイヤーの移動
	BYTE byKey = 0x00;
	byKey |= Keyboard::Prs(DIK_LEFT)	|| Joystick::LStickX(0) < -500 ? 0x01 : 0x00;
	byKey |= Keyboard::Prs(DIK_DOWN)	|| Joystick::LStickY(0) > +500 ? 0x02 : 0x00;
	byKey |= Keyboard::Prs(DIK_RIGHT)	|| Joystick::LStickX(0) > +500 ? 0x04 : 0x00;
	byKey |= Keyboard::Prs(DIK_UP)		|| Joystick::LStickY(0) < -500 ? 0x08 : 0x00;

	// ユニットの更新処理
	m_pUnitManager->MovePlayer(byKey);
	m_pUnitManager->Update();

	// ミニゲーム更新処理
	MiniGameManager::Update();

	return S_OK;
}

//====================================================================
// 描画
//====================================================================
void GameScene::Render(void)
{
	// スクリーンサイズ取得
	UINT scW, scH;
	App::GetScreenSize(&scW, &scH);

	// ビュー射影変換行列
	D3DXMATRIX view, proj;
	auto look = m_eye;
	look.z += 1.0f;
	D3DXMatrixLookAtLH(&view, &m_eye, &look, &D3DXVECTOR3(0.0f, 1.0f, 0.0f));
	D3DXMatrixOrthoLH(&proj, (float)scW, (float)scH, 0.0f, 1.0f);

	// マップ描画
	m_pMap->Draw(view, proj);

	// ユニットを描画
	m_pUnitManager->DrawUnit(view, proj);

	// ミニゲーム画面描画
	MiniGameManager::Draw();
}

//====================================================================
// リストア処理
//====================================================================
void GameScene::OnLostDevice(void)
{
	MiniGameManager::OnLostDevice();
}

HRESULT GameScene::OnResetDevice(void)
{
	if(FAILED(MiniGameManager::OnResetDevice())) return E_FAIL;

	return S_OK;
}
