//====================================================================
//
// 文字列テクスチャ [StrTexture.cpp]
//
//====================================================================
#include "StrTexture.h"

//====================================================================
// 静的メンバ変数
//====================================================================
LPDIRECT3DDEVICE9 StrTexture::m_pDevice = nullptr;

//====================================================================
// 初期化
//====================================================================
HRESULT StrTexture::Initialize(LPDIRECT3DDEVICE9 pDevice)
{
	// デバイスセット
	m_pDevice = pDevice;
	m_pDevice->AddRef();

	return S_OK;
}

//====================================================================
// 終了処理
//====================================================================
void StrTexture::Finalize(void)
{
	SAFE_RELEASE(m_pDevice);
}

//====================================================================
// コンストラクタ、デストラクタ
//====================================================================
StrTexture::StrTexture()
{
	m_pTexture = nullptr;
}

StrTexture::~StrTexture()
{
	SAFE_RELEASE(m_pTexture);
}

//====================================================================
// テクスチャの作成
//====================================================================
HRESULT StrTexture::CreateTexture(LPCSTR pFaceName, UINT weight, LPCSTR str, DWORD format)
{
	// テクスチャが既にある場合は解放
	SAFE_RELEASE(m_pTexture);

	// フォント作成
	LPD3DXFONT pFont = nullptr;
	if(FAILED(D3DXCreateFontA(m_pDevice, 64, 0, weight, 0, FALSE,
		SHIFTJIS_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE, pFaceName, &pFont))) return E_FAIL;

	// 文字列の大きさを計算
	RECT rect = {0, 0, 1, 1};
	pFont->DrawTextA(nullptr, str, -1, &rect, DT_CALCRECT | format, 0x0);

	// 矩形の修正
	rect.right -= rect.left;
	rect.left = 0;
	rect.bottom -= rect.top;
	rect.top = 0;

	// テクスチャ作成
	if(FAILED(m_pDevice->CreateTexture(rect.right, rect.bottom, 1, D3DUSAGE_RENDERTARGET,
		D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &m_pTexture, nullptr))) return E_FAIL;

	// サーフェイス取得
	LPDIRECT3DSURFACE9 pSurface = nullptr;
	if(FAILED(m_pTexture->GetSurfaceLevel(0, &pSurface))) return E_FAIL;

	// ビューポート設定
	D3DVIEWPORT9 viewport = {0, 0, rect.right, rect.bottom, 0.0f, 1.0f};

	// 現在のレンダリングターゲット取得
	LPDIRECT3DSURFACE9 pOldSurface = nullptr, pOldDepth = nullptr;
	D3DVIEWPORT9 oldViewport;
	m_pDevice->GetRenderTarget(0, &pOldSurface);
	m_pDevice->GetDepthStencilSurface(&pOldDepth);
	m_pDevice->GetViewport(&oldViewport);

	// レンダリングターゲットを変更
	m_pDevice->SetRenderTarget(0, pSurface);
	m_pDevice->SetDepthStencilSurface(nullptr);
	m_pDevice->SetViewport(&viewport);

	// テクスチャをクリア
	m_pDevice->Clear(0, nullptr, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0x0, 1.0f, 0);

	// テクスチャに文字描画
	if(SUCCEEDED(m_pDevice->BeginScene())){
		m_pDevice->Clear(0, nullptr, D3DCLEAR_TARGET, 0x0, 1.0f, 0);
		pFont->DrawTextA(nullptr, str, -1, &rect, format, 0xffffffff);
		m_pDevice->EndScene();
	}

	// レンダリングターゲットをもとに戻す
	m_pDevice->SetRenderTarget(0, pOldSurface);
	m_pDevice->SetDepthStencilSurface(pOldDepth);
	m_pDevice->SetViewport(&oldViewport);

	// 解放
	SAFE_RELEASE(pFont);
	SAFE_RELEASE(pSurface);
	SAFE_RELEASE(pOldSurface);
	SAFE_RELEASE(pOldDepth);

	return S_OK;
}

//====================================================================
// テクスチャの解放
//====================================================================
void StrTexture::ReleaseTexture(void)
{
	SAFE_RELEASE(m_pTexture);
}

//====================================================================
// テクスチャ取得
//====================================================================
LPDIRECT3DTEXTURE9 StrTexture::GetTexture(void)
{
	return m_pTexture;
}
