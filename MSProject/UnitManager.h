//====================================================================
//
// ユニット管理クラス [UnitManager.h]
//
//====================================================================
#pragma once

#include "stdafx.h"
#include "Enemy.h"
#include "Player.h"
#include "Map.h"

//====================================================================
// クラス定義
//====================================================================
class UnitManager{
public:
	void Init(Map* pMap);
	void Release(void);
	void AddEnemy(BYTE miniGameType);
	void Update(void);
	void MovePlayer(BYTE key);
	void DrawUnit(const D3DXMATRIX& mView, const D3DXMATRIX& mProj);

private:
	Player m_player;
	std::list<Enemy*> m_enemyList;
	Map* m_pMap;

	DWORD m_allocateId;

private:
	void Decision(Enemy* pEnemy);
	bool CanMove(const POINT& idx, const POINT& dir, POINT* pMoveIdx);
};
