//====================================================================
//
// 刀匠のミニゲーム [Swordsmith.cpp]
//
//====================================================================
#include "Swordsmith_easy.h"

#include "Keyboard.h"

//====================================================================
// コンストラクタ
//====================================================================
Swordsmith_easy::Swordsmith_easy()
{

}

//====================================================================
// 開始
//====================================================================
void Swordsmith_easy::Begin(void)
{

}

//====================================================================
// 終了
//====================================================================
void Swordsmith_easy::End(void)
{

}

//====================================================================
// 更新処理
//====================================================================
bool Swordsmith_easy::Update(void)
{
	return true;
}

//====================================================================
// 描画処理
//====================================================================
void Swordsmith_easy::Draw(void)
{

}

//====================================================================
// リストア処理
//====================================================================
void Swordsmith_easy::OnLostDevice(void)
{

}

HRESULT Swordsmith_easy::OnResetDevice(void)
{
	return S_OK;
}
