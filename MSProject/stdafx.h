//====================================================================
//
// インクルード用 [stdafx.h]
//
//====================================================================
#pragma once

//====================================================================
// 標準ライブラリインクルード
//====================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <time.h>
#include <vector>
#include <list>
#include <array>
#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>

//====================================================================
// ライブラリリンク
//====================================================================
#pragma comment (lib, "d3d9.lib")
#pragma comment (lib, "d3dx9.lib")
#pragma comment (lib, "winmm.lib")
#pragma comment (lib, "dinput8.lib")
#pragma comment (lib, "dxguid.lib")

//====================================================================
// マクロ定義
//====================================================================
#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p) if(p){ p->Release(); p = nullptr; }
#endif

#ifndef SAFE_DELETE
#define SAFE_DELETE(p) if(p){ delete p; p = nullptr; }
#endif

#ifndef SAFE_DELETE_ARRAY
#define SAFE_DELETE_ARRAY(p) if(p){ delete[] p; p = nullptr; }
#endif

// メモリリーク検出用
#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#ifndef DBG_NEW
#define DBG_NEW new (_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DBG_NEW
#endif
#endif
