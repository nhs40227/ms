//====================================================================
//
// メーター [Meter.cpp]
//
//====================================================================
#include "Meter.h"

//====================================================================
// コンストラクタ
//====================================================================
Meter::Meter()
{
	m_pDevice = nullptr;
	m_pVtxBuffer = nullptr;
	m_pEffect = nullptr;

	D3DXMatrixIdentity(&m_mWorld);
	D3DXMatrixIdentity(&m_mProj);

	m_pMeterTex = nullptr;
	m_pNeedleTex = nullptr;
}

Meter::~Meter()
{
	SAFE_RELEASE(m_pDevice);
	SAFE_RELEASE(m_pVtxBuffer);
	SAFE_RELEASE(m_pEffect);
	SAFE_RELEASE(m_pMeterTex);
	SAFE_RELEASE(m_pNeedleTex);
}

//====================================================================
// 初期化
//====================================================================
HRESULT Meter::Initialize(LPDIRECT3DDEVICE9 pDevice, const D3DXVECTOR2& screen)
{
	// デバイスセット
	m_pDevice = pDevice;
	m_pDevice->AddRef();

	// 頂点バッファ作成
	float vtx[] = {
		-0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f,
		+0.5f, -0.5f, 0.0f, 1.0f, 1.0f, 0.0f,
		-0.5f, +0.5f, 0.0f, 1.0f, 0.0f, 1.0f,
		+0.5f, +0.5f, 0.0f, 1.0f, 1.0f, 1.0f
	};
	if(FAILED(m_pDevice->CreateVertexBuffer(sizeof(vtx), D3DUSAGE_WRITEONLY,
		D3DFVF_XYZW | D3DFVF_TEX1, D3DPOOL_MANAGED, &m_pVtxBuffer, nullptr))) return E_FAIL;
	float* p = nullptr;
	m_pVtxBuffer->Lock(0, 0, (void**)&p, 0);
	memcpy(p, vtx, sizeof(vtx));
	m_pVtxBuffer->Unlock();

	// シェーダ作成
	LPCSTR path = "data/shader/Meter.fx";
	LPD3DXBUFFER pErr = nullptr;
	if(FAILED(D3DXCreateEffectFromFileA(m_pDevice, path, nullptr, nullptr, 0, nullptr, &m_pEffect, &pErr))){
		if(pErr) MessageBoxA(nullptr, (LPCSTR)pErr->GetBufferPointer(), path, MB_OK);
		else MessageBoxA(nullptr, "エフェクトファイルが見つかりません", path, MB_OK);
		SAFE_RELEASE(pErr);
		return E_FAIL;
	}

	// 射影行列作成
	m_mProj._11 = 2.0f / screen.x;
	m_mProj._22 = -2.0f / screen.y;
	m_mProj._41 = -1.0f;
	m_mProj._42 = 1.0f;

	// テクスチャ読み込み
	if(FAILED(D3DXCreateTextureFromFileA(m_pDevice, "data/texture/halfCircle.png", &m_pMeterTex))) return E_FAIL;
	if(FAILED(D3DXCreateTextureFromFileA(m_pDevice, "data/texture/hari2.png", &m_pNeedleTex))) return E_FAIL;

	// サンプラー設定
	m_samplerMeter.MagFilter = D3DTEXF_LINEAR;
	m_samplerMeter.MinFilter = D3DTEXF_LINEAR;
	m_samplerNeedle.MagFilter = D3DTEXF_LINEAR;
	m_samplerNeedle.MinFilter = D3DTEXF_LINEAR;
	m_samplerNeedle.AddressU = D3DTADDRESS_CLAMP;
	m_samplerNeedle.AddressV = D3DTADDRESS_CLAMP;

	return S_OK;
}

//====================================================================
// リストア処理
//====================================================================
void Meter::OnLostDevice(void)
{
	m_pEffect->OnLostDevice();
}

HRESULT Meter::OnResetDevice(void)
{
	if(FAILED(m_pEffect->OnResetDevice())) return E_FAIL;

	return S_OK;
}

//====================================================================
// 描画
//====================================================================
void Meter::Draw(float degree, float range, float needleRadian)
{
	// 描画開始
	m_pEffect->Begin(nullptr, 0);
	m_pEffect->BeginPass(0);

	// 頂点データ
	m_pDevice->SetFVF(D3DFVF_XYZW | D3DFVF_TEX1);
	m_pDevice->SetStreamSource(0, m_pVtxBuffer, 0, sizeof(float) * 6);

	// 射影行列
	m_pEffect->SetMatrix("proj", &m_mProj);

	// ワールド変換
	m_pEffect->SetMatrix("world", &m_mWorld);

	// テクスチャとサンプラー
	m_pDevice->SetTexture(0, m_pMeterTex);
	m_pDevice->SetTexture(1, m_pNeedleTex);
	m_samplerMeter.Set(0, m_pDevice);
	m_samplerNeedle.Set(1, m_pDevice);

	// その他
	m_pEffect->SetFloat("degree", degree);
	m_pEffect->SetFloat("range", range);
	float mTexR[9] = {
		cosf(needleRadian), -sinf(needleRadian), 0.0f,
		sinf(needleRadian), cosf(needleRadian), 0.0f,
		0.5f,0.5f,1.0f
	};
	m_pEffect->SetFloatArray("texR", mTexR, 9);

	// 描画
	m_pEffect->CommitChanges();
	m_pDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);

	// 描画終了
	m_pEffect->EndPass();
	m_pEffect->End();

	// サンプラーを戻す
	m_samplerMeter.Reset(0, m_pDevice);
	m_samplerNeedle.Reset(1, m_pDevice);
}

//====================================================================
// セッター
//====================================================================
void Meter::SetPos(const D3DXVECTOR2& pos)
{
	m_mWorld._41 = pos.x;
	m_mWorld._42 = pos.y;
}

void Meter::SetScale(const D3DXVECTOR2& scale)
{
	m_mWorld._11 = scale.x;
	m_mWorld._22 = scale.y;
}
