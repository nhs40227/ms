//====================================================================
//
// テキストボックス [TextBox.h]
//
//====================================================================
#pragma once

#include "stdafx.h"
#include "Sprite.h"

//====================================================================
// クラス定義
//====================================================================
class TextBox{
public:
	TextBox();

	HRESULT Create(LPDIRECT3DDEVICE9 pDevice, const RECT& rect,
		const RECT& drawArea, UINT numRow, LPCWSTR pFaceName,
		const D3DXCOLOR& color, LPDIRECT3DTEXTURE9 pTexture);
	void Release(void);
	void OnLostDevice(void);
	HRESULT OnResetDevice(void);

	void AddStr(LPCWSTR str);
	void ClearText(void);
	void Draw(float time);

private:
	LPD3DXFONT m_pFont;
	Sprite m_sprite;
	RECT m_drawArea;		// 描画領域
	UINT m_numRow;			// 行数
	int m_charSize;			// 文字サイズ

	std::vector<LPCWSTR> m_list;
	std::vector<int> m_strLen;
};
