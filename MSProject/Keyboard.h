//====================================================================
//
// キーボードの処理 [Keyboard.h]
//
//====================================================================
#pragma once

#include "stdafx.h"

//====================================================================
// クラス定義
//====================================================================
class Keyboard{
public:
	static HRESULT Initialize(HWND hWnd, LPDIRECTINPUT8 pDInput);
	static void Finalize(void);
	static void Update(void);

private:
	static LPDIRECTINPUTDEVICE8 m_pDIDevice;

	static BYTE m_keyState[256];
	static BYTE m_keyStatePrev[256];

public:
	static bool Prs(BYTE key);
	static bool Trg(BYTE key);
	static bool Rls(BYTE key);
};
