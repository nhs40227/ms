//====================================================================
//
// 電車の運転手 (Easyモード) [TrainDriver_easy]
//
//====================================================================
#pragma once

#include "stdafx.h"
#include "MiniGame.h"
#include "Camera.h"
#include "Polygon.h"
#include "Meter.h"

//====================================================================
// クラス定義
//====================================================================
class TrainDriver_easy : public MiniGame{
public:
	TrainDriver_easy();

private:
	void Begin(void)override;
	void End(void)override;
	bool Update(void)override;
	void Draw(void)override;
	void OnLostDevice(void)override;
	HRESULT OnResetDevice(void)override;

private:
	DWORD m_dwTime;
	LPD3DXEFFECT m_pEffect;

	// ゲームの状態列挙体
	enum : BYTE{
		GAMESTATE_RUN,
		GAMESTATE_DERAILMENT,
	};
	BYTE m_byState;

	// カメラ
	Camera* m_pCamera;

	// 車両構造体
	struct Vehicle{
		LPD3DXMESH pMesh;
		D3DXMATRIX mWorld;
		DWORD dwNumMaterials;
		D3DMATERIAL9* pMaterials;
	};
	std::array<Vehicle*, 4> m_train;
	UINT m_lineIndex;
	float m_percentage;
	float m_vel;
	float m_trainAngle;
	LONG m_oldInput;
	D3DXVECTOR3 m_dirDerailment;
	float m_derailmentRotateY;
	float m_derailmentRotateZ;
	D3DXVECTOR3 m_posDerailment;

	// 線路構造体
	struct Line{
		D3DXVECTOR3 begin;
		D3DXVECTOR3 end;
		D3DXVECTOR3 dir;
		UINT crvIndex;
	};
	std::array<int, 10> m_curveAngle;
	std::array<Line, 2000> m_lines;
	static const UINT m_numDrawFront = 100;
	static const UINT m_numDrawBack = 3;

	// 線路の間にある板
	LPDIRECT3DTEXTURE9 m_pPlateTexture;

	// 駅
	LPD3DXMESH m_pStation;
	D3DXMATRIX m_mStation;
	D3DXMATRIX m_mBeginStation;
	D3DXMATRIX m_mEndStation;

	// 地面
	Polygon3D* m_pGround;
	LPDIRECT3DTEXTURE9 m_pGroundTex;

	// メーター
	Meter* m_pMeter;

	void BeginLine(void);
	void BeginStation(void);
	bool UpdateRun(void);
	bool UpdateDerailment(void);
	void SetTrain(void);
	void SetEye(void);
	void DrawLine(const D3DXMATRIX& mView, const D3DXMATRIX& mProj);
	void DrawTrain(const D3DXMATRIX& mView, const D3DXMATRIX& mProj);
	void DrawStation(const D3DXMATRIX& mView, const D3DXMATRIX& mProj);

#ifdef _DEBUG
	LPD3DXFONT m_pDbgFont;
#endif
};
