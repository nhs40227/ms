//====================================================================
//
// アプリケーション [App.h]
//
//====================================================================
#pragma once

#include "stdafx.h"

//====================================================================
// クラス定義
//====================================================================
class App{
public:
	App(LPCWSTR pClassName, LPCWSTR pWindowName, UINT width, UINT height, BOOL bWindow);
	virtual ~App();

	int Run(HINSTANCE hInstance, int nCmdShow);

private:
	// グラフィックス初期化と終了処理
	HRESULT InitializeGraphics(void);
	void FinalizeGraphics(void);

	// 純粋仮想関数 (派生クラスで定義)
	virtual HRESULT Initialize(void) = 0;
	virtual void Finalize(void) = 0;
	virtual HRESULT Update(void) = 0;
	virtual void Render(void) = 0;
	virtual HRESULT Restore(void) = 0;

	virtual HRESULT InitializeInput(HINSTANCE hInstance) = 0;
	virtual void FinalizeInput(void) = 0;
	virtual void UpdateInput(void) = 0;

	// ウィンドウプロシージャ
	static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	LPCWSTR					m_pClassName;
	LPCWSTR					m_pWindowName;

	static UINT				m_scWidth;
	static UINT				m_scHeight;
	static BOOL				m_bWindow;

	bool m_bRenderOK;
	static LPDIRECT3D9				m_pDirect3D;

protected:
	HWND					m_hWnd;

	static LPDIRECT3DDEVICE9		m_pDevice;
	static D3DPRESENT_PARAMETERS	m_d3dpp;

	static LPDIRECTINPUT8			m_pDirectInput;

private:
#ifdef _DEBUG
	LPD3DXFONT				m_pFPSFont;
	DWORD					m_dwCountFPS;

	// FPS描画
	void DrawFPS(void);
#endif

public:
	HWND GetWindowHandle(void);
	static LPDIRECT3DDEVICE9 GetDevice(void);
	static void GetScreenSize(UINT* pWidth, UINT* pHeight);
};
