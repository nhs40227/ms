//====================================================================
//
// Fòsm (Easy[h) [Spaceman.cpp]
//
//====================================================================
#include "Spaceman_easy.h"

#include "Keyboard.h"
#include "Joystick.h"

//====================================================================
// RXgN^
//====================================================================
Spaceman_easy::Spaceman_easy()
{
	// Fòsm
	m_pSpacemanTexture = nullptr;
	m_pSpaceman = nullptr;

	// n
	m_pEarthTexture = nullptr;
	m_pEarth = nullptr;

	// wi
	m_pSpaceTexture = nullptr;
	m_pBackground = nullptr;
}

//====================================================================
// Jn
//====================================================================
void Spaceman_easy::Begin(void)
{
	// Fòsm
	D3DXCreateTextureFromFileA(m_pDevice,"data/texture/Keisatsu.png",&m_pSpacemanTexture);
	m_pSpaceman = new Sprite;
	float spaceSize = m_spriteSize.x / 12.0f;
	m_pSpaceman->SetScale(D3DXVECTOR2(spaceSize,spaceSize));

}

//====================================================================
// I¹
//====================================================================
void Spaceman_easy::End(void)
{
}

//====================================================================
// XV
//====================================================================
bool Spaceman_easy::Update(void)
{
}

//====================================================================
// `æ
//====================================================================
void Spaceman_easy::Draw(void)
{
}

//====================================================================
// XgA
//====================================================================
void Spaceman_easy::OnLostDevice(void)
{

}

HRESULT Spaceman_easy::OnResetDevice(void)
{
	return S_OK;
}
