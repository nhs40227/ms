//====================================================================
//
// 敵 [Enemy.h]
//
//====================================================================
#pragma once

#include "stdafx.h"
#include "Polygon.h"

//====================================================================
// クラス定義
//====================================================================
class Enemy{
public:
	void Create(const POINT& gridIdx, const D3DXVECTOR3& pos,
		const D3DXVECTOR3& scale, DWORD id, BYTE miiGameType);

	void BeginMove(const POINT& moveIdx, const D3DXVECTOR3& start, const D3DXVECTOR3& end);
	void BeginMiniGame(void);
	void EndMiniGame(void);
	void MiniGameWait(void);
	void ActionEnd(void);
	void Update(void);
	void Wait(void);
	void Draw(const D3DXMATRIX& mView, const D3DXMATRIX& mProj);

private:
	void Moving(void);

private:
	Polygon3D m_polygon;
	POINT m_gridIdx;
	DWORD m_id;
	BYTE m_actionState;
	BYTE m_miniGameType;
	DWORD m_time;

	// 移動に関するデータ
	D3DXVECTOR3 m_start;
	D3DXVECTOR3 m_end;

public:
	const POINT& GetGridIndex(void);
	BYTE GetActionState(void);
	DWORD GetId(void);
	BYTE GetMiniGameType(void);

	enum eActionState{
		ACTION_WAIT,
		ACTION_MOVE,
		ACTION_MINIGAME,
		ACTION_MINIGAME_WAIT,
		ACTION_END,
	};
};
