//====================================================================
//
// MS用アプリケーションクラス [MSApp.h]
//
//====================================================================
#pragma once

#include "stdafx.h"
#include "App.h"
#include "Keyboard.h"

//====================================================================
// クラス定義
//====================================================================
class MSApp : public App{
public:
	MSApp(BOOL bWindow);
	~MSApp();

private:
	HRESULT Initialize(void)override;
	void Finalize(void)override;
	HRESULT Update(void)override;
	void Render(void)override;
	HRESULT Restore(void)override;

	HRESULT InitializeInput(HINSTANCE hInstance)override;
	void FinalizeInput(void)override;
	void UpdateInput(void)override;
};
