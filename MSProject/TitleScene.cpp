//====================================================================
//
// タイトルシーン [TitleScene.cpp]
//
//====================================================================
#include "TitleScene.h"

#include "App.h"
#include "SceneId.h"
#include "Keyboard.h"
#include "Joystick.h"
#include "MyMath.h"

//====================================================================
// コンストラクタ、デストラクタ
//====================================================================
TitleScene::TitleScene()
{
	m_pTitle = nullptr;
	m_pGF = nullptr;
	m_pGM = nullptr;

	m_pTextFrame = nullptr;
	m_pTextBox = nullptr;
}

TitleScene::~TitleScene()
{

}

//====================================================================
// 作成
//====================================================================
HRESULT TitleScene::Create(void)
{
	// スクリーンサイズ取得
	UINT scW, scH;
	App::GetScreenSize(&scW, &scH);

	// 時間の初期化
	m_dwTime = 0;

	// タイトルのスプライト作成
	if(!(m_pTitle = new Sprite)) return E_OUTOFMEMORY;
	m_pTitle->SetScale(D3DXVECTOR2(scW * 0.5f, scH * 0.5f));
	m_pTitle->SetPos(D3DXVECTOR2(scW * 0.5f, scH * 0.5f));

	// GFとGMの作成
	m_baseScale = D3DXVECTOR2(scH / 4.0f, scH / 4.0f);
	if(!(m_pGF = new Sprite)) return E_OUTOFMEMORY;
	m_pGF->SetScale(m_baseScale);
	m_pGF->SetPos(D3DXVECTOR2(scW * 0.25f, scH * 0.5f));
	m_pGF->SetColor((DWORD)0x0);

	if(!(m_pGM = new Sprite)) return E_OUTOFMEMORY;
	m_pGM->SetScale(m_baseScale);
	m_pGM->SetPos(D3DXVECTOR2(scW * 0.75f, scH * 0.5f));
	m_pGM->SetColor((DWORD)0x0);

	// テキストボックスの作成
	if(FAILED(D3DXCreateTextureFromFileA(m_pDevice,
		"data/texture/textBoxFrame.png", &m_pTextFrame))) return E_FAIL;
	if(!(m_pTextBox = new TextBox)) return E_OUTOFMEMORY;
	const LONG space = 16;
	LONG height = scH / 4;
	RECT rect = {space, 0, scW - space, scH - space};
	rect.top = rect.bottom - height;
	RECT drawArea = {rect.left + 16, rect.top + 16, rect.right - 16, rect.bottom - 16};
	if(FAILED(m_pTextBox->Create(m_pDevice, rect, drawArea, 3, L"ＭＳ ゴシック",
		0xffffffff, m_pTextFrame))) return E_FAIL;

	// タイトル画面の状態
	m_state = 0;

	return S_OK;
}

//====================================================================
// 解放
//====================================================================
void TitleScene::Release(void)
{
	if(m_pTextBox){
		m_pTextBox->Release();
		delete m_pTextBox;
		m_pTextBox = nullptr;
	}

	SAFE_DELETE(m_pTitle);
	SAFE_DELETE(m_pGF);
	SAFE_DELETE(m_pGM);
}

//====================================================================
// 更新処理
//====================================================================
HRESULT TitleScene::Update(void)
{
	switch(m_state){
	case 0:
		if(UpdateBegin()) m_state = 1;
		break;

	case 1:
		if(UpdateCharSelect()) m_state = 2;
		break;

	case 2:
		if(UpdateFadeOut()){
			return Scene::ChangeScene(SCENE_GAME);
		}
		break;
	}

	// 時間の更新
	m_dwTime++;

	return S_OK;
}

//====================================================================
// 描画
//====================================================================
void TitleScene::Render(void)
{
#define RS(State, Value) m_pDevice->SetRenderState(State, Value)
	RS(D3DRS_ALPHABLENDENABLE, TRUE);
	RS(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	RS(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	// タイトル描画
	m_pTitle->Draw();

	// GFとGMの描画
	m_pGF->Draw();
	m_pGM->Draw();

	// テキストボックス描画
	if(m_state == 1){
		m_pTextBox->Draw(m_textTime);
	}

	RS(D3DRS_ALPHABLENDENABLE, FALSE);
	RS(D3DRS_SRCBLEND, D3DBLEND_ONE);
	RS(D3DRS_DESTBLEND, D3DBLEND_ZERO);
#undef RS
}

//====================================================================
// リストア処理
//====================================================================
void TitleScene::OnLostDevice(void)
{
	// テキストボックス
	m_pTextBox->OnLostDevice();
}

HRESULT TitleScene::OnResetDevice(void)
{
	// テキストボックス
	m_pTextBox->OnResetDevice();

	return S_OK;
}

//====================================================================
// 開始時の更新処理
//====================================================================
bool TitleScene::UpdateBegin(void)
{
	if(Keyboard::Trg(DIK_RETURN) ||
		Joystick::Trg(0, 0) ||
		Joystick::Trg(0, 1) ||
		Joystick::Trg(0, 2) ||
		Joystick::Trg(0, 3))
	{
		m_pTitle->SetColor((DWORD)0x0);

		m_pGF->SetColor(0xffffffff);
		m_pGM->SetColor(0xffffffff);

		// カーソルをGFに合わせる
		m_cursor = 1;
		SetCursor(0);

		return true;
	}

	return false;
}

//====================================================================
// キャラクター選択
//====================================================================
bool TitleScene::UpdateCharSelect(void)
{
	// カーソルを切り替え
	if(Keyboard::Trg(DIK_LEFT) || Joystick::LStickX(0) < -500){
		SetCursor(0);
	}
	if(Keyboard::Trg(DIK_RIGHT) || Joystick::LStickX(0) > 500){
		SetCursor(1);
	}

	// カーソルのあるスプライトを拡縮
	if(m_textTime){
		D3DXVECTOR2 scale;
		MyMath::SinWave<D3DXVECTOR2>(&scale, m_baseScale * 0.25f, m_baseScale + m_baseScale * 0.25f, m_textTime * 360.0f - 90.0f);
		if(m_cursor){
			m_pGM->SetScale(scale);
		}else{
			m_pGF->SetScale(scale);
		}
	}

	// Enterキーを押してゲーム画面へ
	if(Keyboard::Trg(DIK_RETURN) ||
		Joystick::Trg(0, 0) ||
		Joystick::Trg(0, 1) ||
		Joystick::Trg(0, 2) ||
		Joystick::Trg(0, 3))
	{
		m_pGF->SetColor((DWORD)0x0);
		m_pGM->SetColor((DWORD)0x0);

		m_dwTime = 0;

		return true;
	}

	// テキスト送り時間更新
	m_textTime += 1.0f / 120;

	return false;
}

//====================================================================
// フェードアウト処理
//====================================================================
bool TitleScene::UpdateFadeOut(void)
{
	// TODO : 現在は無条件で状態遷移 (本当はフェードアウト)
	return true;
}

//====================================================================
// カーソルのセット
//====================================================================
void TitleScene::SetCursor(char cursor)
{
	// カーソルが変われば
	if(m_cursor & 1 ^ cursor){
		// テキストのクリア
		m_pTextBox->ClearText();

		// 文字列を設定
		if(m_cursor){
			m_pTextBox->AddStr(L">> おじいさん");
			m_pTextBox->AddStr(L"   定年退職したばかりのおじいさん。");
			m_pTextBox->AddStr(L"   好きなお酒は芋焼酎。");
		}else{
			m_pTextBox->AddStr(L">> おばあさん");
			m_pTextBox->AddStr(L"   定年退職したばかりのおばあさん。");
			m_pTextBox->AddStr(L"   好きなお酒は芋焼酎。");
		}

		// カーソルを変更
		m_cursor = ~m_cursor;

		// テキスト送りの時間を初期化
		m_textTime = 0.0f;

		// スプライトのサイズをもとに戻す
		m_pGF->SetScale(m_baseScale);
		m_pGM->SetScale(m_baseScale);
	}
}
