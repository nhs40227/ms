//====================================================================
//
// Fòsm (Easy[h) [Spaceman.h]
//
//====================================================================
#pragma once

#include "stdafx.h"
#include "MiniGame.h"

//====================================================================
// NXè`
//====================================================================
class Spaceman_easy : public MiniGame{
public:
	Spaceman_easy();

private:
	void Begin(void)override;
	void End(void)override;
	bool Update(void)override;
	void Draw(void)override;
	void OnLostDevice(void)override;
	HRESULT OnResetDevice(void)override;

private:
	// Fòsm
	LPDIRECT3DTEXTURE9 m_pSpacemanTexture;
	Sprite* m_pSpaceman;
	float m_spacemanSpeed;

	// n
	LPDIRECT3DTEXTURE9 m_pEarthTexture;
	Sprite* m_pEarth;

	// wi
	LPDIRECT3DTEXTURE9 m_pSpaceTexture;
	Sprite* m_pBackground;
};