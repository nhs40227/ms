//====================================================================
//
// 電車の運転手 (Easyモード) [TrainDriver_easy]
//
//====================================================================
#include "TrainDriver_easy.h"

#include "MyMath.h"
#include "Joystick.h"
#include "Keyboard.h"
#include "LinePolygon.h"

//====================================================================
// コンストラクタ
//====================================================================
TrainDriver_easy::TrainDriver_easy()
{
	m_pEffect = nullptr;
	ZeroMemory(&m_train, sizeof(Vehicle) * m_train.size());

	m_pCamera = nullptr;

	m_pPlateTexture = nullptr;

	m_pStation = nullptr;

	m_pGround = nullptr;
	m_pGroundTex = nullptr;

	m_pMeter = nullptr;

#ifdef _DEBUG
	m_pDbgFont = nullptr;
#endif
}

//====================================================================
// 開始
//====================================================================
void TrainDriver_easy::Begin(void)
{
	// 時間初期化
	m_dwTime = 0;

	// シェーダ作成
	LPCSTR path = "data/shader/train.fx";
	LPD3DXBUFFER pErr = nullptr;
	if(FAILED(D3DXCreateEffectFromFileA(m_pDevice, path, nullptr, nullptr, 0, nullptr, &m_pEffect, &pErr))){
		if(pErr) MessageBoxA(nullptr, (LPCSTR)pErr->GetBufferPointer(), path, MB_OK);
		else MessageBoxA(nullptr, "エフェクトファイルが見つかりません", path, MB_OK);
		SAFE_RELEASE(pErr);
	}
	m_pEffect->SetTechnique("tech");

	// ラインポリゴン作成
	LinePolygon::Initialize(m_pDevice);

	// 線路位置の設定
	BeginLine();

	// 電車と線路の位置関係を関連付ける項目の初期化
	m_lineIndex = 0;
	m_percentage = 0.0f;
	m_vel = 1.0f / (m_lines.size() / m_curveAngle.size() / 2);
	m_trainAngle = 0.0f;

	// 車両の生成
	for(UINT i = 0; i < m_train.size(); i++){
		m_train[i] = new Vehicle;
		LPD3DXBUFFER pMtrlBuffer = nullptr;
		D3DXLoadMeshFromXA("data/model/train.x", D3DXMESH_SYSTEMMEM, m_pDevice,
			nullptr, &pMtrlBuffer, nullptr, &m_train[i]->dwNumMaterials, &m_train[i]->pMesh);

		if(m_train[i]->dwNumMaterials){
			m_train[i]->pMaterials = new D3DMATERIAL9[m_train[i]->dwNumMaterials];
		}
		D3DXMATERIAL* pMtrl = (D3DXMATERIAL*)pMtrlBuffer->GetBufferPointer();
		for(DWORD j = 0; j < m_train[i]->dwNumMaterials; j++){
			ZeroMemory(&m_train[i]->pMaterials[j], sizeof(D3DMATERIAL9));
			m_train[i]->pMaterials[j] = pMtrl[j].MatD3D;
			m_train[i]->pMaterials[j].Ambient = m_train[i]->pMaterials[j].Diffuse;
		}
		SAFE_RELEASE(pMtrlBuffer);
		D3DXMatrixIdentity(&m_train[i]->mWorld);
	}
	SetTrain();

	// カメラ作成
	m_pCamera = new Camera;
	m_pCamera->SetAspect(m_spriteSize.x / m_spriteSize.y);
	SetEye();

	// 線路の間にある板
	D3DXCreateTextureFromFileA(m_pDevice, "data/texture/plate.png", &m_pPlateTexture);

	// 駅の作成
	D3DXCreateBox(m_pDevice, 10.0f, 2.0f, 2.0f, &m_pStation, nullptr);
	BeginStation();

	// 地面の作成
	D3DXCreateTextureFromFileA(m_pDevice, "data/texture/ground.png", &m_pGroundTex);
	m_pGround = new Polygon3D;
	m_pGround->SetPos(D3DXVECTOR3(0.0f, -0.1f, 0.0f));
	m_pGround->SetScale(D3DXVECTOR3(m_lines.size() * 2.0f, m_lines.size() * 2.0f, 0.0f));
	D3DXMATRIX mR;
	D3DXMatrixRotationX(&mR, D3DXToRadian(90.0f));
	m_pGround->SetRot(mR);
	m_pGround->SetUVWH(D3DXVECTOR2(50.0f, 40.0f));
	m_pGround->SetTexture(m_pGroundTex);
	m_pGround->GetSamplerState().MagFilter = D3DTEXF_LINEAR;
	m_pGround->GetSamplerState().MinFilter = D3DTEXF_LINEAR;

	// メーターの生成
	m_pMeter = new Meter;
	m_pMeter->Initialize(m_pDevice, m_spriteSize);
	D3DXVECTOR2 meterScale(m_spriteSize.x * 0.1f, m_spriteSize.x * 0.1f);
	m_pMeter->SetScale(meterScale);
	m_pMeter->SetPos(D3DXVECTOR2(m_spriteSize.x * 0.5f, meterScale.y * 0.5f));

#ifdef _DEBUG
	D3DXCreateFontA(m_pDevice, 32, 0, FW_NORMAL, 1, FALSE, SHIFTJIS_CHARSET,
		OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE,
		"ＭＳ ゴシック", &m_pDbgFont);
#endif

	m_oldInput = 0;
	m_byState = GAMESTATE_RUN;
}

//====================================================================
// 終了
//====================================================================
void TrainDriver_easy::End(void)
{
	SAFE_RELEASE(m_pEffect);
	for each(auto& vehicle in m_train){
		if(vehicle){
			auto& p = const_cast<Vehicle*>(vehicle);
			SAFE_RELEASE(p->pMesh);
			SAFE_DELETE_ARRAY(p->pMaterials);
			SAFE_DELETE(p);
		}
	}
	SAFE_DELETE(m_pCamera);

	SAFE_RELEASE(m_pPlateTexture);
	SAFE_RELEASE(m_pStation);

	SAFE_DELETE(m_pGround);
	SAFE_RELEASE(m_pGroundTex);

	SAFE_DELETE(m_pMeter);

	// ラインポリゴン終了処理
	LinePolygon::Finalize();
}

//====================================================================
// 更新処理
//====================================================================
bool TrainDriver_easy::Update(void)
{
	m_dwTime++;

	bool ret = false;
	switch(m_byState){
	case GAMESTATE_RUN:
		ret = UpdateRun();
		break;

	case GAMESTATE_DERAILMENT:
		ret = UpdateDerailment();
		break;
	}

	return ret;
}

//====================================================================
// 描画
//====================================================================
void TrainDriver_easy::Draw(void)
{
	m_pDevice->Clear(0, nullptr, D3DCLEAR_TARGET,0xff0080ff, 1.0f, 0);

	// カメラの行列を取得
	D3DXMATRIX mView, mProj;
	m_pCamera->Transform();
	m_pCamera->GetMatrix(&mView, &mProj);

	// 電車の描画開始
	m_pEffect->Begin(nullptr, 0);
	m_pEffect->BeginPass(0);
	m_pDevice->SetFVF(m_train[0]->pMesh->GetFVF());

	// 地面描画
	m_pGround->Draw(mView, mProj);

	// 線描画
	DrawLine(mView, mProj);

	// 電車の描画
	DrawTrain(mView, mProj);

	// 駅の描画
	DrawStation(mView, mProj);

	// メーター描画
	UINT idx = m_lines[m_lineIndex].crvIndex + 1;
	int angle = idx >= m_curveAngle.size() ? 0 : m_curveAngle[idx];
	m_pMeter->Draw((float)angle, 10.0f, D3DXToRadian(m_trainAngle));

#ifdef _DEBUG
	char str[256];
	RECT rect = {0, 0, (LONG)m_spriteSize.x, (LONG)m_spriteSize.y};
	sprintf_s(str, "線路:%d\nカーブ角度:%d\n電車の角度:%d", idx, angle, (int)m_trainAngle);
	m_pDbgFont->DrawTextA(nullptr, str, -1, &rect, DT_LEFT, 0xffffffff);
#endif
}

//====================================================================
// リストア処理
//====================================================================
void TrainDriver_easy::OnLostDevice(void)
{
	m_pEffect->OnLostDevice();
	LinePolygon::OnLostDevice();
	m_pMeter->OnLostDevice();
}

HRESULT TrainDriver_easy::OnResetDevice(void)
{
	if(FAILED(m_pEffect->OnResetDevice())) return E_FAIL;
	if(FAILED(LinePolygon::OnResetDevice())) return E_FAIL;
	if(FAILED(m_pMeter->OnResetDevice())) return E_FAIL;

	return S_OK;
}

//====================================================================
// 線路位置の設定
//====================================================================
void TrainDriver_easy::BeginLine(void)
{
	// 設定するカーブの角度は-90度 〜 +90度 (30度刻み)の範囲
	m_curveAngle[0] = 0;
	m_curveAngle[1] = 0;
	for(UINT i = 2; i < m_curveAngle.size(); i++){
		do{
			m_curveAngle[i] = (rand() % 7 - 3) * 30;
		}while(m_curveAngle[i] == 0 || m_curveAngle[i] == m_curveAngle[i - 1]);
	}

	// カーブの数と角度から線路の位置を決定
	m_lines[0].begin = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_lines[0].dir = D3DXVECTOR3(0.0f, 0.0f, 1.0f);
	m_lines[0].end = m_lines[0].begin + m_lines[0].dir;
	m_lines[0].crvIndex = 0;
	int absAngle = 0;
	const UINT crvInterval = m_lines.size() / m_curveAngle.size();	// 1カーブ毎の線の個数
	for(UINT i = 1; i < m_lines.size(); i++){
		// カーブ要素数を設定
		const UINT crv = i / crvInterval;
		m_lines[i].crvIndex = crv;

		float ratio = 1.0f / crvInterval * (i % crvInterval);	// 1カーブ間での割合

		// 線の始点を設定
		m_lines[i].begin = m_lines[i - 1].end;

		// 線の向きを決定
		if(i % crvInterval == 0){
			absAngle += m_curveAngle[crv];
		}
		int nextAngle = crv + 1 >= m_curveAngle.size() ? 0 : m_curveAngle[crv + 1];
		float angle;
		MyMath::Lerp<float>(&angle, (float)absAngle, float(absAngle + nextAngle), ratio);
		m_lines[i].dir.x = sinf(D3DXToRadian(angle));
		m_lines[i].dir.y = 0.0f;
		m_lines[i].dir.z = cosf(D3DXToRadian(angle));

		// 線の終点を設定
		m_lines[i].end = m_lines[i].begin + m_lines[i].dir;
	}
}

//====================================================================
// 駅の初期位置を設定
//====================================================================
void TrainDriver_easy::BeginStation(void)
{
	// 行列初期化
	D3DXMatrixIdentity(&m_mBeginStation);
	D3DXMatrixIdentity(&m_mEndStation);

	D3DXVECTOR3 pos;
	D3DXVECTOR3 dir;
	D3DXVECTOR3 cross;

	// 始発駅の設定
	pos = m_lines[m_train.size()].begin;
	dir = m_lines[m_train.size()].dir;
	D3DXVec3Cross(&cross, &D3DXVECTOR3(0,1,0), &dir);
	m_mBeginStation._41 = pos.x + cross.x * 2.0f;
	m_mBeginStation._42 = 1.0f;
	m_mBeginStation._43 = pos.z + cross.z * 2.0f;
	m_mBeginStation._11 = dir.x;
	m_mBeginStation._13 = dir.z;
	m_mBeginStation._31 = -cross.x;
	m_mBeginStation._33 = -cross.z;

	// 終点駅の設定
	pos = m_lines.back().begin;
	dir = m_lines.back().dir;
	D3DXVec3Cross(&cross, &D3DXVECTOR3(0,1,0), &dir);
	m_mEndStation._41 = pos.x + cross.x * 2.0f;
	m_mEndStation._42 = 1.0f;
	m_mEndStation._43 = pos.z + cross.z * 2.0f;
	m_mEndStation._11 = dir.x;
	m_mEndStation._13 = dir.z;
	m_mEndStation._31 = -cross.x;
	m_mEndStation._33 = -cross.z;
}

//====================================================================
// 走行時の更新処理
//====================================================================
bool TrainDriver_easy::UpdateRun(void)
{
	UINT velAmt = m_lines.size() / m_curveAngle.size() / 2;

	// 常に電車の角度を0に戻そうとする
	if(m_trainAngle > 0){
		m_trainAngle -= 0.1f;
	}else if(m_trainAngle < 0){
		m_trainAngle += 0.1f;
	}

	// 入力で角度を増加
	if(Joystick::LStickX(0) < -750 && m_oldInput > -750 || Keyboard::Trg(DIK_LEFT)){
		m_trainAngle -= 10.0f;
		if(m_trainAngle < -90.0f){
			m_trainAngle = -90.0f;
		}
	}
	if(Joystick::LStickX(0) > 750 && m_oldInput < 750 || Keyboard::Trg(DIK_RIGHT)){
		m_trainAngle += 10.0f;
		if(m_trainAngle > 90.0f){
			m_trainAngle = 90.0f;
		}
	}
	m_oldInput = Joystick::LStickX(0);

	// 1線路に対するパーセンテージを速さ分増加
	m_percentage += m_vel;

	// 1線路に対するパーセンテージが100%以上なら
	if(m_percentage >= 1.0f){
		// 最初は一定間隔で速度を上昇させる
		if(m_lineIndex < velAmt){
			m_vel += 1.0f / velAmt;
		}
		// 終点付近も一定間隔で速度を落としていく
		if(m_lineIndex >= m_lines.size() - velAmt){
			m_vel -= 1.0f / velAmt;
		}

		// 車両が対応する線路の要素数を増加 (進む)
		m_lineIndex++;

		// ゲームで使用する角度をカーブの最初に初期化
		if(m_lineIndex % (m_lines.size() / m_curveAngle.size()) == 0){
			UINT crvIndex = m_lines[m_lineIndex].crvIndex;

			// 電車の角度が現在のカーブに対応する角度の一定の範囲なら
			float crvAngle = (float)m_curveAngle[crvIndex];
			if(crvAngle >= m_trainAngle + 10.0f || crvAngle <= m_trainAngle - 10.0f){
				m_byState = GAMESTATE_DERAILMENT;
				m_dwTime = 0;
				m_dirDerailment = D3DXVECTOR3(&m_train.back()->mWorld._31);
				m_derailmentRotateY = atan2f(m_train.back()->mWorld._31, m_train.back()->mWorld._33);

				float nextAngle = crvIndex + 1 >= m_curveAngle.size() ? 0.0f : (float)m_curveAngle[crvIndex + 1];
				if(nextAngle < 0){
					m_derailmentRotateZ = D3DXToRadian(-90.0f);
					m_posDerailment = D3DXVECTOR3(+0.5f, -0.5f, 0.0f);
				}else{
					m_derailmentRotateZ = D3DXToRadian(+90.0f);
					m_posDerailment = D3DXVECTOR3(-0.5f, -0.5f, 0.0f);
				}
			}

			// 電車の角度をカーブの最初にリセット
			m_trainAngle = 0.0f;
		}

		// 新しい線に移ったからパーセンテージを戻す
		m_percentage = 0.0f;
	}

	// 終点までいったら終了
	if(m_lineIndex >= m_lines.size() - m_train.size()){
		return true;
	}

	// 電車の位置や向きを設定
	SetTrain();

	// カメラの位置を設定
	SetEye();

	return false;
}

//====================================================================
// 脱線時の更新処理
//====================================================================
bool TrainDriver_easy::UpdateDerailment(void)
{
	const DWORD dwMaxTime = 120;
	float crntTime = 1.0f / dwMaxTime * m_dwTime * 2;
	if(crntTime > 1.0f){
		crntTime = 1.0f;
	}

	// 先頭車両
	m_vel -= 2.0f / dwMaxTime;
	if(m_vel < 0.0f){
		m_vel = 0.0f;
	}
	auto pHeadVehicle = m_train.back();
	pHeadVehicle->mWorld._41 += m_dirDerailment.x * m_vel;
	pHeadVehicle->mWorld._42 += m_dirDerailment.y * m_vel;
	pHeadVehicle->mWorld._43 += m_dirDerailment.z * m_vel;
	D3DXVECTOR3 pos(&pHeadVehicle->mWorld._41);

	for(int i = m_train.size() - 2; i >= 0; i--){
		pos -= m_dirDerailment;
		m_train[i]->mWorld._41 = pos.x;
		m_train[i]->mWorld._42 = pos.y;
		m_train[i]->mWorld._43 = pos.z;
	}

	for each(auto vehicle in m_train){
		pos = D3DXVECTOR3(&vehicle->mWorld._41);
		D3DXMATRIX m, m1, m2;
		D3DXQUATERNION q, q1, q2;

		D3DXMatrixRotationZ(&m1, 0.0f);
		D3DXMatrixRotationZ(&m2, m_derailmentRotateZ);

		D3DXQuaternionRotationMatrix(&q1, &m1);
		D3DXQuaternionRotationMatrix(&q2, &m2);

		D3DXQuaternionSlerp(&q, &q1, &q2, crntTime);
		D3DXMatrixRotationQuaternion(&m, &q);
		D3DXMATRIX mRY;
		D3DXMatrixRotationY(&mRY, m_derailmentRotateY);
		vehicle->mWorld = m * mRY;
		vehicle->mWorld._41 = pos.x;
		vehicle->mWorld._42 = pos.y;
		vehicle->mWorld._43 = pos.z;
	}
	if(m_dwTime > dwMaxTime){
		return true;
	}

	return false;
}

//====================================================================
// 電車の位置や向きを設定
//====================================================================
void TrainDriver_easy::SetTrain(void)
{
	for(UINT i = 0; i < m_train.size(); i++){
		// 電車の位置を設定
		UINT startIdx = m_lineIndex + i;
		D3DXVECTOR3 trainPos;
		MyMath::Lerp<D3DXVECTOR3>(&trainPos, m_lines[startIdx].begin, m_lines[startIdx].end, m_percentage);
		m_train[i]->mWorld._41 = trainPos.x;
		m_train[i]->mWorld._42 = trainPos.y + 0.5f;
		m_train[i]->mWorld._43 = trainPos.z;

		// 電車の向きを設定
		m_train[i]->mWorld._31 = m_lines[startIdx].dir.x;
		m_train[i]->mWorld._33 = m_lines[startIdx].dir.z;
		D3DXVECTOR3 vCross;
		D3DXVec3Cross(&vCross, &D3DXVECTOR3(0,1,0), &D3DXVECTOR3(&m_train[i]->mWorld._31));
		m_train[i]->mWorld._11 = vCross.x;
		m_train[i]->mWorld._13 = vCross.z;
	}
}

//====================================================================
// カメラの位置設定
//====================================================================
void TrainDriver_easy::SetEye(void)
{
	D3DXVECTOR3 trainPos(&m_train[0]->mWorld._41);
	D3DXVECTOR3 trainDir(&m_train[0]->mWorld._31);
	D3DXVECTOR3 eye(D3DXVECTOR3(trainPos - trainDir * 5.0f));
	eye.y += 2.0f;
	m_pCamera->SetEye(eye);
	m_pCamera->SetAt(D3DXVECTOR3(trainPos));
}

//====================================================================
// 線路の描画
//====================================================================
void TrainDriver_easy::DrawLine(const D3DXMATRIX& mView, const D3DXMATRIX& mProj)
{
	int lineSize = m_lines.size();
	int size = m_numDrawBack + m_numDrawFront;

	std::vector<LinePolygon> m_lineL(size);
	std::vector<LinePolygon> m_lineR(size);
	std::vector<LinePolygon> m_plate(size);

	// 座標決定
	D3DXVECTOR3 pos, dir, cross;
	for(int i = 0; i < size; i++){
		int lineIdx = i + (int)m_lineIndex - (int)m_numDrawBack;

		// ラインの要素数が0未満の場合は線路の後ろ側を作成
		if(lineIdx < 0){
			pos = m_lines[0].begin + m_lines[0].dir * (float)lineIdx;
			dir = m_lines[0].dir;
		}else if(lineIdx >= lineSize){	// ラインの要素数がlineSize以上の場合は線路の前側を作成
			pos = pos + m_lines[lineSize - 1].dir;
			dir = m_lines[lineSize - 1].dir;
		}else{
			pos = m_lines[lineIdx].begin;
			dir = m_lines[lineIdx].dir;
		}
		D3DXVec3Cross(&cross, &D3DXVECTOR3(0.0f, 1.0f, 0.0f), &dir);
		m_lineL[i].SetPos(pos - cross * 0.3f, dir);
		m_lineR[i].SetPos(pos + cross * 0.3f, dir);

		m_plate[i].SetPos(pos - cross * 0.4f - D3DXVECTOR3(0.0f, 0.05f, 0.0f), cross);
		m_plate[i].SetScale(D3DXVECTOR2(2.0f, 0.8f));
	}

	// 線路の描画
	for(int i = 0; i < size; i++){
		m_lineL[i].Draw();
		m_lineR[i].Draw();
		m_plate[i].SetTexture(m_pPlateTexture);
		m_plate[i].Draw();
	}
	LinePolygon::DrawAll(mView, mProj);
}

//====================================================================
// 電車の描画
//====================================================================
void TrainDriver_easy::DrawTrain(const D3DXMATRIX& mView, const D3DXMATRIX& mProj)
{
	// 描画開始
	m_pEffect->Begin(nullptr, 0);
	m_pEffect->BeginPass(0);
	m_pDevice->SetFVF(m_train[0]->pMesh->GetFVF());

	// ビュー射影行列
	m_pEffect->SetMatrix("mView", &mView);
	m_pEffect->SetMatrix("mProj", &mProj);

	// ライト位置
	m_pEffect->SetFloatArray("light", D3DXVECTOR3(10.0f, 10.0f, -10.0f), 3);

	// 車両ごとに描画
	for each(auto vehicle in m_train){
		// ワールド行列
		m_pEffect->SetMatrix("mWorld", &vehicle->mWorld);

		for(UINT i = 0; i < vehicle->dwNumMaterials; i++){
			// マテリアル設定
			m_pEffect->SetValue("Material", &vehicle->pMaterials[i], sizeof(D3DMATERIAL9));

			// 描画
			m_pEffect->CommitChanges();
			vehicle->pMesh->DrawSubset(i);
		}
	}

	// シェーダ終了
	m_pEffect->EndPass();
	m_pEffect->End();
}

//====================================================================
// 駅の描画
//====================================================================
void TrainDriver_easy::DrawStation(const D3DXMATRIX& mView, const D3DXMATRIX& mProj)
{
	// 描画開始
	m_pEffect->Begin(nullptr, 0);
	m_pEffect->BeginPass(1);
	m_pDevice->SetFVF(m_pStation->GetFVF());

	// ビュー射影行列
	m_pEffect->SetMatrix("mView", &mView);
	m_pEffect->SetMatrix("mProj", &mProj);

	// 色
	m_pEffect->SetFloatArray("Diffuse", D3DXCOLOR(1.0f, 1.0f, 0.0f, 1.0f), 4);

	//--------------------------------------
	// 始発駅を描画
	//--------------------------------------
	// ワールド行列
	m_pEffect->SetMatrix("mWorld", &m_mBeginStation);

	// 描画
	m_pEffect->CommitChanges();
	m_pStation->DrawSubset(0);

	//--------------------------------------
	// 終着駅を描画
	//--------------------------------------
	// ワールド行列
	m_pEffect->SetMatrix("mWorld", &m_mEndStation);

	// 描画
	m_pEffect->CommitChanges();
	m_pStation->DrawSubset(0);

	// シェーダ終了
	m_pEffect->EndPass();
	m_pEffect->End();
}
