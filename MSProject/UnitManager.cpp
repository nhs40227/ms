//====================================================================
//
// ユニット管理クラス [UnitManager.cpp]
//
//====================================================================
#include "UnitManager.h"

#include "MiniGameManager.h"

//====================================================================
// 初期化
//====================================================================
void UnitManager::Init(Map* pMap)
{
	m_pMap = pMap;

	POINT gridIdx;
	D3DXVECTOR3 pos;
	float scale;

	// プレイヤーがセットされるグリッド情報を取得
	m_pMap->GetEmptyPoint(&gridIdx);
	scale = m_pMap->GetGridScale();
	m_pMap->GetPos(gridIdx, &pos);

	// プレイヤーの作成
	m_player.Create(gridIdx, pos, D3DXVECTOR3(scale, scale, 0.0f));

	m_allocateId = 0;
}

//====================================================================
// 解放
//====================================================================
void UnitManager::Release(void)
{
	for each(auto enemy in m_enemyList){
		SAFE_DELETE(enemy);
	}
	m_enemyList.clear();
}

//====================================================================
// 敵の追加
//====================================================================
void UnitManager::AddEnemy(BYTE miniGameType)
{
	// 敵をセットするマップ情報の取得
	POINT gridIdx;
	D3DXVECTOR3 pos;
	float scale;
	m_pMap->GetEmptyPoint(&gridIdx);
	m_pMap->GetPos(gridIdx, &pos);
	scale = m_pMap->GetGridScale();

	// 敵の作成
	m_allocateId++;
	auto pEnemy = new Enemy;
	pEnemy->Create(gridIdx, pos, D3DXVECTOR3(scale, scale, 0.0f),
		m_allocateId, miniGameType);
	m_enemyList.push_back(pEnemy);
}

//====================================================================
// 敵の更新処理
//====================================================================
void UnitManager::Update(void)
{
	UINT numActionEnd = 0;
	BYTE canBeginMiniGame = 0x01;

	// プレイヤーの更新処理
	m_player.Update();

	if(m_player.GetActionState() != Enemy::ACTION_END){
		canBeginMiniGame &= 0x00;
	}

	// プレイヤーの行動が特定の状態なら敵の行動を決める
	if(m_player.GetActionState() == Enemy::ACTION_END || m_player.GetActionState() == Enemy::ACTION_MOVE){
		for each(auto enemy in m_enemyList){
			// まだ行動が決定していない
			if(enemy->GetActionState() == Enemy::ACTION_WAIT){
				// 行動を決定
				Decision(enemy);
			}

			// 現在の行動の状態によって変化
			if(enemy->GetActionState() == Enemy::ACTION_MOVE){
				canBeginMiniGame &= 0x00;
			}else if(enemy->GetActionState() == Enemy::ACTION_MINIGAME_WAIT){
				if(canBeginMiniGame){
					MiniGameManager::Begin(enemy->GetMiniGameType());
					enemy->BeginMiniGame();
				}
				break;
			}else if(enemy->GetActionState() == Enemy::ACTION_MINIGAME){
				if(!MiniGameManager::IsPlay()){
					enemy->EndMiniGame();
				}
				break;
			}else if(enemy->GetActionState() == Enemy::ACTION_END){
				numActionEnd++;
			}
		}
	}

	// 敵の更新処理
	for each(auto enemy in m_enemyList){
		enemy->Update();
	}

	// 敵全員の行動が終了していればプレイヤーと敵の状態を待機状態に
	if(numActionEnd == m_enemyList.size()){
		for each(auto enemy in m_enemyList){
			enemy->Wait();
		}
		m_player.Wait();
	}
}

//====================================================================
// プレイヤーの移動
//====================================================================
void UnitManager::MovePlayer(BYTE key)
{
	if(m_player.GetActionState() != Enemy::ACTION_WAIT){
		return;
	}

	POINT moveIdx = m_player.GetGridIndex();

	switch(key){
	case 0x01: moveIdx.x--; break;
	case 0x02: moveIdx.y--; break;
	case 0x04: moveIdx.x++; break;
	case 0x08: moveIdx.y++; break;
	default: return;
	}

	if(m_pMap->GetUnitId(moveIdx) == 0 && m_pMap->GetState(moveIdx) == 1){
		// プレイヤー移動
		D3DXVECTOR3 start, end;
		m_pMap->GetPos(m_player.GetGridIndex(), &start);
		m_pMap->GetPos(moveIdx, &end);
		m_player.BeginMove(moveIdx, start, end);
	}
}

//====================================================================
// 敵の行動を決定
//====================================================================
void UnitManager::Decision(Enemy* pEnemy)
{
	auto playerGridIdx = m_player.GetGridIndex();
	const auto enemyGridIdx = pEnemy->GetGridIndex();

	// 方向だけを表す
	POINT dir;
	dir.x = playerGridIdx.x - enemyGridIdx.x;
	dir.y = playerGridIdx.y - enemyGridIdx.y;

	// 四方四マスにプレイヤーがいる
	if(abs(dir.x) + abs(dir.y) == 1){
		// ミニゲーム待機状態に
		pEnemy->MiniGameWait();
		return;
	}

	// 索敵範囲にプレイヤーがいる
	if(abs(dir.x) <= 2 && abs(dir.y) <= 2){
		// 移動できるか問い合わせ
		POINT moveIdx;
		if(CanMove(enemyGridIdx, dir, &moveIdx)){// 移動可
			// 敵の移動
			D3DXVECTOR3 start, end;
			m_pMap->GetPos(enemyGridIdx, &start);
			m_pMap->GetPos(moveIdx, &end);
			pEnemy->BeginMove(moveIdx, start, end);

			// マップの更新
			m_pMap->SetUnitId(enemyGridIdx, 0);
			m_pMap->SetUnitId(moveIdx, pEnemy->GetId());
			return;
		}
	}

	// 行動終了
	pEnemy->ActionEnd();
}

//====================================================================
// 移動出来るか確認
//
// 戻り値
// true : 移動可, false : 移動不可
//
// 引数
// const POINT& idx : 敵のグリッド要素数
// POINT* pMoveIdx : 移動可能ならここに移動先の要素数を代入
//====================================================================
bool UnitManager::CanMove(const POINT& idx, const POINT& dir, POINT* pMoveIdx)
{
	POINT X = idx;
	POINT Y = idx;
	X.x += dir.x / (dir.x ? abs(dir.x) : 1);
	Y.y += dir.y / (dir.y ? abs(dir.y) : 1);

	// 各方向に移動できるか
	bool canMoveX = m_pMap->GetUnitId(X) == 0 && m_pMap->GetState(X) == 1;
	bool canMoveY = m_pMap->GetUnitId(Y) == 0 && m_pMap->GetState(Y) == 1;

	// x方向にもy方向にも進める
	if(canMoveX && canMoveY){
		if(abs(dir.x) > abs(dir.y)){
			*pMoveIdx = X;
		}else if(abs(dir.x) > abs(dir.y)){
			*pMoveIdx = Y;
		}else{
			*pMoveIdx = rand() % 2 ? X : Y;
		}
		return true;
	}

	// x方向かy方向のどちらかに進める
	else if(canMoveX || canMoveY){
		*pMoveIdx = canMoveX ? X : Y;
		return true;
	}

	// 移動不可
	return false;
}

//====================================================================
// ユニットの描画
//====================================================================
void UnitManager::DrawUnit(const D3DXMATRIX& mView, const D3DXMATRIX& mProj)
{
	// 敵の描画
	for each(auto enemy in m_enemyList){
		enemy->Draw(mView, mProj);
	}

	// プレイヤーの描画
	m_player.Draw(mView, mProj);
}
