//====================================================================
//
// シーンクラス [Scene.h]
//
//====================================================================
#pragma once

#include "stdafx.h"

//====================================================================
// クラス定義
//====================================================================
class Scene{
public:
	static HRESULT Initialize(LPDIRECT3DDEVICE9 pDevice);
	static void Finalize(void);
	static HRESULT UpdateScene(void);
	static void RenderScene(void);
	static void OnLostDeviceScene(void);
	static HRESULT OnResetDeviceScene(void);

	static void RegisterScene(Scene* pScene);
	static HRESULT ChangeScene(UINT id);

protected:
	static LPDIRECT3DDEVICE9 m_pDevice;

private:
	static std::vector<Scene*> m_sceneArray;
	static UINT m_currentId;

public:
	Scene();
	virtual ~Scene();

private:
	// 純粋仮想関数 (派生先で定義してね)
	virtual HRESULT Create(void) = 0;
	virtual void Release(void) = 0;
	virtual HRESULT Update(void) = 0;
	virtual void Render(void) = 0;
	virtual void OnLostDevice(void) = 0;
	virtual HRESULT OnResetDevice(void) = 0;

protected:
	// 使いそうな変数をはじめから定義
	DWORD m_dwTime;
};
