//====================================================================
//
// プレイヤー [Player.cpp]
//
//====================================================================
#include "Player.h"

#include "MyMath.h"

//====================================================================
// 作成
//====================================================================
void Player::Create(const POINT& gridIdx, const D3DXVECTOR3& pos,
	const D3DXVECTOR3& scale)
{
	m_gridIdx = gridIdx;

	// 座標を設定
	m_polygon.SetPos(pos);

	// 大きさを設定
	m_polygon.SetScale(scale);

	// TODO : 本来はテクスチャを貼り付け
	m_polygon.SetTexture(nullptr);
	m_polygon.SetColor(0xffff0000);

	// 状態を初期化
	m_time = 0;
	m_actionState = ACTION_WAIT;
}

//====================================================================
// 移動開始
//====================================================================
void Player::BeginMove(const POINT& moveIdx, const D3DXVECTOR3& start, const D3DXVECTOR3& end)
{
	if(m_actionState != ACTION_WAIT){
		return;
	}


	m_gridIdx = moveIdx;

	m_start = start;
	m_end = end;

	m_actionState = ACTION_MOVE;

	m_time = 0;
}

//====================================================================
// ミニゲーム開始
//====================================================================
void Player::BeginMiniGame(void)
{
	m_actionState = ACTION_MINIGAME;
}

//====================================================================
// ミニゲーム終了
//====================================================================
void Player::EndMiniGame(void)
{
	m_actionState = ACTION_END;
}

//====================================================================
// 更新処理
//====================================================================
void Player::Update(void)
{
	m_time++;

	switch(m_actionState){
	case ACTION_MOVE:
		Moving();
		break;
	}
}

//====================================================================
// 移動中
//====================================================================
void Player::Moving(void)
{
	const DWORD maxMoveFrame = 30;
	D3DXVECTOR3 pos;
	MyMath::Lerp<D3DXVECTOR3>(&pos, m_start, m_end, 1.0f / maxMoveFrame * m_time);
	m_polygon.SetPos(pos);

	if(m_time >= maxMoveFrame){
		m_actionState = ACTION_END;
	}
}

//====================================================================
// 待ち状態に設定
//====================================================================
void Player::Wait(void)
{
	if(m_actionState == ACTION_END){
		m_actionState = ACTION_WAIT;
	}
}

//====================================================================
// 描画
//====================================================================
void Player::Draw(const D3DXMATRIX& mView, const D3DXMATRIX& mProj)
{
	m_polygon.Draw(mView, mProj);
}

//====================================================================
// ゲッター
//====================================================================
const POINT& Player::GetGridIndex(void)
{
	return m_gridIdx;
}

BYTE Player::GetActionState(void)
{
	return m_actionState;
}
