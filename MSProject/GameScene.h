//====================================================================
//
// ゲームシーン [GameScene.h]
//
//====================================================================
#pragma once

#include "stdafx.h"
#include "Scene.h"

#include "Polygon.h"
#include "Map.h"
#include "Sprite.h"

#include "UnitManager.h"

//====================================================================
// クラス定義
//====================================================================
class GameScene sealed : public Scene{
public:
	GameScene();
	~GameScene();

private:
	HRESULT Create(void)override;
	void Release(void)override;
	HRESULT Update(void)override;
	void Render(void)override;
	void OnLostDevice(void)override;
	HRESULT OnResetDevice(void)override;

	Map* m_pMap;
	UnitManager* m_pUnitManager;
	D3DXVECTOR3 m_eye;
};
