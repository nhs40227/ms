//====================================================================
//
// パイロット (Easyモード) [Pilot.h]
//
//====================================================================
#pragma once

#include "stdafx.h"
#include "MiniGame.h"

#include "Balloon.h"

//====================================================================
// クラス定義
//====================================================================
class Pilot_easy : public MiniGame{
public:
	Pilot_easy();

private:
	void Begin(void)override;
	void End(void)override;
	bool Update(void)override;
	void Draw(void)override;
	void OnLostDevice(void)override;
	HRESULT OnResetDevice(void)override;

private:
	DWORD m_dwTime;
	const DWORD m_scrollFrame;
	float m_maxDistance;
	float m_crntPos;

	// 飛行機
	LPDIRECT3DTEXTURE9 m_pPlaneTexture;
	Sprite* m_pPlane;
	float m_planeAccel;
	const float m_accelAmt;

	// 風船関係
	static const UINT m_numBalloon = 20;
	LPDIRECT3DTEXTURE9 m_pBalloonTexture;
	std::array<Balloon*, m_numBalloon> m_balloonAry;

	// 背景
	LPDIRECT3DTEXTURE9 m_pSkyTexture;
	Sprite* m_pBackground;
};
