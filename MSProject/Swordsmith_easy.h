//====================================================================
//
// 刀匠のミニゲーム [Swordsmith.h]
//
//====================================================================
#pragma once

#include "stdafx.h"
#include "MiniGame.h"
#include "StrTexture.h"
#include "Sprite.h"

//====================================================================
// クラス定義
//====================================================================
class Swordsmith_easy : public MiniGame{
public:
	Swordsmith_easy();

private:
	void Begin(void)override;
	void End(void)override;
	bool Update(void)override;
	void Draw(void)override;
	void OnLostDevice(void)override;
	HRESULT OnResetDevice(void)override;

private:

};
