//====================================================================
//
// ミニゲームの管理クラス [MiniGameManager.cpp]
//
//====================================================================
#include "MiniGameManager.h"

#include "MyMath.h"
#include "Swordsmith_easy.h"
#include "TrainDriver_easy.h"
#include "Pilot_easy.h"

//====================================================================
// マクロ定義
//====================================================================
#define TEXTURE_SIZE 1024

//====================================================================
// 静的メンバ変数
//====================================================================
std::vector<MiniGame*> MiniGameManager::m_games;
BYTE MiniGameManager::m_currentMiniGame = 0;
LPDIRECT3DDEVICE9 MiniGameManager::m_pDevice = nullptr;
LPDIRECT3DTEXTURE9 MiniGameManager::m_pTexture = nullptr;
LPDIRECT3DSURFACE9 MiniGameManager::m_pSurface = nullptr;
LPDIRECT3DSURFACE9 MiniGameManager::m_pDepth = nullptr;
Sprite* MiniGameManager::m_pSprite = nullptr;
D3DXVECTOR2 MiniGameManager::m_maxSpriteSize(0,0);
DWORD MiniGameManager::m_dwTime = 0;
BYTE MiniGameManager::m_gameState = GAMESTATE_NOTPLAY;

//====================================================================
// 初期化
//====================================================================
HRESULT MiniGameManager::Initialize(LPDIRECT3DDEVICE9 pDevice, UINT scW, UINT scH)
{
	// デバイスセット
	m_pDevice = pDevice;
	m_pDevice->AddRef();

	// テクスチャやサーフェイスの作成
	if(FAILED(m_pDevice->CreateTexture(TEXTURE_SIZE, TEXTURE_SIZE, 1, D3DUSAGE_RENDERTARGET,
		D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &m_pTexture, nullptr))) return E_FAIL;
	if(FAILED(m_pTexture->GetSurfaceLevel(0, &m_pSurface))) return E_FAIL;
	if(FAILED(m_pDevice->CreateDepthStencilSurface(TEXTURE_SIZE, TEXTURE_SIZE, D3DFMT_D16,
		D3DMULTISAMPLE_NONE, 0, TRUE, &m_pDepth, nullptr))) return E_FAIL;

	// スプライトの作成
	if(!(m_pSprite = new Sprite)) return E_OUTOFMEMORY;
	m_pSprite->SetScale(D3DXVECTOR2(0, 0));
	m_pSprite->SetPos(D3DXVECTOR2(scW * 0.5f, scH * 0.5f));
	m_pSprite->SetTexture(m_pTexture);
	m_maxSpriteSize = D3DXVECTOR2((float)scW, (float)scH);

	// ミニゲーム全体の初期化
	if(FAILED(MiniGame::Initialize(m_pDevice, m_pSprite->GetPos(), m_maxSpriteSize))) return E_FAIL;

	//--------------------------------------
	// ミニゲームを生成
	m_games.push_back(new Swordsmith_easy);
	m_games.push_back(new TrainDriver_easy);
	m_games.push_back(new Pilot_easy);
	// ここまで
	//--------------------------------------

	// パラメータの初期化
	m_currentMiniGame = 0;
	m_dwTime = 0;
	m_gameState = GAMESTATE_NOTPLAY;

	return S_OK;
}

//====================================================================
// 終了処理
//====================================================================
void MiniGameManager::Finalize(void)
{
	// ミニゲームを削除
	for each(auto game in m_games){
		game->End();
		SAFE_DELETE(game);
	}
	m_games.clear();

	// ミニゲーム全体の終了処理
	MiniGame::Finalize();

	// スプライト削除
	SAFE_DELETE(m_pSprite);

	// テクスチャやサーフェイスの解放
	SAFE_RELEASE(m_pTexture);
	SAFE_RELEASE(m_pSurface);
	SAFE_RELEASE(m_pDepth);
}

//====================================================================
// リストア処理
//====================================================================
void MiniGameManager::OnLostDevice(void)
{
	// テクスチャやサーフェイスの解放
	SAFE_RELEASE(m_pTexture);
	SAFE_RELEASE(m_pSurface);
	SAFE_RELEASE(m_pDepth);

	if(m_gameState != GAMESTATE_NOTPLAY){
		m_games[m_currentMiniGame]->OnLostDevice();
	}
}

HRESULT MiniGameManager::OnResetDevice(void)
{
	// テクスチャやサーフェイスの作り直し
	if(FAILED(m_pDevice->CreateTexture(TEXTURE_SIZE, TEXTURE_SIZE, 1, D3DUSAGE_RENDERTARGET,
		D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &m_pTexture, nullptr))) return E_FAIL;
	if(FAILED(m_pTexture->GetSurfaceLevel(0, &m_pSurface))) return E_FAIL;
	if(FAILED(m_pDevice->CreateDepthStencilSurface(TEXTURE_SIZE, TEXTURE_SIZE, D3DFMT_D16,
		D3DMULTISAMPLE_NONE, 0, TRUE, &m_pDepth, nullptr))) return E_FAIL;

	// スプライトに作り直したテクスチャを渡す
	m_pSprite->SetTexture(m_pTexture);

	if(m_gameState != GAMESTATE_NOTPLAY){
		if(FAILED(m_games[m_currentMiniGame]->OnResetDevice())) return E_FAIL;
	}

	return S_OK;
}

//====================================================================
// ミニゲームの開始
//====================================================================
void MiniGameManager::Begin(BYTE type)
{
	// 現在のミニゲームを設定
	m_currentMiniGame = type;

	// ミニゲームの開始時の処理
	m_games[m_currentMiniGame]->Begin();

	// ゲームの状態を設定
	m_gameState = GAMESTATE_FADEIN;

	// 時間を設定
	m_dwTime = 0;
}

//====================================================================
// ミニゲーム更新処理
//====================================================================
void MiniGameManager::Update(void)
{
	if(m_gameState == GAMESTATE_NOTPLAY) return;	// プレイ中でなければ処理しない

	// ローカル変数
	const DWORD dwScalingFrame = 60;		// 拡縮フレーム数
	D3DXVECTOR2 scale(0,0);					// 拡縮時作業用

	// 時間の更新
	m_dwTime++;

	switch(m_gameState){
	case GAMESTATE_FADEIN:
		// ポリゴンの拡縮
		MyMath::Lerp<D3DXVECTOR2>(&scale, D3DXVECTOR2(0.0f, 0.0f),
			m_maxSpriteSize, 1.0f / dwScalingFrame * m_dwTime);
		m_pSprite->SetScale(scale);

		if(m_dwTime > dwScalingFrame){
			m_gameState = GAMESTATE_PLAY;
		}
		break;

	case GAMESTATE_PLAY:
		// ミニゲーム更新処理
		if(m_games[m_currentMiniGame]->Update()){
			// フェードアウト状態に設定する
			m_gameState = GAMESTATE_FADEOUT;

			// 時間設定
			m_dwTime = 0;
		}
		break;

	case GAMESTATE_FADEOUT:
		// ポリゴンの拡縮
		MyMath::Lerp<D3DXVECTOR2>(&scale, m_maxSpriteSize,
			D3DXVECTOR2(0.0f, 0.0f), 1.0f / dwScalingFrame * m_dwTime);
		m_pSprite->SetScale(scale);

		if(m_dwTime > dwScalingFrame){
			// ミニゲーム終了時の処理
			m_games[m_currentMiniGame]->End();

			m_gameState = GAMESTATE_NOTPLAY;
		}
		break;
	}
}

//====================================================================
// ミニゲームの描画
//====================================================================
void MiniGameManager::Draw(void)
{
	if(m_gameState == GAMESTATE_NOTPLAY) return;	// プレイ中でなければ描画しない

	// 現在のレンダリングターゲットを取得
	LPDIRECT3DSURFACE9 pOldSurface, pOldDepth;
	D3DVIEWPORT9 oldViewport;
	m_pDevice->GetRenderTarget(0, &pOldSurface);
	m_pDevice->GetDepthStencilSurface(&pOldDepth);
	m_pDevice->GetViewport(&oldViewport);

	// レンダリングターゲットを変更
	D3DVIEWPORT9 viewport = {0, 0, TEXTURE_SIZE, TEXTURE_SIZE, 0.0f, 1.0f};
	m_pDevice->SetRenderTarget(0, m_pSurface);
	m_pDevice->SetDepthStencilSurface(m_pDepth);
	m_pDevice->SetViewport(&viewport);

	// テクスチャクリア
	m_pDevice->Clear(0, nullptr, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0,0,0), 1.0f, 0);

	// ミニゲーム描画
	m_games[m_currentMiniGame]->Draw();

	// レンダリングターゲットを戻す
	m_pDevice->SetRenderTarget(0, pOldSurface);
	m_pDevice->SetDepthStencilSurface(pOldDepth);
	m_pDevice->SetViewport(&oldViewport);
	SAFE_RELEASE(pOldSurface);
	SAFE_RELEASE(pOldDepth);

	// スプライトを描画
	m_pSprite->Draw();
}

//====================================================================
// プレイ中か問い合わせ
//====================================================================
bool MiniGameManager::IsPlay(void)
{
	return m_gameState != GAMESTATE_NOTPLAY;
}
